
<!--=================================
 rev slider -->
 <section class="slider" style="height:80%;" >
    <div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="car-dealer-03" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.2.6 fullwidth mode -->
      <div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.2.6">
    <ul>  <!-- SLIDE  -->
        <li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="revolution/assets/100x50_banner-image.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
            <img src="<?php echo base_url();?>assets/revolution/assets/banner-image.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
    
        <div class="tp-caption   tp-resizeme" 
           id="slide-5-layer-12" 
           data-x="right" data-hoffset="70" 
           data-y="center" data-voffset="135" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="x:-50px;opacity:0;s:800;e:Power2.easeInOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="620" 
          data-responsive_offset="on" 

          
          style="z-index: 8;"><img src="<?php echo base_url();?>assets/revolution/assets/4f45e-07-bmw-s2.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

        <!-- LAYER NR. 5 -->
        <div class="tp-caption   tp-resizeme" 
           id="slide-5-layer-11" 
           data-x="120" 
           data-y="center" data-voffset="130" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="x:50px;opacity:0;s:800;e:Power2.easeInOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="200" 
          data-responsive_offset="on" 

          
          style="z-index: 9;"><img src="<?php echo base_url();?>assets/revolution/assets/e13ec-06-audi-s2.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>
      </li>
      <!-- SLIDE  -->
        <li data-index="rs-6" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="revolution/assets/100x50_banner-image.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
            <img src="<?php echo base_url();?>assets/revolution/assets/banner-image.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption   tp-resizeme" 
           id="slide-6-layer-4" 
           data-x="3" 
           data-y="center" data-voffset="50" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="x:50px;opacity:0;s:1500;e:Power3.easeOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="2060" 
          data-responsive_offset="on" 

          
          style="z-index: 5;"><img src="<?php echo base_url();?>assets/revolution/assets/74231-04-audi.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption   tp-resizeme" 
           id="slide-6-layer-5" 
           data-x="right" data-hoffset="-10" 
           data-y="center" data-voffset="60" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="x:-50px;opacity:0;s:1500;e:Power3.easeOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="2060" 
          data-responsive_offset="on" 

          
          style="z-index: 6;"><img src="<?php echo base_url();?>assets/revolution/assets/35261-05-honda.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

        <div class="tp-caption   tp-resizeme" 
           id="slide-6-layer-7" 
           data-x="center" data-hoffset="" 
           data-y="center" data-voffset="-140" 
                data-width="['auto']"
          data-height="['auto']"
          data-transform_idle="o:1;"
     
           data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:300;e:Power4.easeInOut;" 
           data-transform_out="opacity:0;s:300;" 
           data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
          data-start="4290" 
          data-splitin="chars" 
          data-splitout="none" 
          data-responsive_offset="on" 

          data-elementdelay="0.05" 
          
          style="z-index: 8; white-space: nowrap; font-size: 40px; line-height: 70px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-align:center;text-transform:uppercase;"> </div>

        <!-- LAYER NR. 5 -->
        

        <!-- LAYER NR. 6 -->
        <div class="tp-caption   tp-resizeme" 
           id="slide-6-layer-3" 
           data-x="right" data-hoffset="159" 
           data-y="center" data-voffset="81" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="x:-50px;opacity:0;s:1500;e:Power3.easeOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="1220" 
          data-responsive_offset="on" 

          
          style="z-index: 10;"><img src="<?php echo base_url();?>assets/revolution/assets/ec416-03-huydai.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

        <!-- LAYER NR. 7 -->
        <div class="tp-caption   tp-resizeme" 
           id="slide-6-layer-2" 
           data-x="202" 
           data-y="center" data-voffset="80" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="x:50px;opacity:0;s:1500;e:Power3.easeOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="1200" 
          data-responsive_offset="on" 

          
          style="z-index: 11;"><img src="<?php echo base_url();?>assets/revolution/assets/1fa45-02-bmw.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

        <!-- LAYER NR. 8 -->
        <div class="tp-caption   tp-resizeme" 
           id="slide-6-layer-1" 
           data-x="center" data-hoffset="" 
           data-y="center" data-voffset="100" 
                data-width="['none','none','none','none']"
          data-height="['none','none','none','none']"
          data-transform_idle="o:1;"
     
           data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:670;e:Power3.easeInOut;" 
           data-transform_out="opacity:0;s:300;" 
          data-start="500" 
          data-responsive_offset="on" 

          
          style="z-index: 12;"><img src="<?php echo base_url();?>assets/revolution/assets/95515-o1-kia.png" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>
      </li>
    </ul>
    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
    </div>
  </section>  

<!--=================================
  rev slider -->


<!--=================================
 welcome -->
  
<section class="welcome-block objects-car page-section-ptb white-bg">
  <div class="container">
  <div class="row" style="margin:0px;">
      <div class="col-lg-12 col-md-12">
         <div class="section-title" >
         <h5 >Welcome to our site</h5>
           <div class="separator"></div>

            </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-md-8" style="font-size:16px;padding-top:10px;text-align: justify;">
      <div>15 years of trusted excellence and the best place to trade in your old vehicle or to buy a new or second hand vehicle for the best market value with trust and confidence. Nandana enterprises provides a warranty for registered used vehicles.</div><br/>
      <div>We carefully select registered vehicles and import best quality from Japan from our direct partner. We arrange leasing facilities within a day on priority basis from any preferred leasing institution. Nandana Enterprises is the one stop shop for your dream vehicle.
     </div><br/>
     <div>We always strive to provide best quality vehicles and after sales service. Come visit us and experience the difference.
     </div>
      </div>
      <div class="col-lg-4 col-md-4"style="font-size:17px;padding-top:20px;">
      <div class="item home-div-s">
            <ul id="content-slider" class="content-slider" >
                <li style="list-style-type: none;">
                    <img src="<?php echo base_url();?>assets/images/objects/img1.jpg" class="img-responsive" />
                </li>
                <li style="list-style-type: none;">
                    <img src="<?php echo base_url();?>assets/images/objects/img2.jpg" class="img-responsive" />
                </li>
                <li style="list-style-type: none;">
                    <img src="<?php echo base_url();?>assets/images/objects/img3.jpg" class="img-responsive" />
                </li>
                <li style="list-style-type: none;">
                    <img src="<?php echo base_url();?>assets/images/objects/img4.jpg"  class="img-responsive" />
                </li>

            </ul>
        </div>
      <!-- <img class="img-responsive" src="<?php echo base_url();?>assets/images/objects/banner1.jpg" alt="" style="height:100%;width:100%;"> -->
    </div>
    
  </div>
</section>


<section class="feature-car bg-2 bg-overlay-black-70 page-section-ptb" style="min-height:300px;">
<div class="container">
<div class="row" style="margin:0px;">
      <div class="col-lg-12 col-md-12">
         <div class="section-title" style="margin-bottom: 30px;">
           <h2 style="color:#fff;">Search</h2>
           <div class="separator" style="color:#fff;"></div>

            </div>
      </div>
    </div>
<div class="row" >
<form  enctype="multipart/form-data" method="get" action="<?php echo base_url() ?>main/search" >
     <div class="col-lg-3 col-md-3">
     <div class="form-group">
  <label  style="font-size:20px;color:#fff;">Brand:</label>
  <select class="form-control" id="bBrand" name="brand" style="background:#fffdfd !important;">
  <?php foreach ($allBrands as $Item) { ?>
    <option value="<?php echo $Item->bid;?>"><?php echo $Item->bname;?></option>
     <?php }?>
  
  </select>
</div>
  </div>
  <div class="col-lg-3 col-md-3">
  <div class="form-group">
  <label style="font-size:20px;color:#fff;">Minimum price:</label>
  <input type="text"  class="form-control"   autocomplete="off" onkeypress="return isNumberKey(event)" style="background: #fffdfd !important;" id="minPrice" name="minPrice"/>

</div>
  </div>
  <div class="col-lg-3 col-md-3">
  <div class="form-group">
  <label style="font-size:20px;color:#fff;">Maximum price:</label>
  <input type="text"   class="form-control"   autocomplete="off" onkeypress="return isNumberKey(event)" style="background: #fffdfd !important;" id="maxPrice" name="maxPrice" />

</div>
  </div>
  <div class="col-lg-3 col-md-3">
  <div class="form-group" style="margin-top:36px;">
  <button type="submit" class="button red col-md-12 btn-block">Search</button>

  </div>
  </div>
   </form>
   </div>
   </div>
   </section>  
<section class="welcome-block objects-car page-section-ptb white-bg " style="padding: 50px 0;">

 <div class="container">
 <div class="row" style="margin:0px;">
      <div class="col-lg-12 col-md-12">
         <div class="section-title" style="margin-bottom: 30px;">
           <h2>RECENT VEHICLES</h2>
           <div class="separator" style="color:#fff;"></div>

            </div>
      </div>
    </div>  
  <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
        <?php foreach ($allvehicles as $Item) { ?>
          <div class="col-lg-3 col-md-3 col-sm-4" style="margin-bottom:20px;">
            <div class="car-item " style="border:1px solid #cacaca;height: 360px;">
              <?php if( $Item['image'] != NULL ){?>
             <div class="car-image" style="text-align:center;height: 140px;">
               <img  src="<?php echo base_url();?>global/uploads/vehicle/thumbnail/<?php echo $Item['image'];?>" alt="" style="    vertical-align: middle;max-width: 100%;max-height: 100%;overflow: hidden;">
             </div>
            
             <?php  } else{ ?>
             <div class="car-image " style="text-align:center;height: 140px;">
               <img  src="<?php echo base_url();?>assets/images/default-thumb.jpg" alt="" style="    vertical-align: middle;max-width: 100%;max-height: 100%;overflow: hidden;">
             </div>

             <?php  } ?>
            
             <div class="car-content " >
               <a href="<?php echo base_url();?>main/vehicle/<?php echo $Item['vId'];?>" class="txt-hide" title="<?php echo $Item['name'];?>"><?php echo $Item['name'];?></a>
               <div>
               <?php $date=date_create($Item['createAt']);?>
               Posted at - <?php echo date_format($date,"Y M d ");?>
              </div>
               <div class="car-lis">
               <ul class="list-inline">
                 <li class="txt-hide" title="<?php echo $Item['year'];?>"> <i class="fa fa-registered"></i> <?php echo $Item['year'];?></li>
                 <li class="txt-hide" title="<?php echo $Item['transmission'];?>"><i class="fa fa-cog"></i> <?php echo $Item['transmission'];?></li>
                 <li class="txt-hide" title="<?php echo $Item['mileage'];?>"> <i class="fa fa-dashboard"></i> <?php echo $Item['mileage'];?></li>
               </ul>
            </div>
           
          
               <div class="price" style="padding-top:10px;">
                 <span class="new-price" style="font-size:15px;">Rs. <?php echo  number_format($Item['price']);?> </span>
               </div>

               <div class="row" style="padding:10px;">
                  <a class="button red col-md-12"  href="<?php echo base_url();?>main/vehicle/<?php echo $Item['vId'];?>"  style="color:#fff;font-size:14px;">More Info</a>
              </div> 
             </div>
           </div>
           </div>
     <?php }?>
           </div>
          </div>
       </div>
     </div>
  </div>
</section>



<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>


 






