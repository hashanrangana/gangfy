<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/portal_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Shared Coupon</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Service Name</th>
                                     <th scope="col">Customer Name</th>
                                    <th scope="col">Coupon Name</th>
                                    <th scope="col">Coupon Code</th>
                                    <th scope="col">Coupon Quota</th>
                                    <th scope="col">Total Share</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($csr)) {
                                    foreach ($csr as $cdata) {
                                        ?>
                                        <tr>
                                            <td><a href="#"><?php echo $cdata['list_name']; ?></a></td>
                                            <td><?php echo $cdata['name']; ?></td>
                                            <td><?php echo $cdata['coupon_name']; ?></td>
                                            <td><?php echo $cdata['coupon_code']; ?></td>
                                            <td><?php echo $cdata['coupon_quota']; ?></td>
                                            <td><?php echo $cdata['coupon_share']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

