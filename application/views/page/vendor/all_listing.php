<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4" style="width: 100%;">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">All Services</h1>
                    <a href="<?php echo base_url(); ?>dashboard/create_services" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Create Service</a>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12 col-xs-12"  style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">Branch Name</th>
                                <th scope="col">Title</th>
                                <th scope="col">Service</th>
                                <th scope="col">Sub Service</th>
                                <th scope="col">Location</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($alllisting)) {
                                foreach ($alllisting as $Item) {
                                    ?>
                                    <tr>
                                        <td><a href="#" target="_blank"><?php echo $Item['branch_name']; ?></a></td>
                                        <td><a href="#" target="_blank"><?php echo $Item['listingName']; ?></a></td>
                                        <td><?php echo $Item['serviceName']; ?></td>
                                        <td><?php echo $Item['subServiceName']; ?></td>
                                        <td class="text-center"><?php echo $Item['location']; ?></td>
                                        <td  class="text-center">
                                            <?php if ($Item['activeStatus'] == 1) { ?>
                                                <span class="approve-btn">Published</span>
                                            <?php } else if ($Item['activeStatus'] == 2) { ?>
                                                <span class="pending-btn">Achieved</span>
                                            <?php } else { ?>
                                                <span class="reject-btn">Unpublished</span>
                                            <?php } ?>
                                        </td>
                                        <td style="width: 150px;">
                                            <a href="<?php echo base_url(); ?>home/details/<?php echo $Item['listingId']; ?>/<?php echo str_replace(" ", "-", $Item['listingName']); ?>" target="_blank" class="btn btn-info btn-sm" style="float:left;margin-right:5px;"><i class="fa fa-eye"></i></a> 
                                            <?php echo form_open('dashboard/edit_service'); ?> <input type="hidden" name="lid" value="<?php echo $Item['listingId']; ?>"> <button type="submit" class="btn btn-warning btn-sm" style="float:left;margin-right:5px;"><i class="fa fa-edit"></i></button><?php echo form_close(); ?>  
                                            <a target="_blank" href="<?php echo base_url(); ?>listing/list_images/<?php echo $Item['listingId']; ?>" class="btn btn-primary btn-sm" style="float:left;margin-right:5px;"><i class="fas fa-image"></i></a>  
                                            <a href="<?php echo base_url(); ?>listing/delete_listing/<?php echo $Item['listingId']; ?>" class="btn btn-danger btn-sm" style="float:left;"><i class="fas fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper



<?php $this->load->view('page/temp/portal_footer'); ?>

