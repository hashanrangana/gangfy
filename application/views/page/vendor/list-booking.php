<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/portal_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Bookings</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Listing Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">From</th>
                                    <th scope="col">To</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Coupon</th>
                                    <th scope="col">Note</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($book)) {
                                    foreach ($book as $bdata) {
                                        ?>

                                        <tr>
                                            <td><a href="<?php echo base_url(); ?>dashboard/customer_detail/<?php echo $bdata['userinfo_id']; ?>" target="_blank"><?php echo $bdata['name']; ?></a></td>
                                            <td><?php echo $bdata['listingName']; ?></td>
                                            <td><?php echo $bdata['book_date']; ?></td>
                                            <td><?php echo $bdata['slot_from']; ?></td>
                                            <td><?php echo $bdata['slot_to']; ?></td>
                                            <td class="text-right"><?php echo $bdata['amount']; ?></td>
                                            <td><?php echo $bdata['coupon_code']; ?></td>
                                            <td><textarea class="form-control" id="bnote" name="bnote[]" onchange="bookingNote(<?php echo $bdata['booking_Id']; ?>)" style="width: 150px; height: 50px;border-radius: 0px;"><?php echo $bdata['note']; ?></textarea> </td>
                                            <td class="text-center"><?php
                                                $sid = $bdata['orderStatus'];
                                                if ($sid == 1) {
                                                    echo "<span class=\"approve-btn\">Approved</span>";
                                                } else if ($sid == 3) {
                                                    echo "<span class=\"reject-btn\">Rejected</span>";
                                                } else {
                                                    echo "<span class=\"pending-btn\">Pending</span>";
                                                }
                                                ?></td>
                                            <td  class="text-center" style="width:150px;">
                                                <?php if ($sid != 1) { ?>
                                                    <a href="<?php echo base_url(); ?>booking/approve_booking/<?php echo $bdata['booking_Id']; ?>" title="Approve"><button class="btn btn-sm btn-primary"><i class="fa fa-check-circle"></i></button></a> 
                                                    <a href="<?php echo base_url(); ?>booking/booking_reject/<?php echo $bdata['booking_Id']; ?>" title="Reject"><button class="btn btn-sm btn-danger"><i class="fa fa-times-circle"></i></button></a> 
                                                <?php } else { ?>

                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<script>
    function bookingNote(id) {
        var note = $('#bnote').val();
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "dashboard/booking_note",
            dataType: 'json',
            data: {"bid": id, "bnote": note},
            success: function (res) {
                if (res)
                {
                    console.log(res);
                }
            }
        });
    }
</script>

<?php $this->load->view('page/temp/portal_footer'); ?>

