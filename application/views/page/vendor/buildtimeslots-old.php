<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Create Time Slot</h1>
            </div>
            </div>

            <div class="row">
                <div class="row">
                    <?php //echo form_open('listing/create_slot', array('class' => 'form-horizontal')); ?>
                    <form method="post" action="<?php echo base_url();?>listing/create_slot">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="edate">Select Services *</label>
                                    <select class="form-control" id="service" name="service" required>
                                        <option value="">Select Services</option>
                                        <?php
                                        if (isset($list)) {
                                            foreach ($list as $ldata) {
                                                ?>
                                                <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="sname">Slot Name *</label>
                                    <input type="text" class="form-control" name="sname" id="sname" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="sday">Day *</label>
                                    <select class="form-control" id="sday" name="sday" required>
                                        <option value="">Select Day</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="sfrom">Time From *</label>
                                    <input id="" type="text" name="sfrom" class="form-control TimeSelect" required readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="sto">Time To *</label>
                                    <input name="sto" id=""  type="text" class="form-control TimeSelect" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom:40px;">
                        
                            <div class="form-group">                
                                <button type="submit" class="btn btn-info btn-md"  name="add" id="add">Save Slot</button>
                            </div>
                        
                    </div>
                    <!-- end row -->
                    <?php //echo form_close(); ?>
                    </form>
                     <hr/>
                </div>
               
                <?php
                if (isset($slot)) {
                    ?>
                    <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Service Name</th>
                                    <th>Slot Name</th>
                                    <th>Day</th>
                                    <th>Time From</th>
                                    <th>Time To</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($slot as $sdata) { ?>
                                    <tr>
                                        <td><?php echo $sdata['listingName']; ?></td>
                                        <td><?php echo $sdata['slot_name']; ?></td>
                                        <td  class="text-center"><?php echo $sdata['slot_day']; ?></td>
                                        <td  class="text-center"><?php echo $sdata['slot_from']; ?></td>
                                        <td  class="text-center"><?php echo $sdata['slot_to']; ?></td>
                                        <td  class="text-center"><a href="<?php echo base_url(); ?>dashboard/delete_slot/<?php echo $sdata['slotId']; ?>"><i class="fas fa-times text-warning-red"></i></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                    <?php
                }
                ?>

            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

