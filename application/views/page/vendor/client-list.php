<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/portal_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Customers List</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Email</th>
                                    <!--<th scope="col">Address</th>-->
                                    <th scope="col">Country</th>
                                    <th scope="col">Contact Number</th>
                                    <th scope="col">Coupon</th>
                                    <th scope="col">Note</th>
                                    <th scope="col" >Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($cus)) {
                                    foreach ($cus as $cdata) {
                                        ?>
                                        <tr>
                                            <td><a href="" target="_blank"><?php echo $cdata['name']; ?></a></td>
                                            <td><?php echo $cdata['userinfo_username']; ?></td>
                                            <!--<td><?php //echo $cdata['address1'] . ',' . $cdata['address2']; ?></td>-->
                                            <td><?php echo $cdata['country']; ?></td>
                                            <td><?php echo $cdata['phonenumber_one']; ?></td>
                                            <td><?php echo $cdata['coupon_code'] ?></td>
                                            <td><textarea class="form-control" id="cnote" name="cnote[]" onchange="customerNote(<?php echo $cdata['userinfo_id']; ?>)" style="width: 150px; height: 50px;border-radius: 0px;"><?php echo $cdata['note'];  ?></textarea> </td>
                                            <td class="text-center"><a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>dashboard/customer_detail/<?php echo $cdata['userinfo_id']; ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" onclick="$('#cid').val('<?php echo $cdata['userinfo_id']; ?>')" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Ban"><i class="fa fa-ban"></i></button>
                                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" onclick="$('#cusid').val('<?php echo $cdata['userinfo_id']; ?>')" data-target="#msgModal" data-toggle="tooltip" data-placement="top" title="Message"><i class="fa fa-envelope"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">

                                    <h4 class="modal-title">Ban Customer</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <?php echo form_open('dashboard/ban_customer'); ?>
                                    <div class="col-md-12">
                                        <input type="hidden" name="cid" id="cid">
                                        <div class="form-group">
                                            <label for="reason">Reason *</label>
                                            <select class="form-control" name="reason" id="reason" required>
                                                <option value="">Select</option>
                                                <option value="Fake Reservation">Fake Reservation</option>
                                                <option value="Fake Details">Fake Details</option>
                                                <option value="Behavior">Behavior</option>
                                                <option value="Disciplines">Disciplines</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea class="form-control" rows="5" name="description" id="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info btn-md"  name="submit">Submit</button>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>


                            </div>

                        </div>
                    </div>

                    <div id="msgModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">

                                    <h4 class="modal-title">Message</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <?php echo form_open('dashboard/send_message'); ?>
                                    <div class="col-md-12">
                                        <input type="hidden" name="cusid" id="cusid">
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea class="form-control" rows="5" name="message" id="message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info btn-md"  name="submit">Send</button>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<script>
    function customerNote(id) {
        var note = $('#cnote').val();
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "dashboard/customer_note",
            dataType: 'json',
            data: {"cid": id, "cnote": note},
            success: function (res) {
                if (res)
                {
                    console.log(res);
                }
            }
        });
    }
</script>

<?php $this->load->view('page/temp/portal_footer'); ?>

