<?php $this->load->view('page/temp/portal_header'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

</head>
<style>
    .tab-pane{
        padding: 20px;
        border: 1px solid #efefef;
        border-top: 0px;
        background: #fbfbfb;
    }
</style>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Create Time Slot</h1>
                </div>
            </div>

            <div class="row">  
                <?php echo form_open('listing/create_slot'); ?>
                <div class="row">     

                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="sname">Date</label>
                                    <input type="text" class="form-control DateFinder" placeholder="Select Date" name="sdate" id="sdate" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="service">Select Services *</label>
                                    <select class="form-control" id="service" name="service[]" required>
                                        <option value="">Select Services</option>
                                        <?php
                                        if (isset($list)) {
                                            foreach ($list as $ldata) {
                                                ?>
                                                <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>                         
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="sfrom">Time From *</label>
                                    <input name="sfrom[]" id="sfrom" type="time" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="sto">Time To *</label>
                                    <input name="sto[]" id="sto"  type="time" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-1" style="text-align: center;margin-top: 30px;">
                                <div class="form-group">
                                    <label for="sto"></label>
                                    <button id="btnAdd" type="button" value="Add" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>

                        </div>
                        <div id="addServices"></div>

                    </div>

                    <div class="col-md-12">
                        <div class="form-group">                
                            <button type="submit" class="btn btn-info btn-md"  name="add" id="add">Save Slot</button>
                        </div>

                    </div>
                    <!-- end row -->
                    <?php echo form_close(); ?>
                </div>
                <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#modalCart">Create Multiple</button>

            </div>
            <br/>
            <?php echo form_open('listing/search_slot'); ?>
            <div class="row" style="padding: 20px 0px 5px;background: #efefef;margin: 20px;border-radius: 5px;">
                <div class="col-md-3" style="text-align: right;">
                    <h6 style="margin-top: 10px;">Filter By</h6>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <input type="text" class="form-control DateFinder" placeholder="Date" name="date" id="date">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <select class="form-control" id="serv" name="serv">
                            <option value="">Services</option>
                            <?php
                            if (isset($list)) {
                                foreach ($list as $ldata) {
                                    ?>
                                    <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">                
                        <button type="submit" class="btn btn-primary btn-md"  name="search"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <?php
            if (isset($slot)) {
                ?>
                <div class="row"> 
                    <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Service Name</th>
                                    <th>Time From</th>
                                    <th>Time To</th>
                                    <th>Open / Close</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($slot as $sdata) {
                                    $bk = $sdata['bookable'];
                                    ?>
                                    <tr>
                                        <td  class="text-center"><?php echo $sdata['slot_date']; ?></td>
                                        <td><?php echo $sdata['listingName']; ?></td>
                                        <td  class="text-center"><?php echo $sdata['slot_from']; ?></td>
                                        <td  class="text-center"><?php echo $sdata['slot_to']; ?></td>
                                        <td class="text-center"><?php
                                            if ($bk == 1) {
                                                echo "<span class=\"approve-btn\">Open</span>";
                                            } else {
                                                echo "<span class=\"reject-btn\">Close</span>";
                                            }
                                            ?></td>
                                        <td  class="text-center">
                                            <?php if ($bk == 1) { ?>
                                                <a href="<?php echo base_url(); ?>dashboard/close_slot/<?php echo $sdata['slotId']; ?>" title="Close" class="btn btn-warning btn-sm"><i class="fas fa-times"></i></a> 
                                            <?php } else { ?>
                                                <a href="<?php echo base_url(); ?>dashboard/open_slot/<?php echo $sdata['slotId']; ?>" title="Open" class="btn btn-success btn-sm"><i class="fas fa-check"></i></a> 
                                            <?php } ?>
                                            <?php if ($sdata['edit'] == 1) { ?>
                                                <button type="button"  class="btn btn-primary btn-sm" title="Edit" data-toggle="modal" onclick="slotValue(<?php echo $sdata['slotId']; ?>);" data-target="#editSlot"><i class="fas fa-edit"></i></button>
                                            <?php } ?>
                                            <a href="<?php echo base_url(); ?>dashboard/delete_slot/<?php echo $sdata['slotId']; ?>" class="btn btn-danger btn-sm" title="Delete"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Create Multiple Time Slot</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <!--Body-->
                    <?php echo form_open('listing/multiple_slot'); ?>
                    <div class="modal-body">  
                        <div class="row">   	
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="drange">Date Range *</label>
                                            <input type="text" class="form-control daterange" placeholder="Select a Date" name="drange" id="drange" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sfrom">Time From *</label>
                                            <input name="sfrom" id="sfrom" type="time" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sto">Time To *</label>
                                            <input name="sto" id="sto" type="time" class="form-control" required>
                                        </div>
                                    </div>   
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-12" for="service">Select Services *</label>
                                            <?php
                                            if (isset($list)) {
                                                foreach ($list as $ldata) {
                                                    ?>
                                                    <input type="checkbox" name="service[]" value="<?php echo $ldata['listingId'] ?>"> <?php echo $ldata['listingName'] ?><br>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>                         

                                </div>

                            </div>
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                            <input type="submit" name="create" class="btn btn-primary" value="Create">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editSlot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit Time Slot</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <!--Body-->
                    <?php echo form_open('listing/update_slot'); ?>
                    <div class="modal-body">  
                        <div class="row">   	
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="drange">Date *</label>
                                            <input type="text" class="form-control DateFinder" placeholder="Select a Date" name="sldate" id="sldate" required>
                                            <input type="hidden" name="slid" id="slid">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sfrom">Time From *</label>
                                            <input name="slfrom" id="slfrom" type="time" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sto">Time To *</label>
                                            <input name="slto" id="slto" type="time" class="form-control" required>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                            <input type="submit" name="create" class="btn btn-primary" value="Edit">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<script>

    $(function () {
        $("#btnAdd").bind("click", function () {
            var div = $("<div class='row' id='dyn_row'>");
            jQuery.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>" + "dashboard/get_service",
                dataType: 'json',
                success: function (res) {
                    var oval = [];
                    if (res)
                    {
                        for (var i = 0; i < res.length; i++) {
                            oval.push('<option value=' + res[i].listingId + '>' + res[i].listingName + '</option>');
                        }
                        div.html(GetDynamicTextBox(oval));
                        $("#addServices").append(div);
                    }

                }
            });

        });

        $("body").on("click", ".remove", function () {
            $(this).closest("#dyn_row").remove();
        });

    });

    function GetDynamicTextBox(value) {
        return '<div class="col-md-2"></div><div class="col-md-3"><div class="form-group"><select class="form-control" id="service" name="service[]" required><option value="">Select Services</option>' + value + '</select></div></div><div class="col-md-3"><div class="form-group"><input name="sfrom[]" id="sfrom" type="time" class="form-control" required></div></div><div class="col-md-3"><div class="form-group"><input name="sto[]" id="sto"  type="time" class="form-control" required></div></div><div class="col-md-1"  style="text-align: center;"><div class="form-group"><button type="button" value="Remove" class="remove btn btn-danger"><i class="fa fa-trash-alt"></i></button></div></div></div>';
    }
</script>
<script src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script>
    function slotValue(id) {
        $('#slid').val(id);
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "listing/slot_data",
            dataType: 'json',
            data: {"sid": id},
            success: function (res) {
                if (res)
                {
                    $('#sldate').val(res[0].slot_date);
                    $('#slfrom').val(res[0].slot_from);
                    $('#slto').val(res[0].slot_to);
                }
            }
        });
    }
    $('.daterange').daterangepicker({
        format: 'yy-mm-dd'
    });

</script>


<?php $this->load->view('page/temp/portal_footer'); ?>

