<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/portal_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Customize Bookings List</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($book)) {
                                    foreach ($book as $bdata) {
                                        ?>

                                        <tr>
                                            <td><a href="<?php echo base_url(); ?>dashboard/customer_detail/<?php echo $bdata['userinfo_id']; ?>" target="_blank"><?php echo $bdata['name']; ?></a></td>
                                            <td><?php echo $bdata['request_date']; ?></td>
                                            <td><?php echo $bdata['request_desc']; ?></td>
                                            <td class="text-center"><?php
                                                $sid = $bdata['status'];
                                                if ($sid == 1) {
                                                    echo "<span class=\"approve-btn\">Approved</span>";
                                                } else if ($sid == 3) {
                                                    echo "<span class=\"reject-btn\">Rejected</span>";
                                                } else {
                                                    echo "<span class=\"pending-btn\">Pending</span>";
                                                }
                                                ?></td>
                                            <td  class="text-center" style="width:150px;">
                                                <a href="<?php echo base_url(); ?>booking/view_booking/<?php echo $bdata['br_id']; ?>" target="_blank"><button class="btn btn-sm btn-success"><i class="fa fa-eye"></i></button></a>
                                                <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" onclick="$('#cusid').val('<?php echo $bdata['user_id']; ?>')" data-target="#msgModal" data-toggle="tooltip" data-placement="top" title="Message"><i class="fa fa-envelope"></i></button>
                                                <?php if ($sid == 2) { ?>
                                                    <a href="<?php echo base_url(); ?>booking/approve_customize_booking/<?php echo $bdata['br_id']; ?>"><button class="btn btn-sm btn-primary"><i class="fa fa-check-circle"></i></button></a> 
                                                    <a href="<?php echo base_url(); ?>booking/customize_booking_reject/<?php echo $bdata['br_id']; ?>"><button class="btn btn-sm btn-danger"><i class="fa fa-times-circle"></i></button></a> 
                                                <?php } else { ?>

                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="msgModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Message</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open('dashboard/customize_message'); ?>
                            <div class="col-md-12">
                                <input type="hidden" name="cusid" id="cusid">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea class="form-control" rows="5" name="message" id="message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info btn-md"  name="submit">Send</button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>


                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->


<?php $this->load->view('page/temp/portal_footer'); ?>

