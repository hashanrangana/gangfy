<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>
<div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title">Create Service</h1>
                </div>
            </div>
            <div class="row">
                <?php echo form_open_multipart('listing/add_listing'); ?>
                <div class="row">
                    <div class="col-md-12"><h5 class="sec-title" style="border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Service Info</h5></div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="branch">Branch Name *</label>
                                    <select class="form-control" id="branch" name="branch" required>
                                        <option value="" >Select Branch</option>
                                        <?php
                                        if (isset($bran)) {
                                            foreach ($bran as $bd) {
                                                ?>
                                                <option value="<?php echo $bd['branch_id']; ?>"><?php echo $bd['branch_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php echo form_error('branch', '<div class="error" style="color:red;">', '</div>'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" id="" aria-describedby="" placeholder="Enter Title" id="listingName"  name="listingName" required>
                                </div>
                                <?php echo form_error('listingName', '<div class="error" style="color:red;">', '</div>'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="services">Service</label>
                                    <select class="form-control" id="serviceName" name="serviceName" required>
                                        <option value="" >Select</option>
                                        <?php
                                        if (isset($allservices)) {
                                            foreach ($allservices as $serviceItem) {
                                                ?>
                                                <option value="<?php echo $serviceItem['serviceId']; ?>"><?php echo $serviceItem['serviceName']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php echo form_error('serviceName', '<div class="error" style="color:red;">', '</div>'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="subservices">Sub Service</label>
                                    <div id="addSubService">
                                        <select class="form-control" id="viewSubAdd" name="viewSubAdd">
                                            <option value="" >Select</option>
                                        </select>
                                    </div>
                                </div>
                                <?php echo form_error('viewSubAdd', '<div class="error" style="color:red;">', '</div>'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rate">Hourly Rate</label>
                                    <input type='text' class="form-control" id='price' name="price" placeholder="$" onkeypress="return isNumberKey(event)" required/></div>
                            </div>
                            <?php echo form_error('price', '<div class="error" style="color:red;">', '</div>'); ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><h5 class="sec-title"  style="margin-top:20px;border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Note</h5></div>
                            <div class="col-md-12">
                                <textarea id="editor1" name="editor" style="border:none;"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12"><h5 class="sec-title" style="margin-top:20px;border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Add Amenities</h5></div>
                            <div class="col-md-12 gb-srv-item">
                                
                                <?php
                                        if (isset($allfeatures)) {
                                            foreach ($allfeatures as $Item) {
                                                $fname = $Item['featuresName'];
                                                ?>

                                                <?php if ($fname == 'Restroom') { ?>
                                                    <div class="col-sm-3"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-restroom" style="margin-right: 18px;"></i><?php echo $Item['featuresName']; ?>
                                                    </div>
                                                <?php } elseif ($fname == 'Car Park') { ?>                                                                     
                                                    <div class="col-sm-3"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-car" style="margin-right: 20px;"></i><?php echo $Item['featuresName']; ?>
                                                    </div>
                                                <?php } elseif ($fname == 'Floodlights') { ?>                                                              
                                                    <div class="col-sm-3"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-lightbulb" style="margin-right: 20px;"></i> <?php echo $Item['featuresName']; ?>
                                                    </div>
                                                <?php } elseif ($fname == 'Water Dispenser') { ?>
                                                    <div class="col-sm-3"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-prescription-bottle" style="margin-right: 20px;"></i> <?php echo $Item['featuresName']; ?>
                                                   </div>
                                                <?php } ?>

                                                <?php
                                            }
                                        }
                                        ?>
                   
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="sec-title"  style="margin-top:20px;border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Images Upload</h5></div>    
                            <div class="row">
                            <div class="col-md-12">
                                <div class="gb-map" style="margin-top: 0px;padding:0px;padding-top:20px;">
                                    <lable class="col-md-12">Maximum 5 images</lable><br>
                                    <span class="btn btn-default btn-file">
                                        <input id="input-2" name="userfile[]" type="file" class="file" multiple data-show-upload="true" data-show-caption="true">
                                    </span>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                
                <div class="col-md-12" style="margin-top:40px;">
                    <div class="form-group">                
                        <button type="submit" class="btn btn-info btn-md"  name="add" id="add">Save Service</button>
                    </div>

                </div>
                </div>
                <?php echo form_close(); ?>
            </div>

</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
</div>

<!--old development-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>global/Multifile/js/jquery.MultiFile.js"></script>


<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>
                                        CKEDITOR.replace('editor1', {
                                            toolbar: [
                                                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript']},
                                                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                                                {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']}
                                            ]
                                        });

</script>

<?php
$this->load->view('page/temp/portal_footer');
?>