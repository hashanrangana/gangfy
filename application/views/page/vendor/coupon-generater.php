<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Create Coupon</h1>
                </div>
            </div>

            <div class="row">
                <div class="row">
                    <?php echo form_open('dashboard/create_coupon'); ?>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="service">Select Services *</label>
                                    <select class="form-control" id="service" name="service" required>
                                        <option value="">Select Services</option>
                                        <?php
                                        if (isset($list)) {
                                            foreach ($list as $ldata) {
                                                ?>
                                                <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="cname">Coupon Name *</label>
                                    <input type="text" class="form-control" name="cname" placeholder="Coupon Name" id="cname"  required>
                                </div>
                            </div>                          
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="ccode">Coupon Code *</label>
                                    <input type="text" class="form-control" name="ccode" placeholder="Coupon Code" id="ccode" required>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group">
                                    <label for="cquota">Rate *</label>
                                    <input type="text" class="form-control" name="cquota" placeholder="" id="cquota" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="ctype"><span style="color:#fff">.</span></label>
                                    <select name="ctype" class="form-control" required>
                                        <option value="">Select</option>
                                        <option value="%">%</option>
                                        <option value="$">$</option>
                                    </select>
                                </div>
                                
                            </div>
                           
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="cdesc">Description *</label>
                                    <textarea class="form-control" name="cdesc" id="cdesc" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom:40px;">
                        <div class="form-group">                
                            <button type="submit" class="btn btn-info btn-md"  name="coupon" id="coupon">Create Coupon</button>
                        </div>

                    </div>
                    <!-- end row -->
                    <?php echo form_close(); ?>
                    <hr/>
                </div>

                <?php
                if (isset($cup)) {
                    ?>
                    <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Service Name</th>
                                    <th>Coupon Name</th>
                                    <th>Coupon Code</th>
                                    <th>Quota</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($cup as $cdata) {
                                    $sid = $cdata['status'];
                                    ?>
                                    <tr>
                                        <td><?php echo $cdata['listingName']; ?></td>
                                        <td><?php echo $cdata['coupon_name']; ?></td>
                                        <td  class="text-center"><?php echo $cdata['coupon_code']; ?></td>
                                        <td  class="text-center"><?php echo $cdata['quota'] . $cdata['coupon_type']; ?></td>
                                        <td  class="text-center"><?php echo $cdata['coupon_desc']; ?></td>
                                        <td class="text-center"><?php
                                            if ($sid == 1) {
                                                echo "<span class=\"approve-btn\">Active</span>";
                                            } else {
                                                echo "<span class=\"reject-btn\">Deactivate</span>";
                                            }
                                            ?></td>
                                        <td  class="text-center">
                                            <?php if ($sid == 1) { ?>
                                            <a href="<?php echo base_url(); ?>dashboard/deactivate_coupon/<?php echo $cdata['cd_id']; ?>" title="Deactivate"><button class="btn btn-sm btn-info">Deactivate</button></a> 
                                                <button type="button" class="btn btn-primary btn-sm" title="Share Coupon" data-toggle="modal" onclick="$('#cid').val('<?php echo $cdata['cd_id']; ?>')" data-target="#myModal"><i class="fa fa-share-alt"></i> </button>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url(); ?>dashboard/active_coupon/<?php echo $cdata['cd_id']; ?>" title="Active"><button class="btn btn-sm btn-primary">Active</button></a> 
                                            <?php } ?>
                                                <a href="<?php echo base_url(); ?>dashboard/delete_coupon/<?php echo $cdata['cd_id']; ?>" title="Delete"><button class="btn btn-sm btn-danger">Delete</button></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                    <?php
                }
                ?>

                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                
                                <h4 class="modal-title">Share Coupon</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open('dashboard/share_coupon'); ?>
                                <div class="col-md-12">
                                    <input type="hidden" name="cid" id="cid">
                                    <div class="form-group">
                                        <label for="cname">Customer Name *</label>
                                        <select class="form-control" name="cname" id="cname" required>
                                            <option value="">Select</option>
                                            <?php
                                            if (isset($cus)) {
                                                foreach ($cus as $cd) {
                                                    ?>
                                                    <option value="<?php echo $cd['userinfo_id'] ?>"><?php echo $cd['name'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-md"  name="submit">Share</button>
                                    </div>
                                </div>
                                <?php form_close(); ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<script>

    $(document).ready(function () {
        $("#cname").change(function (event) {
            event.preventDefault();
            var sname = $('#service').val();
            var cname = $('#cname').val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "dashboard/gen_coupon",
                dataType: 'json',
                data: {"sname": sname, "cname": cname},
                success: function (res) {
                    if (res)
                    {
                        $('#ccode').val(res.cnum);
                    }
                }
            });
        });
    });

</script>

<?php $this->load->view('page/temp/portal_footer'); ?>

