<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Service Image Update</h1>
                </div>
            </div>

            <div class="row">
                <?php
                if (isset($img)) {
                    ?>
                    <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Service Name</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($img as $idata) { ?>
                                    <tr>
                                        <td><?php echo $idata['listingName']; ?></td>
                                        <td><img src="<?php echo base_url(); ?><?php echo $idata['image']; ?>" style="width: 100px; height: 80px;" > </td>
                                        <td  class="text-center"><a href="<?php echo base_url(); ?>listing/delete_image/<?php echo $idata['listingImageId']; ?>"><i class="fas fa-times text-warning-red"></i></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="row">
                <div class="col-md-12">
                    <form method="post" action="<?php echo base_url(); ?>listing/update_images" enctype="multipart/form-data">
                       
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="pimage">Please Upload Image Here</label>
                                        <input id="files" class="form-control" type="file" name="userfile" maxlength="5" />
                                    </div>
                                </div>
                            </div>
                     
                       
                            <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">                
                                    <button type="submit" class="btn btn-primary"  name="add" id="add">Save</button>
                                </div>
                                </div>
                            </div>
                       
                        <!-- end row -->
                    </form>
                </div>
                </div>
            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

