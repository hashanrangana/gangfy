<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">List Amenities</h1>
                </div>
            </div>

            <div class="row">
                <?php
                if (isset($feature)) {
                    ?>
                    <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Service Name</th>
                                    <th>Amenities</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($feature as $fs) { ?>
                                    <tr>
                                        <td><?php echo $fs['listingName']; ?></td>
                                        <td><?php echo $fs['featuresName']; ?></td>
                                        <td  class="text-center"><a href="<?php echo base_url(); ?>listing/delete_feature/<?php echo $fs['listingfeaturesId']; ?>"><i class="fas fa-times text-warning-red"></i></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                        </div>
                    </div>
                    <?php
                }
                ?>
                 <div class="row">
                <div class="col-md-12">
                    <form method="post" action="<?php echo base_url(); ?>listing/add_features">
                        
                            <div class="row">
                                <div class="col-md-12"><h5 class="sec-title" style="margin-top:20px;border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Add Amenities</h5></div>
                                   <div class="col-md-12 gb-srv-item">
                                    
                                     <?php
                                        if (isset($allfeatures)) {
                                            foreach ($allfeatures as $Item) {
                                                $fname = $Item['featuresName'];
                                                ?>

                                                <?php if ($fname == 'Restroom') { ?>
                                                    <div class="col-sm-12"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-restroom" style="margin-right: 18px;"></i><?php echo $Item['featuresName']; ?>
                                                    </div>
                                                <?php } elseif ($fname == 'Car Park') { ?>                                                                     
                                                    <div class="col-sm-12"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-car" style="margin-right: 20px;"></i><?php echo $Item['featuresName']; ?>
                                                    </div>
                                                <?php } elseif ($fname == 'Floodlights') { ?>                                                              
                                                    <div class="col-sm-12"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-lightbulb" style="margin-right: 20px;"></i> <?php echo $Item['featuresName']; ?>
                                                    </div>
                                                <?php } elseif ($fname == 'Water Dispenser') { ?>
                                                    <div class="col-sm-12"> 
                                                        <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId']; ?>" class=""> <i class="fas fa-prescription-bottle" style="margin-right: 20px;"></i> <?php echo $Item['featuresName']; ?>
                                                   </div>
                                                <?php } ?>

                                                <?php
                                            }
                                        }
                                        ?>
                                    
                                   </div>
                            </div>
                       
                        <div class="row" style="margin-top:40px;">
                            <div class="col-md-12">
                                <div class="form-group text-left">                
                                    <button type="submit" class="btn btn-info"  name="add" id="add">Update</button>
                                    <a href="<?php echo base_url(); ?>dashboard/edit_service" class="btn btn-warning">Go Back</a>
                                </div>
                            </div>
                            
                        </div>
                        <!-- end row -->
                    </form>
                    
                </div>
                </div>
            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

