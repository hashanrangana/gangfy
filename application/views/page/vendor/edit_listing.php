<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>
<div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title">Edit Service</h1>
                </div>
            </div>
            <div class="row">
                <?php echo form_open('listing/update_service'); ?>
                <div class="row">
                    <?php
                    if (isset($list)) {
                        foreach ($list as $ls) {
                            ?>
                            <div class="col-md-12"><h5 class="sec-title" style="border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Service Info</h5></div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="hidden" name="lid" value="<?php echo $ls['listingId']; ?>">
                                        <div class="form-group">
                                            <label for="branch">Branch Name *</label>
                                            <select class="form-control" id="branch" name="branch" required>
                                                <?php
                                                foreach ($bran as $bd) {
                                                    if ($bd['branch_id'] == $ls['branch_id']) {
                                                        ?>
                                                        <option value="<?php echo $bd['branch_id']; ?>" selected><?php echo $bd['branch_name']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $bd['branch_id']; ?>"><?php echo $bd['branch_name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <?php echo form_error('branch', '<div class="error" style="color:red;">', '</div>'); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control" id="" aria-describedby="" placeholder="Enter Title" value="<?php echo $ls['listingName']; ?>" id="listingName"  name="listingName" required>
                                        </div>
                                        <?php echo form_error('listingName', '<div class="error" style="color:red;">', '</div>'); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="services">Services</label>
                                            <select class="form-control" id="serviceName" name="serviceName" required>

                                                <?php
                                                foreach ($allservices as $serviceItem) {
                                                    if ($serviceItem['serviceId'] == $ls['serviceId']) {
                                                        ?>
                                                        <option value="<?php echo $serviceItem['serviceId']; ?>" selected=><?php echo $serviceItem['serviceName']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $serviceItem['serviceId']; ?>"><?php echo $serviceItem['serviceName']; ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <?php echo form_error('serviceName', '<div class="error" style="color:red;">', '</div>'); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="subservices">Add Sub Services</label>
                                            <div id="addSubService">
                                                <select class="form-control" id="viewSubAdd" name="viewSubAdd">
                                                    <option value="<?php echo $ls['subServicesId']; ?>" selected=><?php echo $ls['subServiceName']; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php echo form_error('viewSubAdd', '<div class="error" style="color:red;">', '</div>'); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="rate">Hourly Rate</label>
                                            <input type='text' class="form-control" id='price' name="price" placeholder="$" value="<?php echo $ls['price']; ?>" onkeypress="return isNumberKey(event)" required/></div>
                                    </div>
                                    <?php echo form_error('price', '<div class="error" style="color:red;">', '</div>'); ?>
                                </div>

                                <div class="row">
                                    <div class="col-md-12"><h5 class="sec-title"  style="margin-top:20px;border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Note</h5></div>
                                    <div class="col-md-12">
                                        <textarea id="editor1" name="editor" style="border:none;"><?php echo $ls['description']; ?></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12"><h5 class="sec-title" style="margin-top:20px;border-bottom: 1px solid #efefef;padding-bottom: 5px;color: #36b9cc;">Amenities</h5></div>
                                    <div class="col-md-12">
                                        <a href="<?php echo base_url(); ?>dashboard/list_features/<?php echo $ls['listingId']; ?>" class="btn btn-primary">Edit Amenities</a>
                                    </div>
                                </div>
                            </div>                                                              
                        
                        <div class="col-md-12" style="margin-top:40px;">
                            <div class="form-group">                
                                <button type="submit" class="btn btn-info"  name="add" id="add">Edit Service</button>
                                <a href="<?php echo base_url(); ?>listing/list_services" class="btn btn-primary">Go Back</a>
                            </div>

                        </div>
                        </div>
                        <?php echo form_close(); ?>

                        <?php
                    }
                }
                ?>
            </div>

</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
</div>

<!--old development-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>global/Multifile/js/jquery.MultiFile.js"></script>


<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>
                                                    CKEDITOR.replace('editor1', {
                                                        toolbar: [
                                                            {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript']},
                                                            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                                                            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']}
                                                        ]
                                                    });

</script>

<?php
$this->load->view('page/temp/portal_footer');
?>