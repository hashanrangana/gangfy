<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/portal_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Customer Details</h1>
            </div>

            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <?php
                            if (isset($prof)) {
                                foreach ($prof as $pd) {
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?php if (!empty($pd['image'])) { ?>
                                                <img src="<?php echo base_url(); ?><?php echo $pd['image']; ?>"  style="width: 150px;height: 150px;">
                                            <?php } else { ?>
                                                <img src="<?php echo base_url(); ?>assets/img/user-profile.png" style="width: 150px;height: 150px;">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="fname">First Name</label><br>
                                            <label for="fname"><?php echo $pd['name']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="lname">Last Name</label><br>
                                            <label for="lname"><?php echo $pd['lname']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="cname">Company Name</label><br>
                                            <label for="cname"><?php echo $pd['companyName']; ?></label><br>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email">Email</label><br>
                                            <label for="email"><?php echo $pd['contactEmail']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pcode">Phone Number</label><br>
                                            <label for="pcode"><?php echo $pd['phonenumber_two']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pnum">Phone Number</label><br>
                                            <label for="pnum"><?php echo $pd['phonenumber_two']; ?></label>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="adone">Address Line One</label><br>
                                            <label for="adone"><?php echo $pd['address1']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="adtwo">Address Line Two</label><br>
                                            <label for="adtwo"><?php echo $pd['address2']; ?></label>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="city">City</label><br>
                                            <label for="city"><?php echo $pd['city']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="region">Region</label><br>
                                            <label for="region"><?php echo $pd['region']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="country">Country</label><br>
                                            <label for="country"><?php echo $pd['country']; ?></label>
                                        </div>
                                    </div>


                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

