<?php if (isset($menuStyle)) { ?>  
    <footer class="py-3 home-footer">
        <div class="container">
            <p class="m-0 text-center text-white"> &copy; Gangfy Networks Inc 2019 |  <a   href="<?php echo base_url(); ?>home/privacy_policy" style="color:#fff;">Privacy policy</a> &  <a  href="<?php echo base_url(); ?>home/terms_and_conditions" style="color:#fff;">Terms & Conditions </a></p>
        </div>
    </footer>

<?php } else { ?>

    <!-- Footer -->
    <footer class="py-3 footer">
        <div class="container">
            <p class="m-0 text-center text-white"> &copy; Gangfy Networks Inc 2019 | <a   href="<?php echo base_url(); ?>home/privacy_policy" style="color:#fff;">Privacy policy</a> &  <a  href="<?php echo base_url(); ?>home/terms_and_conditions" style="color:#fff;" >Terms & Conditions </a></p>
        </div>
        <!-- /.container -->
    </footer>

<?php } ?>



<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script>
    $('.DateFinder').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        clearBtn: true,
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        toggleActive: true
    });
</script>



<script>


    $('#filter-sub-service').hide();
    $('#showSelectedMain').hide();
    $('#requiredSearch').hide();

    var google;
    var maps;
    var selectedService = '';
    var selectedSubService = '';
    var service = '';
    var lcity = '';
    var newVal = '';
    var latGoogle = '';
    var lngGoogle = '';
    var selectedServiceHome = '';

    $(document).ready(function () {

        $('#filter-service').change(function () {

            $('#filter-sub-service').empty();
            var inputValue = $(this).val();
            var $html = '';
            if (inputValue) {
                $.ajax({
                    url: './listing/get_by_service/' + inputValue,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    error: function () {
                        // $html += '<option value="" id="0"> Select Sub Service</option>';
                        // $('#filter-sub-service').append($html);
                        $('#filter-sub-service').hide();
                        $('#filter-sub-service').empty().append($html);
                    },
                    success: function (data) {
                        if (data) {

                            $html += '';

                            $.each(data, function (key, value) {
                                $html += ' <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox"  checked value="' + value.subServicesId + '" name="subService[]" ><label class="form-check-label" >' + value.subServiceName + '</label></div><br>';
                                // $html += '<option value="'+value.subServicesId+'"> '+value.subServiceName+'</option>';

                            });

                            $('#filter-sub-service').show();
                            $('#filter-sub-service').empty().append($html);
                        } else {
                            $('#filter-sub-service').hide();
                            $('#filter-sub-service').empty().append($html);

                        }
                    }
                });
            } else {
                $('#filter-sub-service').hide();
                $('#filter-sub-service').empty().append($html);
            }
        });

        $('#serviceName').change(function () {
            $('#viewSubAdd').empty();
            var inputValue = $(this).val();
            var $html = '';

            if (inputValue) {
                $.ajax({
                    url: '../listing/get_by_service/' + inputValue,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    error: function () {
                        $html += '<option value=""> Select</option>';
                        $('#viewSubAdd').empty().append($html);

                        // $('#addSubService').hide();
                    },
                    success: function (data) {
                        $html += '';
                        if (data && data.length) {
                            $.each(data, function (key, value) {
                                $html += '<option value="' + value.subServicesId + '"> ' + value.subServiceName + '</option>';

                            });
                            // $('#addSubService').show();
                            $('#viewSubAdd').empty().append($html);
                        } else {
                            $html += '<option value=""> Select</option>';
                            $('#viewSubAdd').empty().append($html);

                            // $('#addSubService').hide();
                        }
                    }
                });
            } else {

            }
        });

        $('#editserviceName').change(function () {
            var inputValue = $(this).val();
            var listingId = $('#listingId').val();
            $('#editSubService').empty();

            var $html = '';
            if (inputValue && listingId) {
                $.ajax({
                    url: '../../listing/get_by_edit_service/' + inputValue + '/' + listingId,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    error: function () {
                        $html += '<option value=""> Select</option>';
                        $('#viewSubedit').empty().append($html);
                    },
                    success: function (data) {
                        $html += '';
                        if (data && data.length) {
                            $.each(data, function (key, value) {
                                if (value != null) {
                                    if (value.selected == true) {
                                        $html += '<option value="' + value.serviceId + '" selected> ' + value.serviceName + '</option>';

                                    } else {
                                        $html += '<option value="' + value.serviceId + '" > ' + value.serviceName + '</option>';
                                    }
                                }

                            });
                            $('#viewSubedit').empty().append($html);
                        } else {
                            $html += '<option value=""> Select</option>';
                            $('#viewSubedit').empty().append($html);
                        }
                    }
                });
            } else {

            }
        });


        $("#errorRequired").hide();
        $("#errorLogin").hide();



    });

    function test(eve) {
        selectedSubService = '';
        $('#showSelectedMain').hide();
        $('#filter-sub-service').hide();
        $("#filter-sub-service").val('');
        $('#filter-service').show();

    }

    function closeRequired(eve) {
        $('#requiredSearch').hide();

    }

    $('#filter-service').on('input', function (e) {
        selectedService = e.target.selectedOptions[0].id;
    });


    $('#find-facility').click(function (e) {

        var selectedSubService = [];
        $("#filter-sub-service").find("input:checked").each(function (i, ob) {
            selectedSubService.push($(ob).val());
        })

        $('#requiredSearch').hide();

        var modelText = 'Search Results:';
        var displayText = '';
        var $html = '';
        let service = $("#filter-service").val();

        lcity = $("#pac-input").val();
        displayText = modelText + ' ' + selectedService + ' + ' + lcity;
        // }
        $('#searchTitleModel').html(displayText);
        $("#searchView").empty();


        if (service && lcity && selectedSubService.length) {

            $.ajax({
                url: 'listing/search',
                type: 'GET',
                data: {service: service, subService: selectedSubService, lcity: lcity},
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function () {
                    $html += '  <div class="row"> <div class="col-md-12">';
                    $html += '   <div class="panel panel-primary"><div class="panel-heading">No service available</div> </div>';
                    $html += '  </div> </div>';
                    $('#searchView').append($html);

                    $('#find-facility-model').modal('show');
                },
                success: function (data) {
                    if (data && data.length) {
                        $html += '  <div class="row"> <div class="col-md-12"><div class="row">';
                        for (var key in data) {
                            $html += '<div class="col-md-6"><div class="cn-content">';
                            $html += '<img class="img-responsive branch-img" src="<?php echo base_url(); ?>' + data[key].branch_logo + '">';
                            $html += '<h5 class="cn-name"><a href="<?php echo base_url(); ?>listing/branch/' + data[key].branch_id + '/' + data[key].branch_name.replace(/\s/g, "-") + '" >' + data[key].branch_name + '</a></h5>';
                            $html += '<p class="cn-desc">' + data[key].branch_desc.substring(0, 20) + (data[key].branch_desc >= 20 ? "..." : "") + '</p>';
                            $html += '<p class="cn-location"><i class="fas fa-map-marker-alt"></i> Location: ' + data[key].location + '</p>';
                            $html += ' <img src="<?php echo base_url(); ?>assets/images/star-rating.png" class="cn-rating">';
                            $html += '</div></div>';
                        }
                        $html += '  </div> </div ></div>';

                        $('#searchView').append($html);
                        $('#find-facility-model').modal('show');

                    } else {
                        $html += '  <div class="row"> <div class="col-md-12">';
                        $html += '   <div class="panel panel-primary"><div class="panel-heading">No service available</div> </div>';
                        $html += '  </div> </div>';
                        $('#searchView').append($html);

                        $('#find-facility-model').modal('show');

                    }

                }
            });
        } else {
            $('#requiredSearch').show();

        }



    });

</script>

<script>

    $(function () {
        $("#btnAdd").bind("click", function () {
            var div = $("<div class='row' id='dyn_row'>");
            var bid = $('#bid').val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "dashboard/get_services",
                dataType: 'json',
                data: {"bid": bid},
                success: function (res) {
                    var oval = [];
                    if (res)
                    {
                        for (var i = 0; i < res.length; i++) {
                            oval.push('<option value=' + res[i].listingId + '>' + res[i].listingName + '</option>');
                        }
                        div.html(GetDynamicTextBox(oval));
                        $("#addServices").append(div);
                    }

                }
            });

        });

        $("body").on("click", ".remove", function () {
            $(this).closest("#dyn_row").remove();
        });

    });

    function GetDynamicTextBox(value) {
        return '<div class="col-md-2"></div><div class="col-md-3"><div class="form-group"><select class="form-control" id="service" name="service[]" required><option value="">Select Services</option>' + value + '</select></div></div><div class="col-md-2"><div class="form-group"><input name="sfrom[]" id="sfrom" type="time" class="form-control" required></div></div><div class="col-md-2"><div class="form-group"><input name="sto[]" id="sto" type="time" class="form-control" required></div></div><div class="col-md-2"><div class="form-group"><input name="duration[]" id="sto"  type="text" placeholder="1.5" class="form-control" required></div></div><div class="col-md-1"  style="text-align: center;"><div class="form-group"><button type="button" value="Remove" class="remove btn btn-danger"><i class="fa fa-trash-alt"></i></button></div></div></div>';

    }

    function setListing(id) {
        $('#bid').val(id);
    }
</script>

<script>
    $(document).ready(function () {
        $("#ccode").change(function (event) {
            event.preventDefault();
            var sid = $('#sid').val();
            var ccode = $('#ccode').val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "dashboard/validate_coupon",
                dataType: 'json',
                data: {"sid": sid, "ccode": ccode},
                success: function (res) {
                    $("#cval").empty();
                    if (res.st === 'valid')
                    {
                        $('#ct').val(res.ct);
                        $('#cv').val(res.cv);
                        $("#cval").append("<label style='color: green;'>Coupon Applied</label>");
                        checkTotal();

                    } else {
                        $("#cval").append("<label style='color: red;'>Invalid Coupon</label>");
                        $("#ccode").val("");
                        $('#ct').val("");
                        $('#cv').val("");
                        checkTotal();
                    }
                }
            });
        });
    });

    document.getElementById("total").disabled = true;
<?php
if ($this->session->userdata('price')) {
    $val = $this->session->userdata('price');
}
?>
    function checkTotal() {
        document.listForm.total.value = '';
        var sum = 0;
        var fval = 0;
        var dval = 0;
        var ct = $('#ct').val();
        var cv = $('#cv').val();
        if (document.listForm.tslot.length > 1) {
            for (i = 0; i < document.listForm.tslot.length; i++) {
                if (document.listForm.tslot[i].checked) {
                    sum = (<?php echo $val; ?> * parseInt(document.listForm.tslot[i].value));
                    fval += sum;
                }
            }
            if (ct === '%') {
                dval = (sum * (cv / 100));
                fval = (sum - dval);
                document.listForm.total.value = fval;
            } else if (ct === '$') {
                fval = (sum - cv);
                document.listForm.total.value = fval;
            } else {
                document.listForm.total.value = fval;
            }

        } else {
            if (document.getElementById('tslot').checked === true) {
                sum = (<?php echo $val; ?> * parseInt(document.listForm.tslot.value));
                if (ct === '%') {
                    dval = (sum * (cv / 100));
                    fval = (sum - dval);
                    document.listForm.total.value = fval;
                } else if (ct === '$') {
                    fval = (sum - cv);
                    document.listForm.total.value = fval;
                } else {
                    document.listForm.total.value = sum;
                }
            } else {
                document.listForm.total.value = 0;
            }
        }
    }

    function bookNow() {
        if (document.getElementById('tslot').checked === false) {
        }
    }

</script>



</body>

</html>
