<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo isset($title) ? $title : 'Gangfy Reservation – Best Value. Best chioce.'; ?> </title>


        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets/css/full-slider.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css">  
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker3.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/theme.css?ver=<?php echo rand(); ?>" rel="stylesheet"> 

        <script>

            function initMap() {
                var map = new google.maps.Map(document.getElementById('searchMap'), {
                    center: {lat: -33.8688, lng: 151.2195},
                    zoom: 13,
                    mapTypeId: 'roadmap'

                });

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
//                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                map.addListener('bounds_changed', function () {
                    searchBox.setBounds(map.getBounds());
                });

                var markers = [];
                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    // Clear out the old markers.
                    markers.forEach(function (marker) {
                        marker.setMap(null);
                    });
                    markers = [];

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {
                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        markers.push(new google.maps.Marker({
                            map: map,
                            icon: icon,
                            title: place.name,
                            position: place.geometry.location
                        }));

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }
                    });
                    map.fitBounds(bounds);
                });
            }

        </script>

    </head>

    <body>
        <!-- Navigation -->
        <?php if (isset($menuStyle)) { ?>  
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
            <?php } else { ?>
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <?php } ?>
                <div class="container">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">GangFY <span>Booking System</span></a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto navbar-dashboard">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>">
                                    <i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  href="<?php echo base_url(); ?>home/about_us">About Us </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  href="<?php echo base_url(); ?>home/contact_us">Contact Us </a>
                            </li>
                            <?php if ($this->session->userdata('user_id')) { ?>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php if ($this->session->userdata('user_image')) { ?>
                                            <img class="pro-icon" src="<?php echo base_url() . $this->session->userdata('user_image'); ?>"> <?php echo $this->session->userdata('user_name'); ?>
                                        <?php } else { ?>
                                            <img class="pro-icon" src="<?php echo base_url(); ?>assets/img/user-profile.png"> <?php echo $this->session->userdata('user_name'); ?>
                                        <?php } ?>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" target="_blank"  href="<?php echo base_url(); ?>welcome">My Dashboard <span class="sr-only">(current)</span></a>
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>home/logout">Sign out</a>
                                </li>
                            <?php } else { ?>

                                <li class="nav-item">
                                    <a class="nav-link nav-btn" href="<?php echo base_url(); ?>home/login">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-btn" href="http://www.gangfy.com/snetui/register" target="_blank">Register</a>
                                </li>

                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </nav>


