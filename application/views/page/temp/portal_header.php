<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Gangfy Reservation - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/vendor/fontawesome-free/css/all.min.css?ver=<?php echo rand();?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>assets/css/sb-admin-2.css?ver=<?php echo rand();?>" rel="stylesheet">
 

  <script>

          function initMap() {
            var input = document.getElementById('pac-input');
            // var inputpac = document.getElementById('pac');
            var options = {
            types: ['(cities)'],
            };

              var geocoder = new google.maps.Geocoder();

map = new google.maps.Map(document.getElementById('searchMap'), {
center: {lat: 0, lng: 0},
zoom: 12
});
geocoder.geocode({'address': ""}, function(results, status) {
    if (status === 'OK') {
      map.setCenter(results[0].geometry.location);
    } else {
    //   alert('Geocode was not successful for the following reason: ' + status);
    }
  });

            var autocomplete = new google.maps.places.Autocomplete(input,options);
            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
        
        
        
        
            autocomplete.addListener('place_changed', function() {
              var place = autocomplete.getPlace();
             
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }
        
          
              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }
              map.setCenter(place.geometry.location);

            });
        
        
          }
                </script>
 <!-- datatables-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <!--/ datatables-->
  
  <!-- TimePicker-->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.timeselector.css" />
  <!--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>-->
  <script src="<?php echo base_url();?>assets/css/jquery.timeselector.js"></script>
  <script>
		$(function() {
        $('.TimeSelect').timeselector({ min: '00:00', max: '23:59' })
		});
  </script>
  <!--/ TimePicker-->
<style>
    .kv-fileinput-caption,.fileinput-upload-button{
        display: none !important;
    }
    @media screen and (max-width: 600px) {
  .logo a {
    width: 80%;
  }
}
</style>
<link href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker3.css" rel="stylesheet">