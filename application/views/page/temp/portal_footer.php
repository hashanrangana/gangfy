<div id="feedback" class="">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
        Feedback
    </button>
</div>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideout" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">System Feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea name="feedback" name="fbtxt" id="fbtxt" class="form-control" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="fbbtn" class="btn btn-primary">Send Feedback</button>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        $("#fbbtn").click(function (event) {
            event.preventDefault();
            var fbk = $('#fbtxt').val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>dashboard/send_feedback",
                dataType: 'json',
                data: {"fdback": fbk},
                success: function (res) {
                    if (res)
                    {
                        location.reload();
                    }
                }
            });
        });
    });

</script>

  <!-- Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
              <span>© GangFY Networks Inc 2019 | <a href="<?php echo base_url(); ?>home/privacy_policy" target="_blank" style="color:#fff;"> Privacy policy </a> & <a href="<?php echo base_url(); ?>home/terms_and_conditions" target="_blank" style="color:#fff;">Terms & Conditions</a></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->


  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo base_url();?>home/logout">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->

  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js?ver=<?php echo rand();?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js?ver=<?php echo rand();?>" type="text/javascript"></script>
  <!-- Core plugin JavaScript-->
      <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js?ver=<?php echo rand();?>" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js?ver=<?php echo rand();?>"></script>
  <script src="<?php echo base_url(); ?>assets/fileupload/js/fileinput.js?ver=<?php echo rand();?>" type="text/javascript"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/js/sb-admin-2.min.js?ver=<?php echo rand();?>"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url();?>assets/vendor/chart.js/Chart.min.js?ver=<?php echo rand();?>"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url();?>assets/js/demo/chart-area-demo.js?ver=<?php echo rand();?>"></script>
  <script src="<?php echo base_url();?>assets/js/demo/chart-pie-demo.js?ver=<?php echo rand();?>"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_js/jquery.qrcode.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/admin_js/qr_jen.js"></script>


<script>
    $(document).ready(function () {
        var table = $('#example').DataTable({
            //fixedHeader: true
        });
    });

</script>
<script>
    $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

  <script type="text/javascript">


    $('#filter-sub-service').hide();
    $('#showSelectedMain').hide();
    $('#requiredSearch').hide();
    var google;
    var maps;
    var selectedService = '';
    var selectedSubService = '';
    var service = '';
    var lcity = '';
    var newVal = '';
    var latGoogle = '';
    var lngGoogle = '';
    var selectedServiceHome = '';

    $(document).ready(function () {

        $('#filter-service').change(function () {

            $('#filter-sub-service').empty();
            var inputValue = $(this).val();
            var $html = '';
            if (inputValue) {
                $.ajax({
                    url: './listing/get_by_service/' + inputValue,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    error: function () {
                        // $html += '<option value="" id="0"> Select Sub Service</option>';
                        // $('#filter-sub-service').append($html);
                        $('#filter-sub-service').hide();
                        $('#filter-sub-service').empty().append($html);
                    },
                    success: function (data) {
                        if (data) {

                            $html += '';
                            $.each(data, function (key, value) {
                                $html += ' <div class="form-check form-check-inline"><input class="form-check-input" type="checkbox"  checked value="' + value.subServicesId + '" name="subService[]" ><label class="form-check-label" >' + value.subServiceName + '</label></div><br>';
                                // $html += '<option value="'+value.subServicesId+'"> '+value.subServiceName+'</option>';

                            })

                            $('#filter-sub-service').show();
                            $('#filter-sub-service').empty().append($html);
                        } else {
                            $('#filter-sub-service').hide();
                            $('#filter-sub-service').empty().append($html);
                        }
                    }
                });
            } else {
                $('#filter-sub-service').hide();
                $('#filter-sub-service').empty().append($html);
            }
        });
        $('#serviceName').change(function () {
            $('#viewSubAdd').empty();
            var inputValue = $(this).val();
            var $html = '';
            if (inputValue) {
                $.ajax({
                    url: '../listing/get_by_service/' + inputValue,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    error: function () {
                        $html += '<option value=""> Select</option>';
                        $('#viewSubAdd').empty().append($html);
                        // $('#addSubService').hide();
                    },
                    success: function (data) {
                        $html += '';
                        if (data && data.length) {
                            $.each(data, function (key, value) {
                                $html += '<option value="' + value.subServicesId + '"> ' + value.subServiceName + '</option>';
                            })
                            // $('#addSubService').show();
                            $('#viewSubAdd').empty().append($html);
                        } else {
                            $html += '<option value=""> Select</option>';
                            $('#viewSubAdd').empty().append($html);
                            // $('#addSubService').hide();
                        }
                    }
                });
            } else {

            }
        });
        $("#errorRequired").hide();
        $("#errorLogin").hide();
    });
    function test(eve) {
        selectedSubService = '';
        $('#showSelectedMain').hide();
        $('#filter-sub-service').hide();
        $("#filter-sub-service").val('');
        $('#filter-service').show();
    }

    function closeRequired(eve) {
        $('#requiredSearch').hide();
    }

    $('#filter-service').on('input', function (e) {
        selectedService = e.target.selectedOptions[0].id;
    })

</script>

<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
    <script>
    $('.DateFinder').datepicker({
    format: "yyyy-mm-dd",
    todayBtn: "linked",
    clearBtn: true,
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
    });
    </script>


</body>

</html>
