<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo isset($title) ? $title : 'Gangfy Reservation – Best Value. Best chioce.' ; ?> </title>


    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/full-slider.css" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css">  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/css/theme.css" rel="stylesheet"> 

  <script>

          function initMap() {
            var input = document.getElementById('pac-input');
            // var inputpac = document.getElementById('pac');
            var options = {
            types: ['(cities)'],
            };

              var geocoder = new google.maps.Geocoder();

map = new google.maps.Map(document.getElementById('searchMap'), {
center: {lat: 0, lng: 0},
zoom: 12
});
geocoder.geocode({'address': ""}, function(results, status) {
    if (status === 'OK') {
      map.setCenter(results[0].geometry.location);
    } else {
    //   alert('Geocode was not successful for the following reason: ' + status);
    }
  });

            var autocomplete = new google.maps.places.Autocomplete(input,options);
            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
        
        
        
        
            autocomplete.addListener('place_changed', function() {
              var place = autocomplete.getPlace();
             
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }
        
          
              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }
              map.setCenter(place.geometry.location);

            });
        
        
          }
                </script>

  </head>

  <body>
  <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"> -->

    <!-- Navigation -->
    <?php if(isset($menuStyle)){?>  
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <?php }else {?>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <?php }?>
      <div class="container">
        <a class="navbar-brand" href="<?php echo base_url();?>">GangFY <span>Reservation</span></a>



        
       
        <div class="nav-right" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>">
              <i class="fas fa-home"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link"  href="<?php echo base_url();?>home/about_us">About us </a>
             </li>
             <li class="nav-item">
              <a class="nav-link"  href="<?php echo base_url();?>home/contact_us">Contact us </a>
             </li>
            <?php if(isset($this->session->logged_in) && isset($this->session->user_id) && $this->session->user_type == 'vendor') {?>
          
      <!-- <li class="nav-item">
        <a class="nav-link" ><i class="fas fa-bell"></i></a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link"  href="<?php echo base_url();?>dashboard/vendor_dashbaord">Dashboard <span class="sr-only">(current)</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>dashboard/vendor_bookings">Bookings</a>
      </li>
          <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Services
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>listing/all_listing">ALL Services</a>
          <a class="dropdown-item" href="<?php echo base_url();?>listing/add_listing">Add Services</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo base_url();?>global/default.png" class="pro-icon"> <?php echo $this->session->user_name?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>dashboard/vendor_settings">Settings</a>
          <a class="dropdown-item" href="<?php echo base_url();?>home/logout">Sign out</a>
      </li>
           <?php } else if(isset($this->session->logged_in) && isset($this->session->user_id) && $this->session->user_type == 'host'){?>
            <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>dashboard/my_bookings">Bookings</a>
      </li>
            
            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php if ( $this->session->user_image) {?>
          <img src="<?php echo base_url();?>global/uploads/profile/<?php echo $this->session->user_image?>" class="pro-icon"> 
        <?php } else {?>
          <img src="<?php echo base_url();?>global/default.png" class="pro-icon"> 
        <?php } ?>

          <?php echo $this->session->user_name?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>dashboard/host_settings">Settings</a>
          <a class="dropdown-item" href="<?php echo base_url();?>home/logout">Sign out</a>
      </li>

            <?php } else{?>

            <li class="nav-item">
              <a class="nav-link nav-btn" href="<?php echo base_url();?>home/login">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-btn" href="<?php echo base_url();?>home/register">Register</a>
            </li>
            <?php }?>

          </ul>
        </div>
      </div>
    </nav>


    