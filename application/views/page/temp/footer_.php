








    <?php if(isset($menuStyle)){?>  
        <footer class="py-3 home-footer">
      <div class="container">
        <p class="m-0 text-center text-white"> &copy; Gangfy Reservation 2019 |  <a   href="<?php echo base_url();?>home/privacy_policy">Privacy policy</a> &  <a  href="<?php echo base_url();?>home/terms_and_conditions">Terms & Conditions </a></p>
      </div>
    </footer>

    <?php }else {?>

      <!-- Footer -->
      <footer class="py-3 footer">
      <div class="container">
        <p class="m-0 text-center text-white"> &copy; Gangfy Reservation 2019 | <a   href="<?php echo base_url();?>home/privacy_policy">Privacy policy</a> &  <a  href="<?php echo base_url();?>home/terms_and_conditions">Terms & Conditions </a></p>
      </div>
      <!-- /.container -->
    </footer>
    
    <?php }?>


 
 <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery/jquery.dataTables.min.js"></script> -->
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>



        <script type="text/javascript">
           $('#searchDateTime').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		startView: 2,
		forceParse: 0,
        startDate: new Date(),
        format: "yyyy-mm-dd hh:ii"
        });
    
        $('#ListingDateTime').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		startView: 1,
        minuteStep:10,
        viewSelect:'hour',
        startDate: new Date(),
        format: "hh:ii"
        });

         $('#ListingEndDateTime').datetimepicker({
            weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		startView: 1,
        minuteStep:10,
        viewSelect:'hour',
        startDate: new Date(),
        format: "hh:ii"
        });


        
       $('#ListingEndDateTime').datetimepicker().on('changeDate', function(ev){
        const date1 = new Date(ev.date.valueOf());
        const date2 = new Date(localStorage.getItem('listingDate'));
        const diffTime =  Math.abs(date1 - date2) / 36e5;
        let amount = $('#idAmount')[0].innerHTML;

        let final = amount * diffTime;
        $('#amountCal').empty().append( Math.round(final));

        // var maxDate = new Date(ev.date.valueOf());
        // $('#ListingDateTime').datepicker('setEndDate', maxDate);
       });

       $('#ListingDateTime').datetimepicker().on('changeDate', function(ev){
        var minDate = new Date(ev.date.valueOf());
        localStorage.setItem('listingDate',moment(minDate).format("YYYY-MM-DD HH:mm"))
        $('#showlistingDate').empty().append('Date '+moment(minDate).format("YYYY-MM-DD"));

        $('#ListingEndDateTime').datetimepicker('setStartDate', minDate);
        // $('#ListingEndDateTime').datetimepicker('setDate', minDate); // <--THIS IS THE LINE ADDED
       });

        </script>

<script type="text/javascript">


$('#filter-sub-service').hide();
$('#showSelectedMain').hide();
$('#requiredSearch').hide();

 var google;
 var maps;
var selectedService = '';
var selectedSubService = '';
var service = '';
var lcity = '';
var newVal = '';
var latGoogle = '';
var lngGoogle = '';
var selectedServiceHome = '';
var dataListing = localStorage.getItem('listingDate');
if(dataListing){
    $('#ListingDateTime').val(moment(dataListing).format("HH:mm"));
    $('#ListingDateTime').datetimepicker('setStartDate', dataListing);
    $('#ListingEndDateTime').datetimepicker('setStartDate', dataListing);

    $('#showlistingDate').empty().append('Date '+moment(dataListing).format("YYYY-MM-DD"));
}else{
    
    let day = new Date();
    localStorage.setItem('listingDate',moment(day).format("YYYY-MM-DD HH:mm"))
    $('#ListingDateTime').val(moment(day).format("HH:mm"));

    $('#ListingDateTime').datetimepicker('setStartDate', moment(day).format("YYYY-MM-DD HH:mm"));
    $('#ListingEndDateTime').datetimepicker('setStartDate', moment(day).format("YYYY-MM-DD HH:mm"));

    $('#showlistingDate').empty().append('Date '+moment(day).format("YYYY-MM-DD"));
}



$(document).ready(function(){

 $('#filter-service').change(function(){
     if($(this)[0].selectedOptions[0].id != 0){
        selectedServiceHome ='Selected Service - '+$(this)[0].selectedOptions[0].id+' <a onclick = "test(this)"><i class="fa fa-times" aria-hidden="true" style="background:#4bd6eb;padding:5px;cursor: pointer; " title="Go Back" ></i></a>';
        $('#showSelectedMain').show();

        $('#showSelectedMain').empty().append(selectedServiceHome);

     }
    $('#filter-sub-service').empty();
      var inputValue = $(this).val();
      var $html ='';
      if(inputValue){
        $.ajax({
           url: './listing/get_by_service/'+inputValue,
           type: 'GET',
           contentType: 'application/json; charset=utf-8',
           dataType: "json",
           error: function() {
            $html += '<option value="" id="0"> Select Sub Service</option>';
            $('#filter-sub-service').append($html);
            $('#filter-sub-service').hide();
           },
           success: function(data) {
               if(data){

                $html += '<option value="" id="0"> Select Sub Service</option>';

                $.each( data, function( key, value ) {
                    $html += '<option value="'+value.subServicesId+'"> '+value.subServiceName+'</option>';

                })
                $('#filter-service').hide();

                $('#filter-sub-service').show();
                $('#filter-sub-service').empty().append($html);
               }else{
                $html += '<option value="" id="0"> Select Sub Service</option>';
                $('#filter-sub-service').hide();
                $('#filter-sub-service').empty().append($html);

               }
           }
        });
      }else{
        $('#filter-sub-service').hide();
        $('#filter-sub-service').empty().append($html);
      }
  });

 $('#serviceName').change(function(){
    $('#viewSubAdd').empty();
      var inputValue = $(this).val();
      var $html ='';

      if(inputValue){
        $.ajax({
           url: '../listing/get_by_service/'+inputValue,
           type: 'GET',
           contentType: 'application/json; charset=utf-8',
           dataType: "json",
           error: function() {
            $html += '<option value=""> Select</option>';
            $('#viewSubAdd').empty().append($html);

            // $('#addSubService').hide();
           },
           success: function(data) {
            $html +='';
               if(data && data.length){
                $.each( data, function( key, value ) {
                    $html += '<option value="'+value.subServicesId+'"> '+value.subServiceName+'</option>';

                })
                // $('#addSubService').show();
                $('#viewSubAdd').empty().append($html);
               }else{
                $html += '<option value=""> Select</option>';
                $('#viewSubAdd').empty().append($html);

                // $('#addSubService').hide();
               }
           }
        });
      }else{

      }
  });

 $('#editserviceName').change(function(){
      var inputValue = $(this).val();
      var listingId = $('#listingId').val();
      $('#editSubService').empty();

      var $html ='';
      if(inputValue && listingId){
        $.ajax({
           url: '../../listing/get_by_edit_service/'+inputValue+'/'+listingId,
           type: 'GET',
           contentType: 'application/json; charset=utf-8',
           dataType: "json",
           error: function() {
            $html += '<option value=""> Select</option>';
                $('#viewSubedit').empty().append($html);
           },
           success: function(data) {
            $html +='';
               if(data && data.length){
                $.each( data, function( key, value ) {
                if(value != null){
                    if( value.selected == true){
                        $html += '<option value="'+value.serviceId+'" selected> '+value.serviceName+'</option>';

                    }else{
                        $html += '<option value="'+value.serviceId+'" > '+value.serviceName+'</option>';
                    }
                }
                 
                })
                $('#viewSubedit').empty().append($html);
               }else{
                $html += '<option value=""> Select</option>';
                $('#viewSubedit').empty().append($html);
               }
           }
        });
      }else{

      }
  });


$("#errorRequired").hide();
 $("#errorLogin").hide();



});

function test(eve){
    selectedSubService = '';
    $('#showSelectedMain').hide();
    $('#filter-sub-service').hide();
    $("#filter-sub-service").val('');
    $('#filter-service').show();

}

function closeRequired(eve){
    $('#requiredSearch').hide();

}

$('#filter-service').on('input',function(e){
    selectedService = e.target.selectedOptions[0].id;
})


$('#filter-sub-service').on('input',function(e){
    selectedSubService = e.target.selectedOptions[0].text;
})

    $('#find-facility').click(function(e) {
        $('#requiredSearch').hide();

       var modelText ='Search Results:';
       var displayText = '';
       var $html ='';
        let service = $("#filter-service").val();
        let subService = $("#filter-sub-service").val();
        let searchDate = $("#searchDateTime").val();
        let lcity = $("#pac-input").val();


        if(service && lcity && subService){
            displayText = modelText + selectedService +' - '+ selectedSubService;

        }else if((lcity == '' || lcity) && service == '' && subService){
                // service = 'all';
                displayText = modelText + selectedSubService;
        }else if((lcity == '' || lcity) && service && subService == ''){
            // subService = 'all';
            displayText = modelText + selectedService;
        }else{
            // service = 'all';
            // lcity = 'all';
            // subService = 'all';
            displayText = modelText  + '  All Service';
        }
        console.log(displayText)
        $('#searchTitleModel').html(displayText);
        $( "#searchView" ).empty();

        if(!searchDate){
            searchDate = null;
        }
        if(service && lcity && subService && searchDate){
            localStorage.setItem('listingDate',searchDate);

            $.ajax({
           url: 'listing/search',
           type: 'GET',
           data: { searchDate: searchDate,service:service,subService:subService,lcity:lcity },
           contentType: 'application/json; charset=utf-8',
           dataType: "json",
           error: function() {
            $html += '  <div class="row"> <div class="col-md-12">';
                $html += '   <div class="panel panel-primary"><div class="panel-heading">No service available</div> </div>';
                $html += '  </div> </div>';
                $('#searchView').append($html);

                $('#find-facility-model').modal('show');
           },
           success: function(data) {
               if(data && data.length){
                   $html += '  <div class="row"> <div class="col-md-12"><div class="row">';
                for (var key in data) {
                    $html += '<div class="col-md-6"><div class="cn-content"><img src="<?php echo base_url();?>assets/images/upload/company-logo.png"><h5 class="cn-name"><a href="<?php echo base_url();?>home/details/'+data[key].listingId+'/'+data[key].listingName+'" target="_blank">'+data[key].listingName+'</a></h5><p class="cn-desc">'+data[key].description.substring(0,15)+(data[key].description >= 15 ? "..." : "")+'</p> <p class="cn-location"><i class="fas fa-map-marker-alt"></i> Location: '+data[key].location+'</p> <img src="<?php echo base_url();?>assets/images/star-rating.png" class="cn-rating"> <a class="btn cn-btn" href="<?php echo base_url();?>home/details/'+data[key].listingId+'/'+data[key].listingName+'" >Select</a>  </div></div>';
                }
                $html += '  </div> </div ></div>';

                $('#searchView').append($html);
                $('#find-facility-model').modal('show');

               }else{
                $html += '  <div class="row"> <div class="col-md-12">';
                $html += '   <div class="panel panel-primary"><div class="panel-heading">No service available</div> </div>';
                $html += '  </div> </div>';
                $('#searchView').append($html);

                $('#find-facility-model').modal('show');

               }

           }
        });
        }else{
            $('#requiredSearch').show();

        }

      

    });


        $('#book-listing').click(function(e) {
            var Sessionvalue = "<?php echo $this->session->userdata('user_name')?>";
            $("#errorLogin").css({ 'display' : 'none'});
            $("#errorRequired").css({ 'display' : 'none'});
            $("#errorBooking").css({ 'display' : 'none'});
            $("#successBooking").css({ 'display' : 'none'});

            var ListingDateVal = $("#ListingDateTime").val();
            var ListingEndDateVal = $("#ListingEndDateTime").val();
            let amount = $('#amountCal')[0].innerHTML;

            var listId = $("#list-id").val();

            if(Sessionvalue == ''){
                $("#errorLogin").css({ 'display' : 'block'});

            }else if( ListingDateVal && ListingEndDateVal && listId){
                $('#showlistingDate').empty().append('Date '+moment(dataListing).format("YYYY-MM-DD"));

                ListingDateVal = moment(dataListing).format("YYYY-MM-DD")+' '+ ListingDateVal;
                ListingEndDateVal = moment(dataListing).format("YYYY-MM-DD")+' '+ ListingEndDateVal;
                $.ajax({
    url: '../../../listing/booking',    
    type: 'POST',
    data: {ListingDateVal : ListingDateVal,ListingEndDateVal : ListingEndDateVal,listId:listId,amount:amount},
    dataType: 'text',
    beforeSend: function(){
        // initiate some spinner or loading infobox
        // to inform the user about the AJAX request
    },
    success: function(response){
        if(response == 1){
               $("#successBooking").css({ 'display' : 'block'});
        }else{
        $("#errorBooking").css({ 'display' : 'block'});
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        $("#errorBooking").css({ 'display' : 'block'});
    },
    complete: function(){
        // close the spinner or loading box
    }
});
            }else{
                $("#errorRequired").css({ 'display' : 'block'});

            }

        })


</script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#editor").editor({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>
    
  </body>

</html>
