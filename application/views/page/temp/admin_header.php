<html>

    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin_css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin_css/admin.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin_css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin_css/main-style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin_css/all-skins.css">
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_js/tree1.js"></script>
        <script  type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script  type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    </head>


    <body style="margin:0px !important;">
      <div class="wrapper">

        <header class="main-header">
          <!-- Logo -->
          <a href="<?php echo base_url();?>admin/main_view" class="logo" style="background:#29B6F6;color:#fff;">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b>Panel</span>
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
              <span class="sr-only">Toggle navigation</span>
            </a> -->

            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu" >
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#29B6F6;">
                    <span class="hidden-xs">Admin</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                      <div class="pull-right">
                        <a href="<?php echo base_url();?>admin/logout" style="color:#29B6F6;">Sign out</a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
          <!-- sidebar: style can be found in sidebar.less -->
          <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">

            </div>
            <!-- search form -->
            <!-- <form action="#" method="get" class="sidebar-form">
              <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                      <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree" style="color:#fff;">
          
              <li class="treeview">
                <a  style="color:#fff;">
                 <span>Orders</span>
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/main_view"  style="color:#29b6f6;"> View all</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a  style="color:#fff;">
                 <span>Host</span>
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/all_host"  style="color:#29b6f6;"> View all</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a  style="color:#fff;">
                 <span>Vendor</span>
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/all_vendor"  style="color:#29b6f6;"> View all</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a  style="color:#fff;">
                 <span>Main Services</span>
                  <!-- <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span> -->
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/all_service"  style="color:#29b6f6;"> View all</a></li>
                  <li><a href="<?php echo base_url();?>admin/create_service"  style="color:#29b6f6;">Create Main Services</a></li>
                </ul>
              </li>

            
            <li class="treeview">
                <a  style="color:#fff;">
                 <span>Sub Services</span>
                  <!-- <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span> -->
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/all_sub_service"  style="color:#29b6f6;"> View all</a></li>
                  <li><a href="<?php echo base_url();?>admin/create_sub_service"  style="color:#29b6f6;">Create Sub Services</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a  style="color:#fff;">
                 <span>Add Main to Sub</span>
                  <!-- <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span> -->
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/add_main_sub_service"  style="color:#29b6f6;"> View all</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a  style="color:#fff;">
                 <span>Listing Features</span>
                  <!-- <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span> -->
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?php echo base_url();?>admin/all_features"  style="color:#29b6f6;"> View all</a></li>
                  <li><a href="<?php echo base_url();?>admin/create_features"  style="color:#29b6f6;">Create Listing Features</a></li>
                </ul>
              </li>
            </ul>
          </section>
          <!-- /.sidebar -->
        </aside>

        <div class="content-wrapper">
          <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-lg-12 col-xs-12">
