</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-gray topbar mb-4 static-top shadodashboardw">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">

            <div class="sidebar-brand-text mx-3"><img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logo.png"/></div>
        </a>
        <span style="color:#fff;margin-top: -25px;">Beta 1.0v</span>

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>
        <?php if ($this->session->userdata('user_type') == 'customer') { ?>
            Want to become a Vendor?&nbsp; <a href="<?php echo base_url(); ?>dashboard/becomevendor"> Click here</a>
        <?php } ?>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                </a>
                <!-- Dropdown - Messages -->
                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <?php if ($this->session->userdata('user_type') == 'customer') { ?>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-star fa-fw"></i>
                        <!-- Counter - Alerts -->
                    </a>
                    <!-- Dropdown - Alerts -->
                    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                            My Favorites 
                        </h6>
                        <?php
                        if ($this->session->userdata('fav')) {
                            $fav = $this->session->userdata('fav');
                            foreach ($fav as $fd) {
                                $url = base_url() . 'home/details/' . $fd['listingId'] . '/' . str_replace(" ", "-", $fd['listingName']);
                                ?>
                                <a class="dropdown-item d-flex align-items-center" target="_blank" href="<?php echo $url; ?>">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-primary">
                                            <i class="fas fa-star text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">Create at <?php echo date('d F Y', strtotime($fd['fdate'])); ?></div>
                                        <span class="font-weight-bold"><?php echo $fd['listingName']; ?></span>
                                    </div>
                                </a>
                                <?php
                            }
                        } else {
                            ?>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="mr-3">
                                    <div class="icon-circle bg-primary">
                                        <i class="fas fa-star text-white"></i>
                                    </div>
                                </div>
                                <div>
                                    <div class="small text-gray-500"><?php echo date('d F Y'); ?></div>
                                    <span class="font-weight-bold">You dont have Favorites </span>
                                </div>
                            </a>
                            <?php
                        }
                        ?>
                        <!--<a class="dropdown-item text-center small text-gray-500" href="#">Show All Favorites</a>-->
                    </div>
                </li>
            <?php } ?>
            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bell fa-fw"></i>
                    <!-- Counter - Alerts -->
                    <span class="badge badge-danger badge-counter"><?php
                        if ($this->session->userdata('nofbook')) {
                            echo $this->session->userdata('nofbook');
                        }
                        ?> +</span>
                </a>
                <!-- Dropdown - Alerts -->
                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                    <h6 class="dropdown-header">
                        Alerts Center
                    </h6>
                    <?php if ($this->session->userdata('nofbook')) { ?>
                        <a class="dropdown-item d-flex align-items-center" href="<?php echo base_url(); ?>booking/list_booking">
                            <div class="mr-3">
                                <div class="icon-circle bg-primary">
                                    <i class="fas fa-file-alt text-white"></i>
                                </div>
                            </div>
                            <div>
                                <div class="small text-gray-500"><?php echo date('d F Y'); ?></div>
                                <span class="font-weight-bold">You Have Received New Bookings</span>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if ($this->session->userdata('cbook')) { ?>
                        <a class="dropdown-item d-flex align-items-center" href="<?php echo base_url(); ?>booking/booking_request">
                            <div class="mr-3">
                                <div class="icon-circle bg-primary">
                                    <i class="fas fa-file-alt text-white"></i>
                                </div>
                            </div>
                            <div>
                                <div class="small text-gray-500"><?php echo date('d F Y'); ?></div>
                                <span class="font-weight-bold">You Have Received Custom Bookings</span>
                            </div>
                        </a>
                    <?php } ?>
                    <!--                                        <a class="dropdown-item d-flex align-items-center" href="#">
                                                                <div class="mr-3">
                                                                    <div class="icon-circle bg-success">
                                                                        <i class="fas fa-donate text-white"></i>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <div class="small text-gray-500">December 7, 2019</div>
                                                                    $290.29 has been deposited into your account!
                                                                </div>
                                                            </a>
                                                            <a class="dropdown-item d-flex align-items-center" href="#">
                                                                <div class="mr-3">
                                                                    <div class="icon-circle bg-warning">
                                                                        <i class="fas fa-exclamation-triangle text-white"></i>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <div class="small text-gray-500">December 2, 2019</div>
                                                                    Spending Alert: We've noticed unusually high spending for your account.
                                                                </div>
                                                            </a>-->
                    <!--<a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>-->
                </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-envelope fa-fw"></i>
                    <!-- Counter - Messages -->
                    <span class="badge badge-danger badge-counter"><?php
                        if ($this->session->userdata('banned')) {
                            echo '1';
                        }
                        ?></span>
                </a>
                <!-- Dropdown - Messages -->
                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">
                        Message Center
                    </h6>
                    <?php if ($this->session->userdata('banned')) { ?>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                            <div class="dropdown-list-image mr-3">
                                <img class="rounded-circle" src="<?php echo base_url(); ?>assets/img/ban-64.png" alt="">
                                <div class="status-indicator bg-success"></div>
                            </div>
                            <div class="font-weight-bold">
                                <div class="text-truncate">Your account is temporarily <br> banned, Wait until we get<br> back to you</div>
                                <div class="small text-gray-500">GangFY Team</div>
                            </div>
                        </a>
                    <?php } ?>
                    <?php
                    if ($this->session->userdata('msg')) {
                        $msg = $this->session->userdata('msg');
                        foreach ($msg as $ms) {
                            ?>
                            <a class="dropdown-item d-flex align-items-center" href="<?php echo base_url(); ?>dashboard/list_messages">
                                <div class="dropdown-list-image mr-3">
                                    <img class="rounded-circle" src="<?php echo base_url(); ?>assets/img/message.png" alt="">
                                    <div class="status-indicator bg-success"></div>
                                </div>
                                <div class="font-weight-bold">
                                    <div class="text-truncate">Your have a Message from<br><?php echo $ms['name']; ?></div>
                                </div>
                            </a>
                            <?php
                        }
                    }
                    ?>
                    <!--                    <a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image mr-3">
                                                <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                                                <div class="status-indicator"></div>
                                            </div>
                                            <div>
                                                <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                                                <div class="small text-gray-500">Jae Chun · 1d</div>
                                            </div>
                                        </a>-->
                    <!--                    <a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image mr-3">
                                                <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                                                <div class="status-indicator bg-warning"></div>
                                            </div>
                                            <div>
                                                <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                                                <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                            </div>
                                        </a>-->
                    <!--                    <a class="dropdown-item d-flex align-items-center" href="#">
                                            <div class="dropdown-list-image mr-3">
                                                <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                                                <div class="status-indicator bg-success"></div>
                                            </div>
                                            <div>
                                                <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                                                <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                            </div>
                                        </a>-->
                    <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $this->session->userdata('user_name') ?></span>
                    <?php if ($this->session->userdata('user_image')) { ?>
                        <img class="img-profile rounded-circle" src="<?php echo base_url() . $this->session->userdata('user_image'); ?>">
                    <?php } else { ?>
                        <img class="img-profile rounded-circle" src="<?php echo base_url(); ?>assets/img/user-profile.png">
                    <?php } ?>
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>home/profile">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profile
                    </a>
                    <a class="dropdown-item" href="#">
                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                        Settings
                    </a>
                    
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>home/logout" data-toggle="modal" data-target="#logoutModal">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                </div>
            </li>

        </ul>

    </nav>
    <!-- End of Topbar -->