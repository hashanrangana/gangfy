<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info  sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Divider -->
        <hr class="sidebar-divider my-0">
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>">
                <i class="fas fa-fw fa-home"></i>
                <span>Home</span></a>
        </li>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>welcome">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Vendor
        </div>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Branches" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-code-branch"></i>
                <span>Branches</span>
            </a>
            <?php if ($this->session->userdata('user_type') == 'vendor') { ?>
                <div id="Branches" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/create_branch">Create Branch</a>
                        <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/list_branches">All Branches</a>


                    </div>
                </div>
            <?php } ?>
        </li>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Services</span>
            </a>
            <?php if ($this->session->userdata('user_type') == 'vendor') { ?>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href="<?php echo base_url(); ?>listing/list_services">Services</a>
                        <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/create_time_slots">Time Slots</a>


                    </div>
                </div>
            <?php } ?>
        </li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-calendar-alt"></i>
                <span>Booking</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <?php if ($this->session->userdata('user_type') == 'vendor') { ?>
                        <a class="collapse-item" href="<?php echo base_url(); ?>booking/list_booking">List Booking</a>
                         <a class="collapse-item" href="<?php echo base_url(); ?>booking/booking_request">Customize  Booking</a>
                    <?php } else { ?>
                        <a class="collapse-item" href="<?php echo base_url(); ?>booking/my_booking">My Booking</a>
                    <?php } ?>

                </div>
            </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Addons
        </div>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-hands-helping"></i>
                <span>Customers</span>
            </a>
            <div id="collapseCustomer" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">

                    <a class="collapse-item" href="<?php echo base_url(); ?>booking/list_customer">Customers List</a>

                    <a class="collapse-item" href="<?php echo base_url(); ?>booking/banned_customers">Banned Customers</a>


                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Coupon" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-code"></i>
                <span>Coupon</span>
            </a>
            <div id="Coupon" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">

                    <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/coupon_generater">Generate Coupon</a>

                    <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/shared_coupon">Shared Coupon</a>


                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Message" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-envelope"></i>
                <span>Messages</span>
            </a>
            <div id="Message" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/send_messages">Sent Messages</a>

                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-file-alt"></i>
                <span>Report</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-question"></i>
                <span>Help</span></a>
        </li>

    </ul>
    <!-- End of Sidebar -->