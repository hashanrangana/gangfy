<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info  sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Divider -->
        <hr class="sidebar-divider my-0">
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>">
                <i class="fas fa-fw fa-home"></i>
                <span>Home</span></a>
        </li>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>welcome/client">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Client
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        </li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-calendar-alt"></i>
                <span>Booking</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" target="_blank" href="<?php echo base_url() ?>">Make a Booking</a>
                    <a class="collapse-item" href="<?php echo base_url(); ?>booking/my_booking">My Bookings</a>
                    <a class="collapse-item" href="<?php echo base_url(); ?>booking/custom_booking">My Custom Bookings</a>
                </div>
            </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Addons
        </div>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>dashboard/list_coupon">
                <i class="fas fa-fw fa-clipboard-list"></i>
                <span>My Coupons</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>welcome/my_qr">
                <i class="fas fa-fw fa-qrcode"></i>
                <span>My QR Code</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>dashboard/list_messages">
                <i class="fas fa-fw fa-envelope"></i>
                <span>Messages</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-question"></i>
                <span>Help</span></a>
        </li>

    </ul>
    <!-- End of Sidebar -->
