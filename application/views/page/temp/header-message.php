<?php
if ($this->session->flashdata('err_msg')) {
    ?>
    <div class="sys-msg alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>Sorry ! </strong><?php echo $this->session->flashdata('err_msg'); ?>
    </div>
    <?php
}
if ($this->session->flashdata('succ_msg')) {
    ?>
    <div class="sys-msg alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $this->session->flashdata('succ_msg'); ?>
    </div>
    <?php
}
if ($this->session->flashdata('war_msg')) {
    ?>
    <div class="sys-msg alert alert-warning" role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $this->session->flashdata('war_msg'); ?>
    </div>
    <?php
}

