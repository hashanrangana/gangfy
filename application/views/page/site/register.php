<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/header-message');
?>
<style>
    form.user .form-control-user {
        font-size: 0.8rem;
        border-radius: 10rem;
        padding: 0rem 1rem;
    }

</style>
<body>

    <div class="container">

        <?php if (isset($regCheckStatus) == 'error') { ?>
            <div class="alert alert-danger">
                <strong>Error</strong> Already email exists
            </div>
        <?php } ?>

        <?php if (isset($vendorCheckStatus) == 'vendor_success') { ?>
            <div class="alert alert-success">
                <p>You have created an vendor account wait for Gangfy admin to approve your account. Please check your email</p> 
            </div>
        <?php } ?>
        <div class="card o-hidden border-0 shadow-lg my-5 login-body">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><img src="<?php echo base_url(); ?>assets/img/logo.png"></h1>
                            </div>
                            <div class="text-center">
                                <h1 class="h4 mb-4">Create an Account!</h1>
                            </div>

                            <form class="user" action="<?php echo current_url() ?>" method="post" name="registerform">

<!--                                <div class="form-group">
                                    <select class="form-control form-control-user" name="usertype" required>
                                        <option value="">Select type</option>
                                        <option value="customer">Customer</option>
                                        <option value="vendor">Vendor</option>
                                    </select>
                                </div>-->
                                <?php // echo form_error('usertype', '<div class="error" style="color:red;">', '</div>'); ?>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName" name="fname" placeholder="First Name">
                                    </div>
                                    <?php echo form_error('fname', '<div class="error" style="color:red;">', '</div>'); ?>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="exampleLastName" name="lname" placeholder="Last Name">
                                    </div>
                                    <?php echo form_error('lname', '<div class="error" style="color:red;">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="exampleInputEmail" name="email" placeholder="Email Address">
                                </div>
                                <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" name="password" placeholder="Password">
                                    </div>
                                    <?php echo form_error('password', '<div class="error" style="color:red;">', '</div>'); ?>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" name="repassword" placeholder="Repeat Password">
                                    </div>
                                    <?php echo form_error('repassword', '<div class="error" style="color:red;">', '</div>'); ?>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block" name="add" id="add"> Register Account  </button>
                                <hr>
                                <a href="<?php echo base_url(); ?>home/login" class="btn btn-gangfy btn-user btn-block">
                                    Login with GangFy
                                </a>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="<?php echo base_url(); ?>home/recover">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="<?php echo base_url(); ?>home/login">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php
    $this->load->view('page/temp/portal_footer');
    ?>