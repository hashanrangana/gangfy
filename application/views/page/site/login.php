<?php
$this->load->view('page/temp/portal_header');
?>
<body>
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <?php if (isset($checkStatus) == 1) { ?>
                <div class="alert alert-danger">
                    <strong>Error</strong> Please check your email and password
                </div>
            <?php } ?>
            <?php if (isset($Checkbanner) == 1) { ?>
                <div class="alert alert-danger">
                    <strong>User Banned</strong> Contact Gangfy Admin
                </div>
            <?php } ?>
            <?php if (isset($CheckVendor) == 1) { ?>
                <div class="alert alert-warning">
                    <strong>Vendor Account waiting for approval</strong>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('rps_msg')) { ?>
                <div class="alert alert-success">
                    <strong><?php echo $this->session->flashdata('rps_msg'); ?></strong>
                </div>
            <?php } ?>
            <?php if (isset($ban)) { ?>
                <div class="alert alert-danger">
                    <strong>Sorry! </strong> <?php echo $ban; ?>
                </div>
            <?php } ?>
            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5 login-body">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <!--<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>-->
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4"><img src="<?php echo base_url(); ?>assets/img/logo.png"></h1>
                                    </div>
                                    <form class="user" action="<?php echo current_url() ?>" method="post">
                                        <div class="form-group">
                                            <input autofocus="" id="email" name="email" autocomplete="off" required type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                        </div>
                                        <div class="form-group">
                                            <input id="pwd" name="pwd" autocomplete="off" required type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember Me</label>
                                            </div>
                                        </div>
                                        <button type="submit"  name="add" id="add" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                        <hr>


                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="<?php echo base_url(); ?>home/recover" target="_blank" >Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="<?php echo base_url(); ?>home/register" target="_blank">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <?php
    $this->load->view('page/temp/portal_footer');
    ?>