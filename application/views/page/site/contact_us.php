<div class="container main-container">
    <div class="row">

        <div class="col-md-12">
            <h2 class="page-title">Contact Us</h2>
            <h4 class="page-sub-title"></h4>
        </div>  

    </div>


    <div class="row">
        <div class="col-md-8">
            <form enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
                <?php if (isset($emailSuccess) == 'success') { ?>
                    <div class="alert alert-success">
                        <label>Email send successfully.</label> 
                    </div>
                <?php } ?>
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                        <input class="form-control" name="firstname" placeholder="Firstname" type="text" required autofocus />
                        <?php echo form_error('firstname', '<div class="error" style="color:red;">', '</div>'); ?>

                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                        <input class="form-control" name="lastname" placeholder="Lastname" type="text" required />
                        <?php echo form_error('lastname', '<div class="error" style="color:red;">', '</div>'); ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                        <input class="form-control" name="email" placeholder="E-mail" type="email" required />
                        <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                        <input class="form-control" name="subject" placeholder="Subject" type="text" required />
                        <?php echo form_error('subject', '<div class="error" style="color:red;">', '</div>'); ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <textarea style="resize:vertical;" class="form-control" placeholder="Message..." rows="6" name="comment" required></textarea>
                        <?php echo form_error('comment', '<div class="error" style="color:red;">', '</div>'); ?>

                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <br/>
                        <input type="submit" class="btn gbc-btn" value="Send" name="Send" id="Send" />

                    </div>
                </div>
            </form>

        </div>


        <div class="col-md-4 contact-icon">
            <p>
                <i class="fas fa-envelope-square"></i><strong> E-mail :</strong> works@gangfy.com
            </p>
            <p>
                <i class="fas fa-book"></i><strong> Address :</strong> GangFY Networks Inc, 51 Ashfield Drive, Toronto, ON, M9C 4T7, Canada
            </p>
            <p class="g-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d369318.0059150073!2d-79.91598686693162!3d43.68532981266689!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b469fe76b05b7%3A0x3146cbed75966db!2sMississauga%2C+ON%2C+Canada!5e0!3m2!1sen!2slk!4v1564921113304!5m2!1sen!2slk" width="100%" height="240px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </p>


        </div>


    </div>




</div>
