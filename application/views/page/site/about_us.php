

<div class="container main-container">
    <div class="row">

        <div class="col-md-12">
            <h2 class="page-title">About Us</h2>
            <h4 class="page-sub-title">GangFY Reservation</h4>
        </div>  


        <div class="col-md-12">
            <p><img src="<?php echo base_url(); ?>assets/images/about.jpg" class="align-left img-about"> This is an online reservation system where you can make bookings without having the need for a Credit Card. We are affiliated to gangFy Networks <a href="http://gangfy.com/" target="_blank">(www.gangfy.com)</a>, a Social Network that enables you to get rewarded for your good deeds. You will able to book sports facilities, salons, swimming pools and many more through our booking system soon.
                <br/><br/>
            </p>
        </div>  
    </div>
</div>




