<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/header-message');
?>
<style>
    form.user .form-control-user {
        font-size: 0.8rem;
        border-radius: 10rem;
        padding: 0rem 1rem;
    }

</style>
<body>

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5 login-body">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><img src="<?php echo base_url(); ?>assets/img/logo.png"></h1>
                            </div>
                            <div class="text-center">
                                <h1 class="h4 mb-4">Choose Your User Type!</h1>
                            </div>
                            <!--<form class="user" action="<?php echo current_url() ?>" method="post" name="registerform">-->
                            <?php echo form_open('home/register_usertype', array('name' => 'registerform', 'class' => 'user')); ?>
                            <div class="form-group">
                                <select class="form-control form-control-user" name="usertype" required>
                                    <option value="">Select type</option>
                                    <option value="customer">Customer</option>
                                    <option value="vendor">Vendor</option>
                                </select>
                            </div>
                            <?php echo form_error('usertype', '<div class="error" style="color:red;">', '</div>'); ?>                                                                                
                            <button type="submit" class="btn btn-primary btn-user btn-block" name="add" id="add"> Register </button>
                            <hr>
                            <a href="<?php echo base_url(); ?>home/login" class="btn btn-gangfy btn-user btn-block">
                                Login with GangFy
                            </a>
                            <!--</form>-->
                            <?php echo form_close(); ?>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="<?php echo base_url(); ?>home/recover">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="<?php echo base_url(); ?>home/login">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php
    $this->load->view('page/temp/portal_footer');
    ?>