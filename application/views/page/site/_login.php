<div class="container main-container">
     

     <div class="card bg-light">
     <article class="card-body mx-auto" style="max-width: 400px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="panel panel-default">
        <h4 class="card-title mt-3 text-center">Login </h4>
	<p class="text-center">Get started by logging to book now</p>
	<p>
    <?php if (isset($checkStatus) == 1) { ?>
<div class="alert alert-danger">
  <strong>Error</strong> Please check your email and password
</div>
<?php } ?>
<?php if (isset($Checkbanner) == 1) { ?>
<div class="alert alert-danger">
  <strong>User Banned</strong> Contact Gangfy Admin
</div>
<?php } ?>
<?php if (isset($CheckVendor) == 1) { ?>
<div class="alert alert-warning">
  <strong>Vendor Account waiting for approval</strong>
</div>
<?php } ?>
<hr/>

             <form class="form-signin" role="form" action="<?php echo current_url()?>" method="post" name="loginform">
  <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
		 </div>
     <input type="email" class="form-control"  autofocus="" id="email" name="email" autocomplete="off" required>

   </div> <!-- form-group// -->
   <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>

   <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
		</div>
    <input type="password" class="form-control"  id="pwd" name="pwd" autocomplete="off" required>

    </div> <!-- form-group// -->
    <?php echo form_error('pwd', '<div class="error" style="color:red;">', '</div>'); ?>

    <br>
  <button type="submit" class="btn btn-primary btn-block"  name="add" id="add">Login</button>
  <br/>
  <p class="text-center">Don't have an account? <a href="<?php echo base_url();?>home/register">Register now</a> </p>                                                                 

</form> 
             </div>
        </div>
      

        </div>

    </div>
    </article>

</div>
</div>