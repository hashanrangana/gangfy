<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info  sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>dashboard/vendor">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Vendor
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Services</span>
            </a>
            <?php if ($this->session->userdata('user_type') == 'vendor') { ?>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/create_branch">Add Branch</a>
                        <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/create_services">Add Services</a>
                        <a class="collapse-item" href="<?php echo base_url(); ?>dashboard/create_time_slots">Build Time Slots</a>
                        <a class="collapse-item" href="<?php echo base_url(); ?>listing/all_listing">All Services</a>
                        <a class="collapse-item" href="<?php echo base_url(); ?>booking/list_booking">All Booking</a>
                    </div>
                </div>
            <?php } else { ?>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">                  
                        <a class="collapse-item" href="<?php echo base_url(); ?>listing/all_listing">My Booking</a>
                    </div>
                </div>
            <?php } ?>
        </li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-calendar-alt"></i>
                <span>Booking</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">

                    <a class="collapse-item" href="#">Add Booking</a>
                    <a class="collapse-item" href="#">All Booking</a>

                </div>
            </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Addons
        </div>

        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-clipboard-list"></i>
                <span>Client List</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-file-alt"></i>
                <span>Report</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-question"></i>
                <span>Help</span></a>
        </li>

    </ul>
    <!-- End of Sidebar -->