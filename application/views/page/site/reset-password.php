<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/header-message');
?>
<body>
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5 login-body">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">

                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 mb-4"><img src="<?php echo base_url(); ?>assets/img/logo.png"></h1>
                                    </div>
                                    <div class="text-center">
                                        <h1 class="h4  mb-2">Reset Your Password?</h1>
                                        <p class="mb-4">We get it, stuff happens. Just enter your new password for recover your account!</p>
                                    </div>
                                    <?php echo form_open('home/change_password', array('class' => 'user')); ?>
                                    <div class="form-group">
                                        <input type="hidden" name="uid" value="<?php
                                        if (isset($uid)) {
                                            echo $uid;
                                        }
                                        ?>">
                                        <input type="password" name="password" class="form-control form-control-user" placeholder="Enter New Password">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="repassword" class="form-control form-control-user" placeholder="Retype New Password">
                                    </div>
                                    <button type="submit"  name="reset_password" id="reset_password" class="btn btn-primary btn-user btn-block">
                                        Reset Password
                                    </button>
                                    <?php echo form_close(); ?>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="<?php echo base_url(); ?>home/login">Already have an account? Login!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="<?php echo base_url(); ?>home/register">Create an Account!</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>    








    <?php
    $this->load->view('page/temp/portal_footer');
    ?>