<div class="container main-container">
      
    <div class="row title-holder">
      <div class="col-md-12">
      <h2 >Dashboard</h2>
        <br/>
        <div class="row">
          <div class="col-md-4"> 
          <h4> Total </h4>   
          <?php echo $totalOrders;?>
          </div>
          <div class="col-md-4"> 
          <h4> Pending </h4>   
          <?php echo $totalPending;?>
          </div>
          <div class="col-md-4"> 
          <h4> Confirmed  </h4>   
          <?php echo $totalconfirm;?>
          </div>
        </div>


<br/><br/>
        
<table id="orderVendor" class="table table-striped table-bordered" style="width:100%">
                    <thead>
  <tr>
  <th scope="col">#</th>
    <th scope="col">Customer</th>
    <th scope="col">Service</th>
    <th scope="col">From</th>
    <th scope="col">To</th>
    <th scope="col">Status</th>
    <th scope="col">&nbsp;</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($latestOrders as $serviceItem) { ?>

  <tr>
    <td>GB-<?php echo $serviceItem->booking_Id;?> </td>
    <td><?php echo $serviceItem->name;?></td>
    <td><?php echo $serviceItem->serviceName;?></td>
    <td>
    <?php $date=date_create($serviceItem->booking_from);?>
     <?php echo  date_format($date,"Y-m-d H:i ");?>
    </td>
    <td>
    <?php $date=date_create($serviceItem->booking_to);?>
     <?php echo  date_format($date,"Y-m-d H:i ");?>
    </td>
    <td><?php echo $serviceItem->orderStatus;?></td>
    <td> <a href="#"><i class="fas fa-eye"></i></a> </td>
  </tr>
  <?php }?>
  
 </tbody>
</table>
      
      </div>
      <div  class="col-md-6">
     
      </div>
    </div>
</div>
  
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
   
   <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>

       <script>
     
   $(document).ready(function() {
 $('#listingDashboard').DataTable();
 $('#orderVendor').DataTable();

} );
       </script>