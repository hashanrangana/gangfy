<?php $this->load->view('page/temp/header-message'); ?>

<div class="container main-container">
    <div class="row">
        <?php
        if (isset($allListings)) {
            foreach ($allListings as $listingItem) {
                $lid = $listingItem['listingId'];
                ?>

                <div class="col-md-8">
                    <div class="facility-detail-page-info">
                        <h2>
                            <?php echo $listingItem['listingName']; ?>
                        </h2>
                        <p>
                            <?php
                            if ($listingItem['description']) {
                                echo $listingItem['description'];
                            } else {
                                echo 'Details Pending...';
                            }
                            ?>
                        </p>
                        <?php if (sizeof($listingImages) > 0) { ?>
                            <div class="row gb-gallery">
                                <?php
                                $i = 0;
                                foreach ($listingImages as $Item) {
                                    ?>
                                    <?php if ($i == 0) { ?> 

                                        <div class="col-md-6">
                                            <img src="<?php echo base_url(); ?><?php echo $Item['image']; ?>" style="width:100%;" class="gbg-col-1">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                            <?php } else { ?> 


                                                <div class="col-md-6">
                                                    <img src="<?php echo base_url(); ?><?php echo $Item['image']; ?>" style="width:100%;" class="gbg-col-2">
                                                </div>


                                                <?php
                                            } $i++;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if (sizeof($listingFeatures)) { ?>

                        <div class="row">
                            <div class="gb-srv">
                                <div class="row">
                                    <div class="col-md-12"><h5 class="sec-title">Amenities</h5></div>
                                    <?php
                                    foreach ($listingFeatures as $ldata) {
                                        $fname = $ldata['featuresName'];
                                        ?>
                                        <?php if ($fname == 'Restroom') { ?>
                                            <div class="col-md-3 gb-srv-item"><i class="fas fa-restroom"></i> <?php echo $fname; ?></div>
                                        <?php } elseif ($fname == 'Car Park') { ?>                                                                     
                                            <div class="col-md-3 gb-srv-item"><i class="fas fa-car"></i> <?php echo $fname; ?></div>
                                        <?php } elseif ($fname == 'Floodlights') { ?>                                                              
                                            <div class="col-md-3 gb-srv-item"><i class="fas fa-lightbulb"></i> <?php echo $fname; ?></div>
                                        <?php } elseif ($fname == 'Water Dispenser') { ?>
                                            <div class="col-md-3 gb-srv-item"><i class="fas fa-prescription-bottle"></i> <?php echo $fname; ?></div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <!--                    <div class="row">
                                            <div class="gb-map gb-map-details">
                                                <div class="row">
                    
                                                    <input type="hidden" id="locationMap" value="<?php echo $listingItem['location']; ?>"/>
                    
                                                    <div class="col-md-12"><h5 class="sec-title">Location</h5></div>
                                                    <div class="col-md-12">
                                                        <div id="listingMap" style="height: 250px;">
                    
                                                                                                                                                                                                                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             <iframe src="http://maps.google.com/maps?q= <?php echo $listingItem['lat']; ?>,<?php echo $listingItem['lng']; ?>&z=15&output=embed" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->

                    <div class="row">
                        <div class="gb-list">
                            <div class="row">
                                <div class="col-md-12"><h5 class="sec-title">Things To Note</h5></div>
                                <div class="col-md-12">
                                    <?php echo $listingItem['description']; ?>
                                </div>
                            </div>
                        </div>  
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <h5 class="card-header gbc-header">Book a Slot</h5>
                        <div class="card-body">
                            <label style="margin-bottom:10px;"><b>Service :</b> <?php echo $listingItem['serviceName']; ?></label>
                            <br/>
                            <label style="margin-bottom:10px;"><b>Sub Service :</b> <?php echo $listingItem['subServiceName']; ?></label>
                            <hr/>
                            <?php if ($this->session->userdata('user_id')) { ?>
                                <br>
                                <center>

                                    <p align="center"><span  style="font-weight:bold">For reservations during:</span><br>
                                        8am to 6pm on weekdays<br>
                                        12am to 8am on all seven days<br>
                                    </p>
                                    <button type="button" style="border-radius:50px;margin-bottom:20px;" class="btn btn-md btn-primary" data-toggle="modal" onclick="setListing(<?php echo $listingItem['branch_id']; ?>);" data-target="#modalCart">Request a Custom Slot</button>
                                    <img src="<?php echo base_url(); ?>assets/images/or.png" style="margin-bottom:10px;">
                                </center>
                            <?php } ?>

                            <label style="margin-bottom:10px;"><b>Select a date </b></label>
                            <?php echo form_open('booking/search', array('class' => 'form')); ?>
                            <div class="form-group">
                                <input type="text" name="bookdate" value="<?php
                                if ($this->session->userdata('book_date')) {
                                    echo date('Y-m-d', $this->session->userdata('book_date'));
                                }
                                ?>" autocomple="off" readonly class="form-control DateFinder" required  style="">
                                <input type="hidden" value="<?php echo $listingItem['listingId']; ?>" name="sid">
                                <input type="hidden" name="sect" value="sng">
                            </div>

                            <button type="submit" name="Search" class="btn btn-info" style="background-color:#4bd6eb;border-color:#4bd6eb;margin-bottom:20px"><i class="fa fa-search"></i> Search</button>




                            <?php echo form_close(); ?>

                            <input type='hidden' class="form-control bn-search-date"  value="<?php echo $listingItem['listingId']; ?>" id="list-id" name="list-id" />

                            <?php
                            if ($this->session->userdata('book_msg')) {
                                echo $this->session->userdata('book_msg');
                            }
                            if ($this->session->userdata('slot_data')) {
                                echo $this->session->userdata('slot_data');
                            }
                            ?>

                        </div>
                    </div>

                </div>
                <?php
            }
        }
        ?>

        <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg modal-cus" role="document">
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Make a Custom Request</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <!--Body-->
                    <?php echo form_open('booking/customize_booking'); ?>
                    <div class="modal-body">  
                        <div class="row">   	
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="date">Select a Date *</label>
                                            <input type="text" class="form-control DateFinder" placeholder="Select a Date" name="cdate" id="cdate" required>
                                            <input type="hidden" name="bid" id="bid">
                                            <input type="hidden" name="sec" value="single">
                                            <input type="hidden" name="lid" id="lid" value="<?php echo $lid; ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="service">Select Services *</label>
                                            <select class="form-control" id="service" name="service[]" required>
                                                <option value="">Select Services</option>
                                                <?php
                                                if (isset($srv)) {
                                                    foreach ($srv as $ldata) {
                                                        ?>
                                                        <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>                         
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="sfrom">From * (10:30 AM)</label>
                                            <input name="sfrom[]" id="sfrom" type="time" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="sto">To * (11:30 AM)</label>
                                            <input name="sto[]" id="sto" type="time" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="sto">Duration * (1.5) *</label>
                                            <input name="duration[]" id="sto"  type="text" placeholder="1.5" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-1 btn-add">
                                        <div class="form-group">
                                            <label for="btnadd"></label>
                                            <button id="btnAdd" type="button" value="Add" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                </div>
                                <div id="addServices"></div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="desc" class="form-control" placeholder="Notes"></textarea>
                                </div>
                            </div>

                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                            <input type="submit" name="request" class="btn btn-primary" value="Request">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>