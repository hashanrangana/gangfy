

      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
       
       <div class="carousel-inner" role="listbox">
         <!-- Slide One - Set the background image for this slide in the line below -->
         <div class="carousel-item active" style="background-image: url('<?php echo base_url();?>assets/images/main-slider.jpg')">
           <div class="carousel-caption d-md-block">
             <h3>Book your playing space through us</h3>
             <h4>Discover & Book Unique Spaces</h4>
             
             <div class="row" id="requiredSearch" style="display:none;">
             <div class="col-md-2" ></div>
             <div class="col-md-8" >
             <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Select Service,Location and Datetime to Search
         <button type="button" class="close" onclick = "closeRequired(this)">
         <span aria-hidden="true">&times;</span>
        </button>
      </div>
             </div>
             <div class="col-md-2" ></div>
             </div>

            
             <div class="row" style="text-algin:right;height:30px;">

             <div class="col-md-4" >
               <label id="showSelectedMain" style="color:#fff;text-algin:right;"></label>
             </div>
             <div class="col-md-8">

             </div>
            </div>
             <div class="row">

             <div class="col-md-4" style="color:#333;text-algin:right;">
               <select class="btn dropdown-toggle bn-search-dropdown" name="service" id="filter-service">
               <option value=""  id="0">Select Service</option>

                 <?php foreach ($allservices as $serviceItem) { ?>
                     <option value="<?php echo $serviceItem['serviceId'];?>" id="<?php echo $serviceItem['serviceName'];?>"><?php echo $serviceItem['serviceName'];?></option>
                  <?php }?>
               </select>
               <select class="btn dropdown-toggle bn-search-dropdown" name="subService" id="filter-sub-service">
               <option value="" >Select Sub Service</option>
               </select>
               </div>
                   
                       <div class="col-md-3" style="color:#333;">
                         <div class="dropdown">
                         <input id="pac-input" type="text" name="lcity" class="btn dropdown-toggle bn-search-dropdown">
                         </div>
                       </div>

                     <div class="col-md-3" style="color:#333;">
                     <input type='text' class="form-control bn-search-date" id='searchDateTime' placeholder="Date"   data-date-format="dd MM yyyy"  />
                          <!-- <i class="fas fa-calendar-alt bn-search-icon" ></i> -->
                       </div>

                      

                       <div class="col-md-2">
                         <button class="btn bn-search-btn" id="find-facility">Find Facility <i class="fas fa-chevron-right"></i></button>
                       </div>

             </div>
           </div>
         </div>
     </div>

   <!-- Page Content -->
   


<!-- Modal -->
<div class="modal fade" id="find-facility-model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content" id="search-bx-1" >
     <div class="modal-header">
       <h5 class="modal-title" id="searchTitleModel">Search Results: Services + Location</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body" >
     <div class="row">
       <div class="col-md-7" id="searchView">
       </div>
       <div class="col-md-5">
       <div id="searchMap" style="height: 250px;"></div>

       <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d184740.70651542794!2d-79.46093320431146!3d43.657040323480764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4cb90d7c63ba5%3A0x323555502ab4c477!2sToronto%2C+ON%2C+Canada!5e0!3m2!1sen!2slk!4v1553056384291" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->

       </div>
     </div>
    
     </div>
     
   </div>


 </div>
</div>
   <!-- Bootstrap core JavaScript -->
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpHZC2DR6DiK9TfSBO-6Xr_JcLwIe94so&libraries=places&callback=initMap"
         async defer></script>
   