<?php
$this->load->view('page/temp/site_header');
$this->load->view('page/temp/header-message');
?>

<div class="container main-container">
    <div class="row">
        <div class="col-md-8" style="margin-bottom:15px;">
            <div class="row">
                <?php
                if (isset($list)) {
                    foreach ($list as $ld) {
                        ?>
                        <div class="col-md-4" style="margin-bottom:15px;"><div class="cn-content"  style="min-height:260px">
                                <img src="<?php echo base_url() . $ld['image']; ?>" class="img-responsive list-img" style="width:100%">
                                <h5 class="cn-name"><a href="<?php echo base_url(); ?>home/details/<?php echo $ld['listingId'] . '/' . str_replace(' ', '-', $ld['listingName']); ?>" ><?php echo $ld['listingName']; ?></a></h5>
                                <p class="cn-desc"><?php echo substr($ld['description'], 0, 30) . '...'; ?></p>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <h5 class="card-header gbc-header">Book a Slot</h5>
                <div class="card-body">

                    <?php if ($this->session->userdata('user_id')) { ?>
                        <center>
                            <p align="center"><span  style="font-weight:bold">For reservations during:</span><br>
                                8am to 6pm on weekdays<br>
                                12am to 8am on all seven days<br>
                            </p>
                            <button type="button" style="border-radius:50px;margin-bottom:10px;" class="btn btn-md btn-primary" data-toggle="modal" onclick="setListing(<?php echo $ld['branch_id']; ?>);" data-target="#modalCart">Request a Custom Slot</button>
                            <img src="<?php echo base_url(); ?>assets/images/or.png" style="margin-bottom:10px;">	
                        </center>
                    <?php } ?>

                    <?php echo form_open('booking/search', array('class' => 'form')); ?>
                    <div class="row">
                        <div class="col-md-7" style="margin-bottom:10px;">

                            <select class="form-control" name="sid" id="sid" required>
                                <?php
                                $sid = $this->session->userdata('sid');
                                if (empty($sid)) {
                                    ?>
                                    <option value="">Service</option>
                                    <?php
                                }
                                if (isset($srv)) {
                                    foreach ($srv as $sv) {
                                        $lid = $sv['listingId'];
                                        if ($sid == $lid) {
                                            ?>
                                            <option value="<?php echo $sv['listingId']; ?>" selected><?php echo $sv['listingName']; ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $sv['listingId']; ?>"><?php echo $sv['listingName']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="sect" value="service">
                        </div>
                        <div class="col-md-5" style="margin-bottom:10px;">
                            <input type="text" name="bookdate" value="<?php
                            if ($this->session->userdata('book_date')) {
                                echo date('Y-m-d', $this->session->userdata('book_date'));
                            }
                            ?>" autocomple="off" readonly class="form-control DateFinder" required placeholder="Date">
                        </div>
                    </div>
                    <button type="submit" name="Search" class="btn btn-info" style="background-color:#4bd6eb;border-color:#4bd6eb;margin-top:0px"><i class="fa fa-search"></i> Search</button>
                    <?php echo form_close(); ?>
                    <br/>
                    <?php if ($this->session->userdata('user_id')) { ?>
                                                                                                                                                    <!--<button type="button" class="btn btn-primary" data-toggle="modal" onclick="setListing(<?php echo $listingItem['listingId']; ?>);" data-target="#modalCart">Custom Slot</button>-->
                    <?php } ?>
                    <?php
                    if ($this->session->userdata('book_msg')) {
                        echo $this->session->userdata('book_msg');
                    }
                    if ($this->session->userdata('slot_data')) {
                        echo $this->session->userdata('slot_data');
                    }
                    ?>



                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-cus" role="document">
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Make a Custom Request</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--Body-->
                <?php echo form_open('booking/customize_booking'); ?>
                <div class="modal-body">  
                    <div class="row">   	
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="date">Select a Date *</label>
                                        <input type="text" class="form-control DateFinder" placeholder="Select a Date" name="cdate" id="cdate" required>
                                        <input type="hidden" name="bid" id="bid">
                                        <input type="hidden" name="sec" value="service">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="service">Select Services *</label>
                                        <select class="form-control" id="service" name="service[]" required>
                                            <option value="">Select Services</option>
                                            <?php
                                            if (isset($srv)) {
                                                foreach ($srv as $ldata) {
                                                    ?>
                                                    <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>                         
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="sfrom">From * (10:30 AM)</label>
                                        <input name="sfrom[]" id="sfrom" type="time" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="sto">To * (11:30 AM)</label>
                                        <input name="sto[]" id="sto" type="time" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="sto">Duration * (1.5)</label>
                                        <input name="duration[]" id="sto"  type="text" placeholder="1.5" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="btnadd"></label>
                                        <button id="btnAdd" type="button" value="Add" class="btn btn-success btn-add"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>

                            </div>
                            <div id="addServices"></div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="desc" class="form-control" placeholder="Notes"></textarea>
                            </div>
                        </div>

                    </div>
                    <!--Footer-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                        <input type="submit" name="request" class="btn btn-primary" value="Request">
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap core JavaScript -->

<?php $this->load->view('page/temp/site_footer'); ?>