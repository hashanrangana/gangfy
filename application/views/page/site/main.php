

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('<?php echo base_url(); ?>assets/images/main-slider.jpg')">
            <div class="carousel-caption d-md-block">
                <h3>Make Reservations without a Credit Card!</h3>
                <!--<h4>Discover & Book Unique Spaces</h4>-->

                <div class="row" id="requiredSearch" style="display:none;">
                    <div class="col-md-2" ></div>
                    <div class="col-md-8" >
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Select Location and Service to Search
                            <button type="button" class="close" onclick = "closeRequired(this)">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-2" ></div>
                </div>

                <div class="row" style="text-algin:right;height:30px;">

                    <div class="col-md-4" >
                        <label id="showSelectedMain" style="color:#fff;text-algin:right;"></label>
                    </div>
                    <div class="col-md-8">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5" style="color:#333;margin-bottom: 10px;">
                        <div class="dropdown">
                            <input id="pac-input" type="text" name="lcity" placeholder="Enter Location" class="btn dropdown-toggle bn-search-dropdown">
                        </div>
                    </div>
                    <div class="col-md-4 search-box" style="margin-bottom: 10px !important;">
                        <select class="btn dropdown-toggle bn-search-dropdown" name="service" id="filter-service" style="padding-top: 8px !important;padding-bottom: 8px !important;">
                            <option value=""  id="0">Select Service</option>

                            <?php
                            if (isset($allservices)) {
                                foreach ($allservices as $serviceItem) {
                                    ?>
                                    <option value="<?php echo $serviceItem['serviceId']; ?>" id="<?php echo $serviceItem['serviceName']; ?>"><?php echo $serviceItem['serviceName']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>

                        <div class="search-sub-cat" id="filter-sub-service">

                        </div>
                    </div>
                    <div class="col-md-3" style="margin-bottom: 10px !important;">
                        <button class="btn bn-search-btn" id="find-facility">Find Facility <i class="fas fa-chevron-right"></i></button>
                    </div>

                </div>
                <div class="row FeaturedPlace">
                    <div class="col-md-4">
                        <div class="cn-content">
                            <div class="f-label">Featured Places</div>
                            <img class="feature-img" src="<?php echo base_url(); ?>assets/images/logo/soff-cricket.png">
                            <h5 class="cn-name"><a href="<?php echo base_url(); ?>listing/branch/3/SOFF-Cricket">SOFF Cricket</a></h5>
                            <p class="cn-desc">When Soff opened in 2013, we set out with the goal of sharing the wonderful game...</p>
<!--                            <ul class="list-sub-service">
                                <li><a href="<?php echo base_url(); ?>listing/all_services/3/SOFF-Cricket">View Services</a></li>
                            </ul>-->
                            <p class="cn-location"><i class="fas fa-map-marker-alt"></i> Location: Mississauga, ON, Canada</p> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="cn-content">
                            <div class="f-label">Featured Places</div>
                            <img class="feature-img" src="<?php echo base_url(); ?>assets/images/logo/background.png">
                            <h5 class="cn-name">Centre Name (Coming Soon)</h5>
                            <p class="cn-desc">sample text, sample text, sample text, sample text</p>
<!--                            <ul class="list-sub-service">
                                <li>Branch Name</li>
                            </ul>-->
                            <p class="cn-location"><i class="fas fa-map-marker-alt"></i> Location:</p> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="cn-content">
                            <div class="f-label">Featured Places</div>
                            <img class="feature-img" src="<?php echo base_url(); ?>assets/images/logo/background.png">
                            <h5 class="cn-name">Centre Name (Coming Soon)</h5>
                            <p class="cn-desc">sample text, sample text, sample text, sample text</p>
<!--                            <ul class="list-sub-service">
                                <li>Branch Name</li>
                            </ul>-->
                            <p class="cn-location"><i class="fas fa-map-marker-alt"></i> Location:</p> 
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Page Content -->



    <!-- Modal -->
    <div class="modal fade" id="find-facility-model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="search-bx-1" >
                <div class="modal-header">
                    <h5 class="modal-title" id="searchTitleModel">Search Results: Services + Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <div class="col-md-7" id="searchView">
                        </div>
                        <div class="col-md-5">
                            <div id="searchMap" style="height: 250px;"></div>


                        </div>
                    </div>

                </div>

            </div>


        </div>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpHZC2DR6DiK9TfSBO-6Xr_JcLwIe94so&libraries=places&callback=initMap"
    async defer></script>

