<div class="container main-container">
     

     <div class="card bg-light">
     <article class="card-body mx-auto" style="max-width: 400px;">
         <h4 class="card-title mt-3 text-center">Create Account</h4>
         <p class="text-center">Get started with your free account</p>
         <p>
             <a href="" class="btn btn-block btn-gfy">Gangfy</a>
             
         </p>
         <p class="divider-text">
             <span class="bg-light">OR</span>
         </p>
         <?php if (isset($regCheckStatus) == 'error') { ?>
<div class="alert alert-danger">
  <strong>Error</strong> Already email exists
</div>
<?php } ?>

<?php if (isset($vendorCheckStatus) == 'vendor_success') { ?>
<div class="alert alert-success">
<p>You have created an vendor account wait for Gangfy admin to approve your account.</p> 
</div>
<?php } ?>

         <form action="<?php echo current_url()?>" method="post" name="registerform">
         <div class="form-group input-group">
             <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-user"></i> </span>
              </div>
             <input name="name" class="form-control" placeholder="Full name" type="text" autocomplete="off" required>
         </div> <!-- form-group// -->
         <?php echo form_error('name', '<div class="error" style="color:red;">', '</div>'); ?>

         <div class="form-group input-group">
             <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
              </div>
             <input name="email" class="form-control" placeholder="Email address" type="email" autocomplete="off" required>
         </div> <!-- form-group// -->
         <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>

         <div class="form-group input-group">
             <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
             </div>
             <select class="custom-select" style="max-width: 120px;" autocomplete="off" required name="phonecode">
                 <option selected="+972" value="+972">+971</option>
                 <option value="+972">+972</option>
                 <option value="+198">+198</option>
                 <option value="+701">+701</option>
             </select>
             <input name="phonenumber" class="form-control" placeholder="Phone number" type="text" autocomplete="off" required onkeypress="return isNumberKey(event)" >
         </div> <!-- form-group// -->
         <?php echo form_error('phonecode', '<div class="error" style="color:red;">', '</div>'); ?>
         <?php echo form_error('phonenumber', '<div class="error" style="color:red;">', '</div>'); ?>

         <div class="form-group input-group">
             <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-building"></i> </span>
             </div>
             <select class="form-control" autocomplete="off" required name="usertype">
                 <option value="" selected=""> Select type</option>
                 <option value="host">Customer</option>
                 <option value="vendor">Vendor</option>
             
             </select>
         </div> <!-- form-group end.// -->
         <?php echo form_error('usertype', '<div class="error" style="color:red;">', '</div>'); ?>

         <div class="form-group input-group">
             <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
             </div>
             <input class="form-control" placeholder="Create password" type="password" autocomplete="off" required  name="password">
         </div> <!-- form-group// -->
         <?php echo form_error('password', '<div class="error" style="color:red;">', '</div>'); ?>

         <div class="form-group input-group">
             <div class="input-group-prepend">
                 <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
             </div>
             <input class="form-control" placeholder="Repeat password" type="password" autocomplete="off" required name="repassword">
         </div> <!-- form-group// -->   
         <?php echo form_error('repassword', '<div class="error" style="color:red;">', '</div>'); ?>
                                   
         <div class="form-group">
             <button type="submit" class="btn btn-primary btn-block" name="add" id="add"> Create Account  </button>
         </div> <!-- form-group// -->      
         <p class="text-center">Have an account? <a href="<?php echo base_url();?>home/login">Log In</a> </p>                                                                 
     </form>
     </article>
     </div> <!-- card.// -->
     
     </div> 
     <!--container end.//-->
             
            
           </div>
       
           <script type="text/javascript">
            function isNumberKey(evt){
                 var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
             return false;
            return true;
        }
 </script>