

 <div class="container main-container">
      
      <div class="row title-holder">
      <div class="col-md-6">
      <h2 >All Listing</h2>
      </div>
      
       <div class="col-md-6" style="text-align:right;">
        <a class="btn add-btn btn-sm" href="<?php echo base_url();?>dashboard/create_services"><i class="fas fa-plus"></i> Add Services</a>
       </div>
      </div>
      
    
      
      
        <div class="row">
             
            <div class="col-md-12">


                
					<table id="listing" class="table table-striped table-bordered" style="width:100%">
                    <thead>
  <tr>
    <th scope="col">Title</th>
    <th scope="col">Service</th>
    <th scope="col">Sub Service</th>
    <th scope="col">Location</th>
    <th scope="col">Status</th>
    <th scope="col">&nbsp;</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($alllisting as $Item) { ?>

  <tr>
    <td><a href="<?php echo base_url();?>home/details/<?php echo $Item->listingId;?>/<?php echo $Item->listingName;?>" target="_blank"><?php echo $Item->listingName;?></a></td>
    <td><?php echo $Item->serviceName;?></td>
    <td><?php echo $Item->subServiceName;?></td>
    <td><?php echo $Item->location;?></td>
    <td>
    <?php if($Item->activeStatus == 1){?>
        Published
    <?php }else if($Item->activeStatus == 2) {?>
        Achieved
    <?php }else {?>
      Unpublished
    <?php }?>
    </td>
    <td>
      <a href="<?php echo base_url();?>home/details/<?php echo $Item->listingId;?>/<?php echo $Item->listingName;?>" target="_blank"><i class="fas fa-eye text-primary"></i></a> 
      <!-- <a href="#"><i class="fas fa-eye-slash text-muted"></i></a>  -->
      <a  href="<?php echo base_url();?>listing/edit_listing/<?php echo $Item->listingId;?>"><i class="fas fa-edit text-success"></i></a>  
      <a href="<?php echo base_url();?>listing/delete_listing/<?php echo $Item->listingId;?>"><i class="fas fa-trash-alt text-warning-red"></i></a>
    </td>
  </tr>
  <?php }?>
 </tbody>
</table>
			</div>
					
</div>
<!-- ./Tabs -->

        
  
        
        </div>
        
        
        
     

          
            
  
    
  

    </div>
                   
                   
                   
        
        
        </div>
   </div>
  
  
 

   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
   
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>

        <script>
      
    $(document).ready(function() {
	$('#listing').DataTable();
} );
        </script>