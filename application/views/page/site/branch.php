<?php $this->load->view('page/temp/site_header'); ?>


<div class="container main-container">
    <div class="row">

        <div class="col-md-8">

            <div class="facility-detail-page-info">
                <?php
                if (isset($list)) {
                    foreach ($list as $ld) {
                        $bid = $ld['branch_id'];
                        ?>
                        <h2>
                            <img src="<?php echo base_url() . $ld['branch_logo']; ?>"> | <?php echo $ld['branch_name']; ?>
                        </h2>
                        <p>
                            <?php echo $ld['branch_desc']; ?>
                        </p>
                        <input type="hidden" id="locationMap" value="<?php echo $ld['branch_name']; ?>"/>
                        <?php
                    }
                }
                ?>
            </div>


            <div class="row gb-gallery">
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>assets/images/soff-cricket/Lane_1_2_combined_2.jpg" style="width:100%;" class="gbg-col-1">
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="<?php echo base_url(); ?>assets/images/soff-cricket/Seating-Area.jpg" style="width:100%;" class="gbg-col-2">
                        </div>
                        <div class="col-md-6">
                            <img src="<?php echo base_url(); ?>assets/images/soff-cricket/Seating-area-2.jpg" style="width:100%;" class="gbg-col-2">
                        </div>
                        <div class="col-md-6">
                            <img src="<?php echo base_url(); ?>assets/images/soff-cricket/Lane_3_4_combined_2.jpg" style="width:100%;" class="gbg-col-3">
                        </div>
                        <div class="col-md-6">
                            <img src="<?php echo base_url(); ?>assets/images/soff-cricket/Store-Pic-2.jpg" style="width:100%;" class="gbg-col-3">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 30px;">
                <div class="gb-srv" style="margin: 0px;">
                    <div class="row">
                        <div class="col-md-12"><h5 class="sec-title">Amenities</h5></div>
                        <?php
                        if (isset($amt)) {
                            foreach ($amt as $am) {
                                $fid = $am['features_id'];
                                if ($fid == 1) {
                                    ?>
                                    <div class="col-md-3 gb-srv-item"><i class="fas fa-restroom"></i> Restroom</div>
                                <?php } elseif ($fid == 2) {
                                    ?>
                                    <div class="col-md-3 gb-srv-item"><i class="fas fa-car"></i> Car Park</div>
                                    <?php
                                } elseif ($fid == 3) {
                                    ?>
                                    <div class="col-md-3 gb-srv-item"><i class="fas fa-lightbulb"></i> Floodlights</div>                              <?php
                                } else {
                                    ?>
                                    <div class="col-md-3 gb-srv-item"><i class="fas fa-prescription-bottle"></i> Water Dispenser</div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="gb-map">
                    <div class="row">
                        <?php if (!$this->session->userdata('slot_data')) { ?>
                            <div class="col-md-12"><h5 class="sec-title">Location</h5></div>
                            <div class="col-md-12">
                                <div id="listingMap" style="height: 250px;">

                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-12">
                                <?php echo $this->session->userdata('slot_data') ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card" style="margin-bottom: 20px;">
                <h5 class="card-header gbc-header">Services</h5>
                <div class="card-body">
                    <?php
                    if (isset($list)) {
                        foreach ($list as $ls) {
                            ?>
                            <center><a class="btn btn-md btn-success" style="border-radius:50px;" href="<?php echo base_url(); ?>listing/all_services/<?php echo $ls['branch_id'] . '/' . str_replace(" ", "-", $ls['branch_name']); ?>">View Services</a></center>
                            <?php
                        }
                    }
                    ?>

                    <!--                    <div class="row">
                                            <div class="col-md-12">
                                                <ul class="list-sub-service">
                                                    <li>Sub Service 1</li>
                                                    <li>Sub Service 2</li>
                                                    <li>Sub Service 3</li>
                                                    <li>Sub Service 4</li>
                                                </ul>
                                            </div>
                                        </div>-->
                </div>
            </div>
            <div class="card">
                <h5 class="card-header gbc-header"> <center>Book a Slot</center></h5>
                <div class="card-body">
				
				 <?php if ($this->session->userdata('user_id')) { ?>
                        <center>
                            

                            <p align="center"><span  style="font-weight:bold">For reservations during:</span><br>
                                8am to 6pm on weekdays<br>
                                12am to 8am on all seven days<br>
                            </p>
                            <button type="button" style="border-radius:50px;margin-bottom:10px;" class="btn btn-md btn-primary" data-toggle="modal" onclick="setListing(<?php echo $bid; ?>);" data-target="#modalCart">Request a Custom Slot</button>
						<img src="<?php echo base_url(); ?>assets/images/or.png" style="margin-bottom:10px;">
                        </center>

                    <?php } ?>

                    <?php echo form_open('booking/search', array('class' => 'form')); ?>
                    <div class="row">
                        <div class="col-md-7" style="margin-bottom:10px;">

                            <select class="form-control" name="sid" id="sid" required>
                                <?php
                                $sid = $this->session->userdata('sid');
                                if (empty($sid)) {
                                    ?>
                                    <option value="">Service</option>
                                    <?php
                                }
                                if (isset($srv)) {
                                    foreach ($srv as $sv) {
                                        $lid = $sv['listingId'];
                                        if ($sid == $lid) {
                                            ?>
                                            <option value="<?php echo $sv['listingId']; ?>" selected><?php echo $sv['listingName']; ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $sv['listingId']; ?>"><?php echo $sv['listingName']; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="sect" value="branch">

                        </div>
                        <div class="col-md-5" style="margin-bottom:10px;">

                            <input type="text" name="bookdate" value="<?php
                            if ($this->session->userdata('book_date')) {
                                echo date('Y-m-d', $this->session->userdata('book_date'));
                            }
                            ?>" autocomple="off" readonly class="form-control DateFinder" required placeholder="Date" style="">

                        </div>
                    </div>
                    <center>
                        <button type="submit" name="Search" class="btn btn-info" style="background-color:#4bd6eb;border-color:#4bd6eb;margin-top:10px"><i class="fa fa-search"></i> Search</button>
                    </center>
                    <?php echo form_close(); ?>
                    <br/>
                    <?php if ($this->session->userdata('user_id')) { ?>
                                                                                                                                                                <!--<button type="button" class="btn btn-primary" data-toggle="modal" onclick="setListing(<?php echo $listingItem['listingId']; ?>);" data-target="#modalCart">Custom Slot</button>-->
                    <?php } ?>
                    <?php
                    if ($this->session->userdata('book_msg')) {
                        echo $this->session->userdata('book_msg');
                    }
//                    if ($this->session->userdata('slot_data')) {
//                        echo $this->session->userdata('slot_data');
//                    }
                    ?>

                   

                </div>
            </div>







        </div>
    </div>
    <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-cus" role="document">
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Make a Custom Request</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--Body-->
                <?php echo form_open('booking/customize_booking'); ?>
                <div class="modal-body">  
                    <div class="row">   	
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="date">Select a Date *</label>
                                        <input type="text" class="form-control DateFinder" placeholder="Select a Date" name="cdate" id="cdate" required>
                                        <input type="hidden" name="bid" id="bid">
                                        <input type="hidden" name="sec" value="branch">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="service">Select Services *</label>
                                        <select class="form-control" id="service" name="service[]" required>
                                            <option value="">Select Services</option>
                                            <?php
                                            if (isset($srv)) {
                                                foreach ($srv as $ldata) {
                                                    ?>
                                                    <option value="<?php echo $ldata['listingId'] ?>"><?php echo $ldata['listingName'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>                         
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="sfrom">From * (10:30 AM)</label>
                                        <input name="sfrom[]" id="sfrom" type="time" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="sto">To * (11:30 AM)</label>
                                        <input name="sto[]" id="sto" type="time" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="sto">Duration * (1.5)</label>
                                        <input name="duration[]" id="sto"  type="text" placeholder="1.5" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="btnadd"></label>
                                        <button id="btnAdd" type="button" value="Add" class="btn btn-success btn-add"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>

                            </div>
                            <div id="addServices"></div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="desc" class="form-control" placeholder="Notes"></textarea>
                            </div>
                        </div>

                    </div>
                    <!--Footer-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                        <input type="submit" name="request" class="btn btn-primary" value="Request">
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpHZC2DR6DiK9TfSBO-6Xr_JcLwIe94so&libraries=places&callback=initMapListing"
async defer></script>


<script>
<?php
if (isset($list)) {
    foreach ($list as $ld) {
        $lat = $ld['lat'];
        $lng = $ld['lng'];
    }
}
?>
                                function initMapListing() {
                                    var input = $('#locationMap').val();
                                    var myLatLng = {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>};

                                    var map = new google.maps.Map(document.getElementById('listingMap'), {
                                        zoom: 12,
                                        center: myLatLng
                                    });

                                    var marker = new google.maps.Marker({
                                        position: myLatLng,
                                        map: map,
                                        title: input
                                    });
                                }
</script>

<?php $this->load->view('page/temp/site_footer'); ?>



