 <link rel="stylesheet" type="text/css" href="http://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">   
    <div class="container main-container">
    <form enctype="multipart/form-data" method="post" action="<?php echo current_url()?>">
    <?php foreach ($listingData as $listingItem) { ?>
      <input type="hidden" class="form-control hidden" value="<?php echo $listingItem->listingId;?>" id="listingId" name="listingId" required >

        <div class="row">

            <div class="col-md-8">
            <div class="facility-detail-page-info"><h2 class="admin-page-title"> Edit Services</h2></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" id="" aria-describedby="" placeholder="Enter Title" id="listingName"  name="listingName" value="<?php echo $listingItem->listingName;?>" required>
                     </div>
                    <?php echo form_error('listingName', '<div class="error" style="color:red;">', '</div>'); ?>
                </div>
            </div>


<div class="row">
<div class="col-md-6">
<div class="form-group">
    <label for="exampleInputEmail1">Services</label>
    <select class="btn dropdown-toggle bn-search-dropdown" style="border-radius:0px;" id="editserviceName" name="serviceName" required>
    <option value="" >Select</option>
        <?php 
         $status = ''; 
        foreach ($allservices as $serviceItem) { ?>
                <option value="<?php echo $serviceItem['serviceId'];?>"  <?php if($serviceItem['serviceId'] == $listingItem->service_Id) echo "selected";?>><?php echo $serviceItem['serviceName'];?></option>
        <?php }?>
    </select>
  </div>
  <?php echo form_error('serviceName', '<div class="error" style="color:red;">', '</div>'); ?>

 </div> 
  
  
  <div class="col-md-6">
  <div class="form-group">
  
  <div class="form-group" id="addSubService">
  <label for="exampleInputEmail1">Add Sub Services</label>
    <br>
    <div class="panel panel-default">
  <div class="panel-body" >
  <select class="btn dropdown-toggle bn-search-dropdown" style="border-radius:0px;" id="viewSubedit" name="viewSubAdd" required>
  <option value="" >Select</option>
  <?php 
         $status = ''; 
        foreach ($subservices as $serviceItem) { ?>
                <option value="<?php echo $serviceItem['serviceId'];?>"  <?php if($serviceItem['serviceId'] == $listingItem->service_Id) echo "selected";?>><?php echo $serviceItem['serviceName'];?></option>
        <?php }?>
  </select>

  </div>
  <?php echo form_error('viewSubAdd', '<div class="error" style="color:red;">', '</div>'); ?>

    </div>
    </div>
  </div>
  
                    </div>
                    </div>

                    
<div class="row">
<div class="gb-list">
<div class="row">
 

<div class="col-md-12"><h5 class="sec-title">Location</h5></div>

 <div class="form-group col-md-4">
  <label for="exampleInputEmail1">Address Line 1</label>
  <input type="text" class="form-control"  aria-describedby="" placeholder="Address Line 1" id="address1" name="address1" value="<?php echo $listingItem->address1;?>"required>
  <?php echo form_error('address1', '<div class="error" style="color:red;">', '</div>'); ?>

</div>
 
 <div class="form-group col-md-4">
 <label for="exampleInputEmail1">Address Line 2</label>
  <input type="text" class="form-control" aria-describedby="" placeholder="Address Line 2" id="address2" name="address2" value="<?php echo $listingItem->address2;?>" >
 </div>
 
 <div class="form-group col-md-4">
  <label for="exampleInputEmail1">City / Town</label>
  <input type="text" class="form-control" id="pac"  name="pac" aria-describedby="" placeholder="City / Town" value="<?php echo $listingItem->location;?>" required>
  <?php echo form_error('pac', '<div class="error" style="color:red;">', '</div>'); ?>

</div>
 
</div>


<div class="row">
 
 <div class="form-group col-md-4">
 <label for="exampleInputEmail1">State / Province / Region</label>
  <input type="text" class="form-control" id="addRegion" name="addRegion"  aria-describedby="" placeholder="Enter State / Province / Region " value="<?php echo $listingItem->region;?>" required>
  <?php echo form_error('addRegion', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>


  <div class="row" style="display:none;">
    <div class="col-md-6">
    <div class="form-group">
    <input type="hidden" class="form-control" id="latbox" name="latbox" required value="<?php echo $listingItem->lat;?>">
  </div>
    </div>
    <div class="col-md-6">
  <div class="form-group">
    <input type="hidden" class="form-control " id="lngbox" name="lngbox" required value="<?php echo $listingItem->lng;?>">
  </div>
    </div>
  </div>


<div class="form-group col-md-4">
  <label for="exampleInputEmail1">Country</label>
  <select id="addCountry" name="addCountry" class="browser-default custom-select" required>
                            <option value="">Select a country</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua and Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia</option>
                            <option value="BA">Bosnia and Herzegowina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei Darussalam</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CA">Canada</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (Keeling) Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CD">Congo, the Democratic Republic of the</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">Cote d'Ivoire</option>
                            <option value="HR">Croatia (Hrvatska)</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="TP">East Timor</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands (Malvinas)</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="FX">France, Metropolitan</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard and Mc Donald Islands</option>
                            <option value="VA">Holy See (Vatican City State)</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran (Islamic Republic of)</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KP">Korea, Democratic People's Republic of</option>
                            <option value="KR">Korea, Republic of</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao People's Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libyan Arab Jamahiriya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macau</option>
                            <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States of</option>
                            <option value="MD">Moldova, Republic of</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="KN">Saint Kitts and Nevis</option>
                            <option value="LC">Saint LUCIA</option>
                            <option value="VC">Saint Vincent and the Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome and Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia (Slovak Republic)</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SH">St. Helena</option>
                            <option value="PM">St. Pierre and Miquelon</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syrian Arab Republic</option>
                            <option value="TW">Taiwan, Province of China</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania, United Republic of</option>
                            <option value="TH">Thailand</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad and Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks and Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="US">United States</option>
                            <option value="UM">United States Minor Outlying Islands</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VE">Venezuela</option>
                            <option value="VN">Viet Nam</option>
                            <option value="VG">Virgin Islands (British)</option>
                            <option value="VI">Virgin Islands (U.S.)</option>
                            <option value="WF">Wallis and Futuna Islands</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="YU">Yugoslavia</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select>
 </div>

  <?php echo form_error('addCountry', '<div class="error" style="color:red;">', '</div>'); ?>

</div>
</div>
</div>



<div class="row">
<div class="gb-map">
    <div class="row">
    
        <div class="col-md-12">
        <div id="map" style="height: 250px;"></div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="gb-list">
        <div class="row">
        <div class="col-md-12"><h5 class="sec-title">Note</h5></div>
            <div class="col-md-12">
                <textarea id="editor1" name="editor" style="border:none;">
                <?php echo $listingItem->description;?>
    </textarea>


            </div>
           </div>
           
      
           
        </div>
         </div>

                 <div class="row" style="margin-top:30px;">
        
        <div class="col-md-12">
     

       <div class="card">
                         <h5 class="card-header gbc-header">Create Time Slot</h5>
                         <div class="card-body ">

    <table class="col-md-12">
    <?php foreach ($allSlots as $slot) { ?>

 <tr >                 
                             <td class=" gbc-item"><?php echo $slot['slot_name'];?></td>
                             <td class=" gbc-item" > 
                             <?php $date=date_create($slot['slot_from']);?>
                             <?php echo  date_format($date,"Y-m-d H:i ");?>
                             </td>
                             <td class=" gbc-item" >
                             <?php $date2=date_create($slot['slot_to']);?>
                             <?php echo  date_format($date2,"Y-m-d H:i ");?>
                             </td>
                             <td class=" gbc-item">
                             <a href="<?php echo base_url();?>listing/slot/<?php echo $slot['slotId'];?>/<?php echo $listingItem->listingId;?>" ><i class="fa fa-window-close g-plus-icon" style="color: #ff365b;font-size: 28px;"></i></a>
                             </td>
</tr>

        <?php }?>
</table>      

<table id="productTable" class="col-md-12">
 <!-- <tr>                 
                             <td class=" gbc-item"><input type='text' class="form-control" id='creatName' placeholder="Slot Name" name="creatName[]" required/></td>
                             <td class=" gbc-item"><input type='text' class="form-control creatSlotFrom " id='creatSlotFrom' name="creatSlotFrom[]" placeholder="From :" autocomplete="off" required/></td>
                             <td class=" gbc-item"><input type='text' class="form-control creatSlotTo" id='creatSlotTo' name="creatSlotTo[]" placeholder="To :" autocomplete="off" required/></td>
                             <td class=" gbc-item"></td>
</tr> -->
</table>
<i id="addnew" class="fas fa-plus-square g-plus-icon"  style="font-size: 28px;"></i>
                         </div>
                        
                       </div>
           
         </div>
     </div>
     
     
     
     

            </div>

             <div class="col-md-4" style="margin-top:40px;">
             <div class="row">
 <div class="col-md-12">
<div class="gb-srv">
<h5 class="sec-title">Hourly rate</h5>
<div class="row" style="margin-bottom:20px;">
<div class="col-md-6 gb-srv-item">
  <input type='text' class="form-control" id='price' name="price" placeholder="$" onkeypress="return isNumberKey(event)" required value="<?php echo $listingItem->price;?>"/></div>
  <?php echo form_error('price', '<div class="error" style="color:red;">', '</div>'); ?>

</div>

    
  <h5 class="sec-title">Add Amenities</h5>
  <?php foreach ($allfeatures as $Item) { ?>
    <div class="col-md-12 gb-srv-item">
    <input type="checkbox" name="features[]" id="features" value="<?php echo $Item['featuresId'];?>" class="form-check-input" <?php if($Item['selected'] == 1) echo "checked";?>> <?php echo $Item['featuresName'];?><br>
    <!-- <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">  Restroom  -->
    <!-- <i class="fas fa-restroom"></i> -->
    </div>
  <?php } ?>

    <h5 class="sec-title" style="margin-top:30px;">Images Upload</h5>
 
    <div class="row">
    <?php foreach ($allImages as $Item) { ?>
     <div class="col-md-3 gfy-small-img">
     <div class="row" style="padding:0px;">
        <div class="col-md-4">
        <a href="<?php echo base_url();?>listing/deleteImage/<?php echo $Item['listingImageId'];?>/<?php echo $listingItem->listingId;?>" ><i class="fa fa-window-close g-plus-icon" style="color: #ff365b;font-size: 15px;"></i></a>
        </div>
        <div class="col-md-8" style="padding:0px;">
        <img src="<?php echo base_url();?>global/uploads/listing/<?php echo $Item['image'];?>" class="img-fluid" style="height:50px;width:100px;">
        </div>
     </div>
    </div>
    
    <?php }?>

    
   
    </div>
    <?php if(sizeof($allImages) < 5){?>
    <div class="custom-file col-md-12" style="margin-top:20px;">
    <input id="files" multiple="multiple" class="form-control multi with-preview" type="file" name="files[]" maxlength="5" accept="image/x-png,image/gif,image/jpeg,image/jpg"/>
  <!-- <input type="file" class="custom-file-input" id="customFile"> -->
  <!-- <label class="custom-file-label" for="customFile">choose File</label> -->


</div>
<?php }?>
    
  
</div>
</div>
    </div>
                   
    </div>           
           
            </div>
  <?php }?>
            <div class="row submit-btn-con">
                               
                               <div class=" gbc-item" >
                               <button type="submit" class="btn gbc-btn"  name="edit" id="edit" style="padding: 10px;margin-left:10px;">Edit Services</button>
                               </div>
                               </div>
        </div>
        </form>

    </div>
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>
<script src="<?= base_url() ?>global/Multifile/js/jquery.MultiFile.js"></script>
 
<script type="text/javascript">
            function isNumberKey(evt){
                 var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
             return false;
            return true;
        }
 </script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpHZC2DR6DiK9TfSBO-6Xr_JcLwIe94so&libraries=places&callback=initMapListing"
          async defer></script>
    

  <script>
    let serviceCityEdit = $("#pac").val();

          function initMapListing() {
            var input = document.getElementById('pac');
            var options = {
            types: ['(cities)'],
            };
            var geocoder = new google.maps.Geocoder();

     map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 0, lng: 0},
    zoom: 12
  });

  geocoder.geocode({'address': serviceCityEdit}, function(results, status) {
    if (status === 'OK') {
      map.setCenter(results[0].geometry.location);
      document.getElementById("latbox").value = results[0].geometry.location.lat();
      document.getElementById("lngbox").value = results[0].geometry.location.lng();
      let city = [];
          let province = [];
          let googleCountry = [];
          let locationSelect = results[0].address_components.forEach((option) => {
            if(option.types.indexOf('administrative_area_level_1') >= 0 && option.types.indexOf('political') >= 0 ){
            province.push(option)
            return province;
           }
           if(option.types.indexOf('locality') >= 0 && option.types.indexOf('political') >= 0){
            city.push(option)
            return city;
           }
           if(option.types.indexOf('country') >= 0 && option.types.indexOf('political') >= 0){
            googleCountry.push(option)
            return googleCountry;
           }          
        })
            if(googleCountry[0] && googleCountry[0].short_name){
                $('#addCountry').val(googleCountry[0].short_name)
            }
    } else {
    //   alert('Geocode was not successful for the following reason: ' + status);
    }
  });



            var autocomplete = new google.maps.places.Autocomplete(input,options);

            autocomplete.addListener('place_changed', function() {
              var place = autocomplete.getPlace();
             
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                // window.alert("No details available for input: '" + place.name + "'");
                return;
              }
        
          
              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }

              document.getElementById("latbox").value = place.geometry.location.lat();
              document.getElementById("lngbox").value = place.geometry.location.lng();
              map.setCenter(place.geometry.location);

              let city = [];
          let province = [];
          let googleCountry = [];
          let locationSelect = place.address_components.forEach((option) => {
            if(option.types.indexOf('administrative_area_level_1') >= 0 && option.types.indexOf('political') >= 0 ){
            province.push(option)
            return province;
           }
           if(option.types.indexOf('locality') >= 0 && option.types.indexOf('political') >= 0){
            city.push(option)
            return city;
           }
           if(option.types.indexOf('country') >= 0 && option.types.indexOf('political') >= 0){
            googleCountry.push(option)
            return googleCountry;
           }          
        })


            if(province[0] && province[0].long_name){
                document.getElementById("addRegion").value = province[0].long_name;
            }
        
            if(googleCountry[0] && googleCountry[0].short_name){
                $('#addCountry').val(googleCountry[0].short_name)
            }
                    
            });
        
        
          }
                </script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace('editor1', {
    language: 'en',
            uiColor: '#9AB8F3'
    });
</script>
