
    
    <div class="container main-container">
      
      <div class="row">
      <div class="col-md-12 title-holder">
      <h2>Bookings</h2>
      </div>
      </div>
      
      
        <div class="row">
             
            <div class="col-md-9">

<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Up Coming</a>
						<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">History</a>
						
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                
					<table id="upcomingvendor" class="table table-striped table-bordered" style="width:100%">
                    <thead>
  <tr>
  <th scope="col">#</th>
    <th scope="col">Customer</th>
    <th scope="col">Service</th>
    <th scope="col">From</th>
    <th scope="col">To</th>
    <th scope="col">Price</th>
    <th scope="col">Status</th>
    <th scope="col">&nbsp;</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($upcomingData as $serviceItem) { ?>

  <tr>
    <td> GB-<?php echo $serviceItem->booking_Id;?></td>
    <td><?php echo $serviceItem->name;?></td>
    <td><?php echo $serviceItem->serviceName;?></td>
    <td>
    <?php $date=date_create($serviceItem->booking_from);?>
     <?php echo  date_format($date,"Y-m-d H:i ");?>
    </td>
    <td>
    <?php $date=date_create($serviceItem->booking_to);?>
     <?php echo  date_format($date,"Y-m-d H:i ");?>
    </td>
    <td>$ <?php echo $serviceItem->amount;?></td>
    <td><?php echo $serviceItem->orderStatus;?></td>
    <td>
    <?php if($serviceItem->orderStatus == 'Pending'){?>

<a  href="<?php echo base_url();?>dashboard/approve/<?php echo $serviceItem->booking_Id;?>" class="btn btn-success btn-sm" style="color:#fff;"> Approve</a>
<?php }?>
      <!-- <button type="button" class="btn btn-success btn-sm">Approve</button> -->
      
       <a href="#"><i class="fas fa-eye"></i></a> </td>
  </tr>
  <?php }?>
 </tbody>
</table>
			</div>
					<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
																<table id="historyvendor" class="table table-striped table-bordered" style="width:100%">
                    <thead>
  <tr>
  <th scope="col">#</th>
    <th scope="col">Customer</th>
    <th scope="col">Service</th>
    <th scope="col">From</th>
    <th scope="col">To</th>
    <th scope="col">Status</th>
    <th scope="col">&nbsp;</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($allData as $serviceItem) { ?>

  <tr>
    <td>GB-<?php echo $serviceItem->booking_Id;?> </td>
    <td><?php echo $serviceItem->name;?></td>
    <td><?php echo $serviceItem->serviceName;?></td>
    <td>
    <?php $date=date_create($serviceItem->booking_from);?>
     <?php echo  date_format($date,"Y-m-d H:i ");?>
    </td>
    <td>
    <?php $date=date_create($serviceItem->booking_to);?>
     <?php echo  date_format($date,"Y-m-d H:i ");?>
    </td>
    <td><?php echo $serviceItem->orderStatus;?></td>
    <td> <a href="#"><i class="fas fa-eye"></i></a> </td>
  </tr>
  <?php }?>
  
 </tbody>
</table>
					</div>
				
</div>
<!-- ./Tabs -->

        
  
        
        </div>
        
        
        
     

          
            
        
  <div class="col-md-3 bkg-right">
         
   <div class="calendar calendar-first" id="calendar_first">
    <div class="calendar_header">
        <button class="switch-month switch-left"> <i class="fa fa-chevron-left"></i></button>
         <h2></h2>
        <button class="switch-month switch-right"> <i class="fa fa-chevron-right"></i></button>
    </div>
    <div class="calendar_weekdays"></div>
    <div class="calendar_content"></div>
</div>

				
    
  

    </div>
                   
                   
                   
        
        
        </div>
   </div>
  
  
 
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> -->
    <!-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->
     <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" /> -->
      <!-- <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" /> -->

   <!-- <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

    
   <!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>

 <script src="<?php echo base_url();?>assets/vendor/jquery/calendar.js"></script>
        <script>
      
    $(document).ready(function() {
    $('#upcomingvendor').DataTable();
	$('#historyvendor').DataTable();
} );
        </script>







