
      <div class="container main-container">
      
      <div class="row title-holder">
      <div class="col-md-6">
      <h2 >Company Profile</h2>
      </div>
      </div>
      
       
      
      <div class="row">
      <div class="col-md-3">
      
       <div class="col-md-12">
       <div class="alert alert-danger" role="alert" id="sizeError">
           Upload less or equal to 10 MB
        </div>
       <img src="<?php echo base_url();?>global/default.png" class="pro-image">
       </div>
       <div class="col-md-12">
       <input type="file" class="custom-file-input" id="customFileProfile" name="customFileProfile">
  <label class="custom-file-label" for="customFileProfile">Company Logo</label>
  </div>
      </div>
      
      
      <div class="col-md-9 pro-right">

      <?php if (isset($successprofile) == 'success') { ?>
<div class="alert alert-success">
<label>Successfully updated.</label> 
</div>
<?php } ?>
      <?php  foreach ($userDate as $item) { ?>

      <form enctype="multipart/form-data" method="post" action="<?php echo current_url()?>">
      <div class="row">
      <div class="form-group col-md-6">
  <label for="exampleInputEmail1">Catergory</label>
   <select id="catergory" name="catergory" class="browser-default custom-select" required >
   <option value="" >please select a catergory</option>
        <?php 
        foreach ($services as $serviceItem) { ?>
                <option value="<?php echo $serviceItem->userServiceCatId;?>" <?php if($serviceItem->userServiceCatId == $item->userCatergory) echo "selected";?>><?php echo $serviceItem->uServiceName;?></option>
        <?php }?>
    </select>
 </div>
 
 <div class="form-group col-md-6">
 <label for="exampleInputEmail1">Company Name</label>
  <input type="text" class="form-control" id="companyname" name="companyname" aria-describedby="" placeholder="Company Name" value="<?php echo $item->companyName;?>" required>
  <?php echo form_error('companyname', '<div class="error" style="color:red;">', '</div>'); ?>
 </div>
 
      </div>
      
      
      <div class="row">
      <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Owner First Name</label>
  <input type="text" class="form-control" id="firstname" name="firstname" aria-describedby="" placeholder="First Name" value="<?php echo $item->name;?>" required>
  <?php echo form_error('firstname', '<div class="error" style="color:red;">', '</div>'); ?>

      </div>
       <div class="form-group col-md-6">
         <label for="exampleInputEmail1">Owner Last Name</label>
  <input type="text" class="form-control" id="lastname" name="lastname"  aria-describedby="" placeholder="Last Name" value="<?php echo $item->lname;?>" required>
  <?php echo form_error('lastname', '<div class="error" style="color:red;">', '</div>'); ?>

      </div>
      </div>
      
       <hr/>
      <div class="row">
      <div class="col-md-12"><h5 class="sec-title">Contact Details</h5></div>
       <div class="form-group col-md-4">
  <label for="exampleInputEmail1">Phone</label>
  <input type="text" class="form-control" id="phone" name="phone" aria-describedby="" placeholder="Phone" value="<?php echo $item->phonenumber;?>" required onkeypress="return isNumberKey(event)">
 
  <?php echo form_error('phone', '<div class="error" style="color:red;">', '</div>'); ?>
</div>
  <div class="form-group col-md-4">
  <label for="exampleInputEmail1">Mobile</label>
  <input type="text" class="form-control" id="mobile" name="mobile"  aria-describedby="" placeholder="Mobile" value="<?php echo $item->mobileNumber;?>" required onkeypress="return isNumberKey(event)">
  <?php echo form_error('mobile', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>
  <div class="form-group col-md-4">
  <label for="exampleInputEmail1">Contact E-mail</label>
  <input type="email" class="form-control" id="email" name="email" aria-describedby="" placeholder="E-mail" value="<?php echo $item->contactEmail;?>" required>
  <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>
      </div>
      
      <hr/>
      
      <div class="row">
 

<div class="col-md-12"><h5 class="sec-title">Location</h5></div>

 <div class="form-group col-md-4">
  <label for="exampleInputEmail1">Address Line 1</label>
  <input type="text" class="form-control"  id="address1" name="address1" aria-describedby="" placeholder="Address Line 1" value="<?php echo $item->address1;?>" required>
  <?php echo form_error('address1', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>
 
 <div class="form-group col-md-4">
 <label for="exampleInputEmail1">Address Line 2</label>
  <input type="text" class="form-control" id="address2" name="address2" aria-describedby="" placeholder="Address Line 2" value="<?php echo $item->address2;?>">
  <?php echo form_error('address2', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>
 
 <div class="form-group col-md-4">
  <label for="exampleInputEmail1">City / Town</label>
  <input type="text" class="form-control" id="profile-city"  name="profile-city" aria-describedby="" placeholder="City / Town" value="<?php echo $item->city;?>" required>
  <?php echo form_error('profile-city', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>
 
</div>


<div class="row">
 
 <div class="form-group col-md-4">
 <label for="exampleInputEmail1">State / Province / Region</label>
  <input type="text" class="form-control" id="profileRegion"  name="profileRegion" aria-describedby="" placeholder="Enter State / Province / Region " value="<?php echo $item->region;?>" required>
  <?php echo form_error('profileRegion', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>

<div class="form-group col-md-4">
  <label for="exampleInputEmail1">Country</label>
  <select id="profileCountry" name="profileCountry" class="browser-default custom-select" required>
                            <option value="" selected="selected">(please select a country)</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua and Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BT">Bhutan</option>
                            <option value="BO">Bolivia</option>
                            <option value="BA">Bosnia and Herzegowina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei Darussalam</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CA">Canada</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Central African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (Keeling) Islands</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CD">Congo, the Democratic Republic of the</option>
                            <option value="CK">Cook Islands</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">Cote d'Ivoire</option>
                            <option value="HR">Croatia (Hrvatska)</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DK">Denmark</option>
                            <option value="DJ">Djibouti</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="TP">East Timor</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands (Malvinas)</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FR">France</option>
                            <option value="FX">France, Metropolitan</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Southern Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="DE">Germany</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard and Mc Donald Islands</option>
                            <option value="VA">Holy See (Vatican City State)</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran (Islamic Republic of)</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KP">Korea, Democratic People's Republic of</option>
                            <option value="KR">Korea, Republic of</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao People's Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libyan Arab Jamahiriya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macau</option>
                            <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States of</option>
                            <option value="MD">Moldova, Republic of</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanmar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="NL">Netherlands</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NZ">New Zealand</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="KN">Saint Kitts and Nevis</option>
                            <option value="LC">Saint LUCIA</option>
                            <option value="VC">Saint Vincent and the Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome and Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="SC">Seychelles</option>
                            <option value="SL">Sierra Leone</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia (Slovak Republic)</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                            <option value="ES">Spain</option>
                            <option value="LK">Sri Lanka</option>
                            <option value="SH">St. Helena</option>
                            <option value="PM">St. Pierre and Miquelon</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                            <option value="SZ">Swaziland</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="SY">Syrian Arab Republic</option>
                            <option value="TW">Taiwan, Province of China</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania, United Republic of</option>
                            <option value="TH">Thailand</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad and Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks and Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="GB">United Kingdom</option>
                            <option value="US">United States</option>
                            <option value="UM">United States Minor Outlying Islands</option>
                            <option value="UY">Uruguay</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VE">Venezuela</option>
                            <option value="VN">Viet Nam</option>
                            <option value="VG">Virgin Islands (British)</option>
                            <option value="VI">Virgin Islands (U.S.)</option>
                            <option value="WF">Wallis and Futuna Islands</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="YU">Yugoslavia</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select>


            <?php echo form_error('profileCountry', '<div class="error" style="color:red;">', '</div>'); ?>

 </div>


</div>

 <div class="row">
                               
                                <div class="col-md-3 gbc-item">
                                <button class="btn btn-primary" name="update" id="update">Update</button>
                                </div>
                                </div>
      </form>
      <?php }?>

   
                                <hr/>
                                <form enctype="multipart/form-data" method="post" action="<?php echo current_url()?>">

                          <div class="row">
                          <div class="col-md-12"><h5 class="sec-title">Reset Password</h5>
                          <?php if (isset($successReset) == 'success') { ?>
                            <div class="alert alert-success">
                            <label>Password changed.</label> 
                            </div>
                          <?php } ?>
                          </div>
                       
                           <div class="col-md-6">
                            <label for="exampleInputEmail1">New Password</label>
                           <input type="password" class="form-control" id="password" aria-describedby="" placeholder="New Password" name="password" required>
                           <?php echo form_error('password', '<div class="error" style="color:red;">', '</div>'); ?>

                           </div>
                            <div class="col-md-6">
                             <label for="exampleInputEmail1">Confirme Password</label>
                           <input type="password" class="form-control" id="repassword" aria-describedby="" placeholder="Confirme Password" name="repassword" required>
                           <?php echo form_error('repassword', '<div class="error" style="color:red;">', '</div>'); ?>

                           </div>
                          </div>     
                         <br/>
 <div class="row">
                            <div class="col-md-3 gbc-item">
                                <button class="btn btn-primary" name="reset" id="reset">Reset</button>
                                </div>
                                </div> 

</div>
                                </form>

</div>
      
      
      
      
      </div>
      </div>
      
        
        </div>
   </div>

   <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpHZC2DR6DiK9TfSBO-6Xr_JcLwIe94so&libraries=places&callback=initMapListing"
          async defer></script>
    

  <script>
    let serviceCityEdit = $("#profile-city").val();

          function initMapListing() {
            var input = document.getElementById('profile-city');
            var options = {
            types: ['(cities)'],
            };
            var geocoder = new google.maps.Geocoder();


  geocoder.geocode({'address': serviceCityEdit}, function(results, status) {
    if (status === 'OK') {
      let city = [];
          let province = [];
          let googleCountry = [];
          let locationSelect = results[0].address_components.forEach((option) => {
            if(option.types.indexOf('administrative_area_level_1') >= 0 && option.types.indexOf('political') >= 0 ){
            province.push(option)
            return province;
           }
           if(option.types.indexOf('locality') >= 0 && option.types.indexOf('political') >= 0){
            city.push(option)
            return city;
           }
           if(option.types.indexOf('country') >= 0 && option.types.indexOf('political') >= 0){
            googleCountry.push(option)
            return googleCountry;
           }          
        })
            if(googleCountry[0] && googleCountry[0].short_name){
                $('#profileCountry').val(googleCountry[0].short_name)
            }
    } 
  });



            var autocomplete = new google.maps.places.Autocomplete(input,options);

            autocomplete.addListener('place_changed', function() {
              var place = autocomplete.getPlace();
             
              if (!place.geometry) {
                return;
              }
        
          
              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }


              let city = [];
          let province = [];
          let googleCountry = [];
          let locationSelect = place.address_components.forEach((option) => {
            if(option.types.indexOf('administrative_area_level_1') >= 0 && option.types.indexOf('political') >= 0 ){
            province.push(option)
            return province;
           }
           if(option.types.indexOf('locality') >= 0 && option.types.indexOf('political') >= 0){
            city.push(option)
            return city;
           }
           if(option.types.indexOf('country') >= 0 && option.types.indexOf('political') >= 0){
            googleCountry.push(option)
            return googleCountry;
           }          
        })


            if(province[0] && province[0].long_name){
                document.getElementById("profileRegion").value = province[0].long_name;
            }
        
            if(googleCountry[0] && googleCountry[0].short_name){
                $('#profileCountry').val(googleCountry[0].short_name)
            }
                    
            });
        
        
          }
                </script>


                <script type="text/javascript">
            function isNumberKey(evt){
                 var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
             return false;
            return true;
        }
 </script>

 <script type="text/javascript">
 $('#sizeError').hide();

 $('#customFileProfile').bind('change', function() {
   if(this.files[0].size >= 10000000){
    $('#sizeError').show();

   }else{
    $('#sizeError').hide();
    console.log(this.files[0])
    // $.ajax({
    //        url: '../../listing/get_by_edit_service/'+inputValue+'/'+listingId,
    //        type: 'GET',
    //        contentType: 'application/json; charset=utf-8',
    //        dataType: "json",
    //        error: function() {
    //         $html += '<option value=""> Select</option>';
    //             $('#viewSubedit').empty().append($html);
    //        },
    //        success: function(data) {
    //         $html +='';
    //            if(data && data.length){
    //             $.each( data, function( key, value ) {
    //             if(value != null){
    //                 if( value.selected == true){
    //                     $html += '<option value="'+value.serviceId+'" selected> '+value.serviceName+'</option>';

    //                 }else{
    //                     $html += '<option value="'+value.serviceId+'" > '+value.serviceName+'</option>';
    //                 }
    //             }
                 
    //             })
    //             $('#viewSubedit').empty().append($html);
    //            }else{
    //             $html += '<option value=""> Select</option>';
    //             $('#viewSubedit').empty().append($html);
    //            }
    //        }
    //     });

   }
})
 </script>


