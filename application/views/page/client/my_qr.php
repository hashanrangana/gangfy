<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/client_sidebar');
?>

<script src=
        "https://files.codepedia.info/files/uploads/iScripts/html2canvas.js">
</script>

<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">


        <!-- Begin Page Content -->
        <er_ class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800" style="padding-left: 11px;">QR Code</h1>
            <!--a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate QR</a-->
          </div>


               <div class="row">
                   <div class="col-xl-3 col-md-6 mb-4">
                   </div>
                        <div class="col-xl-6 col-md-6 mb-4">
                          <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                              <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Download QR Here <?php $id = $this->session->userdata('user_id'); ?></div>
                                </div>
                                <div class="col-auto">
                                    <div id="html-content-holder" style="background-color: #F0F0F1; color: #00cc65; width: 275px;padding-left: 9px; padding-top: 10px;"></div>
                                    <div id="output"></div>
                                    <div  style="text-align: center; padding-top: 23px;"><a id="btn-Convert-Html2Image" href="#"><button type="button" class="btn btn-outline-primary">Download</button></a></div>
                                    <div id="previewImage" style="display:none"> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
               </div>

            <!--h3 class="h3 mb-0 text-gray-800">ID : <?php echo $id = $this->session->userdata('user_id'); ?></h3-->
            <input type="hidden" id="hdnSession" data-value="<?php echo $id = $this->session->userdata('user_id'); ?>" />
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<!-- QR JS STATIC -->
<script>
    jQuery(function(){
        var sessName = '<?php echo $_SESSION['user_id']?>';
        console.log(sessName);
        jQuery('#html-content-holder').qrcode( sessName);
    })
</script>
<!-- ------------ -->


    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
<?php
$this->load->view('page/temp/portal_footer');
?>
