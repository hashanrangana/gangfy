<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/client_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Modify Booking</h1>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <?php echo form_open('booking/booking_modify'); ?>
                    <div class="row">
                        <?php
                        if (isset($book)) {
                            foreach ($book as $bd) {
                                $desc = $bd['request_desc'];
                                ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="date">Select Date</label>
                                        <input type="text" class="form-control DateFinder" value="<?php echo $bd['request_date']; ?>" placeholder="Select Date" name="cdate" id="cdate" required>
                                        <input type="hidden" name="bid" id="bid" value="<?php echo $bd['br_id']; ?>">
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="row">
                        <?php
                        if (isset($slot)) {
                            foreach ($slot as $sd) {
                                $li = $sd['listingId'];
                                ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="service">Select Services *</label>
                                        <select class="form-control" id="service" name="service[]" required>
                                            <option value="">Select Services</option>
                                            <?php
                                            if (isset($list)) {
                                                foreach ($list as $ldata) {
                                                    $lid = $ldata['listingId'];
                                                    if ($li == $lid) {
                                                        ?>
                                                        <option value="<?php echo $ldata['listingId']; ?>" selected><?php echo $ldata['listingName']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $ldata['listingId']; ?>"><?php echo $ldata['listingName']; ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>                         
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="sfrom">Time From *</label>
                                        <input name="sfrom[]" id="sfrom" type="time" value="<?php echo $sd['req_from']; ?>" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="sfrom">Time To *</label>
                                        <input name="sto[]" id="sto" type="time" value="<?php echo $sd['req_to']; ?>" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="duration">Duration *</label>
                                        <input name="duration[]" id="duration" type="text" value="<?php echo $sd['duration']; ?>" placeholder="1 Hour" class="form-control" required>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="desc" class="form-control" placeholder="Note"><?php echo $desc; ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">                
                                <button type="submit" class="btn btn-info btn-md">Modify</button>
                                <a href="<?php echo base_url(); ?>booking/custom_booking" class="btn btn-primary btn-md">Go Back</a>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>

                <!-- end row -->


            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

