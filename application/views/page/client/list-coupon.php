<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/client_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Coupons</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Vendor</th>
                                    <th scope="col">Service Name</th>
                                    <th scope="col">Coupon Code</th>
                                    <th scope="col">Quota</th>
                                    <th scope="col">Description</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($cup)) {
                                    foreach ($cup as $cdata) {
                                        ?>
                                        <tr>
                                            <td><?php echo $cdata['name']; ?></td>
                                            <td><?php echo $cdata['listingName']; ?></td>
                                            <td><?php echo $cdata['coupon_code'] . 'C' . $cdata['customer_id']; ?></td>
                                            <td><?php echo $cdata['quota'] . $cdata['coupon_type']; ?></td>
                                            <td><?php echo $cdata['coupon_desc']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

