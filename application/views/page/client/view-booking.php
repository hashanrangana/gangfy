<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/client_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Booking Details</h1>
            </div>


            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <?php
                            if (isset($book)) {
                                foreach ($book as $bd) {
                                    ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="fname">First Name : <?php echo $bd['name']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="lname">Last Name : <?php echo $bd['lname']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email">Email : <?php echo $bd['userinfo_username']; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pcode">Phone Numbers : <?php echo $bd['phonenumber_one'] . ' / ' . $bd['phonenumber_two']; ?></label>
                                        </div>
                                    </div>   
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pcode">Booking Date : <?php echo $bd['request_date']; ?></label>
                                        </div>
                                    </div>  
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="pcode">Details : <?php echo $bd['request_desc']; ?></label>
                                        </div>
                                    </div> 
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Service Name</th>
                                            <th scope="col">From Time</th>
                                            <th scope="col">To Time</th>
                                            <th scope="col">Duration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($slot)) {
                                            foreach ($slot as $sd) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $sd['listingName']; ?></td>
                                                    <td><?php echo $sd['req_from']; ?></td>
                                                    <td><?php echo $sd['req_to']; ?></td>
                                                    <td><?php echo $sd['duration']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- end row -->
                </div>

            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

