<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/client_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">My Custom Bookings</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($book)) {
                                    foreach ($book as $bdata) {
                                        ?>

                                        <tr>
                                            <td><a href="#"><?php echo $bdata['name']; ?></a></td>
                                            <td><?php echo $bdata['request_date']; ?></td>
                                            <td><?php echo $bdata['request_desc']; ?></td>
                                            <td class="text-center"><?php
                                                $sid = $bdata['status'];
                                                if ($sid == 1) {
                                                    echo "<span class=\"approve-btn\">Approved</span>";
                                                } else if ($sid == 3) {
                                                    echo "<span class=\"reject-btn\">Rejected</span>";
                                                } else {
                                                    echo "<span class=\"pending-btn\">Pending</span>";
                                                }
                                                ?></td>
                                            <td  class="text-center" style="width:150px;">
                                                <a href="<?php echo base_url(); ?>booking/booking_details/<?php echo $bdata['br_id']; ?>" target="_blank" title="View Booking"><button class="btn btn-sm btn-success"><i class="fa fa-eye"></i></button></a>
                                                <a href="<?php echo base_url(); ?>booking/modify_booking/<?php echo $bdata['br_id']; ?>" target="_blank" title="Modify Booking"><button class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->


<?php $this->load->view('page/temp/portal_footer'); ?>

