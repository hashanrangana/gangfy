<?php $this->load->view('page/temp/portal_header'); ?>
<?php
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/client_sidebar');
$this->load->view('page/temp/header-message');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">My Profile</h1>
            </div>


            <div class="row">
                <div class="row">
                    <form method="post" action="<?php echo base_url(); ?>home/update_profile" enctype="multipart/form-data">
                        <div class="col-md-12">
                            <div class="row">
                                <?php
                                if (isset($prof)) {
                                    foreach ($prof as $pd) {
                                        ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <?php if (!empty($pd['image'])) { ?>
                                                    <img src="<?php echo base_url(); ?><?php echo $pd['image']; ?>"  style="width: 150px;height: 150px;">
                                                <?php } else { ?>
                                                    <img src="<?php echo base_url(); ?>assets/img/user-profile.png" style="width: 150px;height: 150px;">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="pimage">Change Profile Image</label>
                                                <input id="files" class="form-control" type="file" name="userfile" maxlength="5" />
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="fname">First Name *</label>
                                                <input type="text" class="form-control" name="fname" value="<?php echo $pd['name']; ?>" id="fname" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="lname">Last Name *</label>
                                                <input type="text" class="form-control" name="lname" value="<?php echo $pd['lname']; ?>" id="lname" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cname">Company Name </label>
                                                <input type="text" class="form-control" name="cname" value="<?php echo $pd['companyName']; ?>" id="pnum">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="email">Additional Email </label>
                                                <input type="text" class="form-control" name="email" value="<?php echo $pd['contactEmail']; ?>" id="email">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="pcode">Phone Number One *</label>
                                                <input type="text" class="form-control" name="pone" value="<?php echo $pd['phonenumber_one']; ?>" id="pone" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="pnum">Phone Number Two</label>
                                                <input type="text" class="form-control" name="ptwo" value="<?php echo $pd['phonenumber_two']; ?>" id="ptwo">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="adone">Address Line One *</label>
                                                <input type="text" class="form-control" name="adone" value="<?php echo $pd['address1']; ?>" id="adone" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="adtwo">Address Line Two</label>
                                                <input type="text" class="form-control" name="adtwo" value="<?php echo $pd['address2']; ?>" id="adtwo">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="city">City *</label>
                                                <input type="text" class="form-control" name="city" value="<?php echo $pd['city']; ?>" id="city" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="region">Region *</label>
                                                <input type="text" class="form-control" name="region" value="<?php echo $pd['region']; ?>" id="region" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="country">Country *</label>
                                                <input type="text" class="form-control" name="country" value="<?php echo $pd['country']; ?>" id="country" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="postal">Postal Code *</label>
                                                <input type="text" class="form-control" name="postal" value="<?php echo $pd['user_postal']; ?>" id="postal" required>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:40px;">

                            <div class="form-group text-left">                
                                <button type="submit" class="btn btn-info btn-md"  name="add" id="add">Update Profile</button>
                            </div>

                        </div>
                        <!-- end row -->
                    </form>
                </div>

            </div>

        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

