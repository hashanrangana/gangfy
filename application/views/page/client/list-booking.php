<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/client_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">My Bookings</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Listing Name</th>
                                    <th scope="col">Service Name</th>
                                    <th scope="col">Sub Service</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">From</th>
                                    <th scope="col">To</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($book)) {
                                    foreach ($book as $bdata) {
                                        $sid = $bdata['orderStatus'];
                                        $bid = $bdata['booking_Id'];
                                        $fv = $bdata['fav'];
                                        ?>
                                        <tr>

                                            <td><?php echo $bdata['listingName']; ?></td>
                                            <td><?php echo $bdata['serviceName']; ?></td>
                                            <td><?php echo $bdata['subServiceName']; ?></td>
                                            <td><?php echo $bdata['book_date']; ?></td>
                                            <td><?php echo $bdata['slot_from']; ?></td>
                                            <td><?php echo $bdata['slot_to']; ?></td>
                                            <td><?php echo $bdata['amount']; ?></td>
                                            <td><?php
                                                if ($sid == 1) {
                                                    echo "<span class=\"approve-btn\">Approved</span>";
                                                } else {
                                                    echo "<span class=\"pending-btn\">Pending</span>";
                                                }
                                                ?></td>
                                            <th scope="col"><?php if ($sid == 1 && $fv == 0) { ?>
                                                    <a href="<?php echo base_url(); ?>booking/add_favorites/<?php echo $bid; ?>" title="Add to Favorites"><i class="fa fa-star"></i></a> 
                                                <?php } else if ($sid == 1 && $fv == 1) {
                                                    ?>
                                                    <a href="<?php echo base_url(); ?>booking/remove_favorites/<?php echo $bdata['favid']; ?>" title="Remove Favorites"><i class="fa fa-star"></i></a>
                                                        <?php
                                                    }
                                                    ?></th>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<?php $this->load->view('page/temp/portal_footer'); ?>

