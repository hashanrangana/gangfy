<?php
$this->load->view('page/temp/portal_header');
$this->load->view('page/temp/portal_topbar');
$this->load->view('page/temp/header-message');
$this->load->view('page/temp/client_sidebar');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="row">
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800 gf-page-title" style="font-size: 24px !important;font-weight: 400 !important;font-family: 'Roboto', sans-serif;">Messages</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">From</th>
                                    <th scope="col">Message</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($msg)) {
                                    foreach ($msg as $md) {
                                        $st = $md['status'];
                                        ?>
                                        <tr>
                                            <td><?php echo $md['name']; ?></td>
                                            <td><?php echo substr($md['message'], 0, 10) . '...'; ?></td>
                                            <td><?php
                                                if ($st == 1) {
                                                    echo 'New';
                                                } else {
                                                    echo 'Read';
                                                }
                                                ?></td>
                                            <td><?php echo date('d F Y', strtotime($md['created_at'])); ?></td>
                                            <td><button type="button" class="btn btn-xs" data-toggle="modal" onclick="getMessage(<?php echo $md['msg_id']; ?>)" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye text-danger"></i></button></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">

                                    <h4 class="modal-title">Message</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="reason">From</label>
                                            <p id="from"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <p id="msg"></p>
                                        </div>
                                    </div> 
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label id="mtime"></label>
                                        </div>
                                    </div> 
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<script>

    function getMessage(id) {
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "dashboard/get_message",
            dataType: 'json',
            data: {"mid": id},
            success: function (res) {
                console.log(res);
                if (res)
                {
                    $("#from").append("<label>" + res.name + "</label>");
                    $("#msg").append("<label>" + res.msg + "</label>");
                    $("#mtime").append("<label> Date: " + res.mtime + "</label>");
                } else {
                    location.reload();
                }
            }
        });
    }
</script>

<?php $this->load->view('page/temp/portal_footer'); ?>

