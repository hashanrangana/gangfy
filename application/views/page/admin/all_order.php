<section class="content-header">
    <h1>
        All Order
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order</li>
    </ol>
</section>




<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <!-- small box -->
            <table  class="table table-striped table-bordered" style="width:100%" id="orders">
                <thead>
                    <tr>
                        <th scope="col">Order ID</th>
                        <th scope="col">Listing Name</th>
                        <th scope="col">Listing Service</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">From</th>
                        <th scope="col">To</th>
                        <th scope="col">Order Status</th>

                        <th scope="col">Ordered At</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    foreach ($listingData as $serviceItem) {
                        $sid = $serviceItem['orderStatus'];
                        ?>
                        <tr class="table-secondary">
                            <th scope="row">GB-<?php echo $serviceItem['booking_Id']; ?></th>
                            <td><?php echo $serviceItem['listingName']; ?></td>
                            <td><?php echo $serviceItem['serviceName']; ?></td>
                            <td><?php echo $serviceItem['name']; ?> <?php echo $serviceItem['lname']; ?></td>
                            <td><?php echo $serviceItem['slot_name']; ?></td>
                            <td><?php echo $serviceItem['slot_day']; ?></td>
                            <td><?php
                                if ($sid == 2) {
                                    echo 'Pending';
                                } elseif ($sid == 1) {
                                    echo 'Approve';
                                } else {
                                    echo 'Rejected';
                                }
                                ?></td>
                            <td><?php echo $serviceItem['create_At']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.row -->
</section>


<script>

    $(document).ready(function () {
        $('#orders').DataTable();
    });
</script>