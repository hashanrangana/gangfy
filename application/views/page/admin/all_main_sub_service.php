<section class="content-header">
    <h1>
        Add Main to Sub Service
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Main to Sub Service</li>
    </ol>
</section>




<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <!-- small box -->
            <table class="table display" id="userTable">
                <thead>
                    <tr>
                        <th scope="col">Main Service Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action </th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($allservices as $serviceItem) { ?>
                        <tr class="table-secondary">
                            <td><?php echo $serviceItem['serviceName']; ?></td>

                            <td>
                                <?php if ($serviceItem['serviceStatus'] == 1) { ?>
                                    <span class="label label-success" >Active</span>
                                <?php } else { ?>
                                    <span class="label label-danger">In-Active</span>
                                <?php } ?>
                            </td>
                            <td>
                                <a href="<?php echo base_url(); ?>admin/main_view_sub/<?php echo $serviceItem['serviceId']; ?>" title="Add / Edit">
                                    Add / Edit
                                </a>

                            </td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.row -->
</section>
