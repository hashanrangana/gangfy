<section class="content-header">
    <h1>
        Create Features
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Features</li>
    </ol>
</section>




<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <br/>    <br/>

            <div style="color:red;"><?php echo validation_errors(); ?></div>

            <form class="form-signin" role="form" action="<?php echo base_url() ?>admin/createFeatures" method="post" name="loginform">
                <label >Feature Name</label>
                <input type="text" placeholder="Feature Name" class="form-control"  required="" autofocus="" id="name" name="name" autocomplete="off"/>
                <br/>
                <label >Status</label>
                <select class="form-control" id="status" name="status">
                    <option value="1">Active</option>
                    <option value="0">In-active</option>
                </select>
                <br/><br/>
                <button class="btn btn-md btn-primary btn-block" type="submit"  name="add" id="add">Save</button>
            </form>
        </div>
    </div>
    <!-- /.row -->
</section>
