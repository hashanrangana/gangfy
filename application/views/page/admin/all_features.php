<section class="content-header">
    <h1>
        All Features
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Features</li>
    </ol>
</section>




<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <!-- small box -->
            <table class="table display" id="userTable">
                <thead>
                    <tr>
                        <th scope="col">Features Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action </th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($allfeature as $serviceItem) { ?>
                        <tr class="table-secondary">
                            <td><?php echo $serviceItem['featuresName']; ?></td>

                            <td>
                                <?php if ($serviceItem['fStatus'] == 1) { ?>
                                    <span class="label label-success" >Active</span>
                                <?php } else { ?>
                                    <span class="label label-danger">In-Active</span>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if ($serviceItem['fStatus'] == 1) { ?>
                                    <a href="<?php echo base_url(); ?>admin/featuresInactive/<?php echo $serviceItem['featuresId']; ?>" title="In-Active service">
                                        Inactive
                                    </a>
                                <?php } else { ?>
                                    <a href="<?php echo base_url(); ?>admin/featuresActive/<?php echo $serviceItem['featuresId']; ?>" title="Active service">
                                        Active
                                    </a>
                                <?php } ?>
                            </td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.row -->
</section>
