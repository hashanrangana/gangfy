<html>

    <head>
        <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets/css/full-slider.css" rel="stylesheet">


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css">  

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <link href="<?php echo base_url(); ?>assets/css/theme.css" rel="stylesheet"> 
        <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js" ></script>
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>

    </head>


    <body >




        <div class="container" style="margin-top:30px;">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">

                    <div style="color:red;"><?php echo validation_errors(); ?></div>

                    <form class="form-signin" role="form" action="<?php echo base_url() ?>admin/checkLogin" method="post" name="loginform">
                        <h1 class="h3 mb-3 font-weight-normal">Admin</h1>
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" placeholder="Your Email" class="form-control"  required="" autofocus="" id="email" name="email" autocomplete="off"/>
                        <br/>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password"  placeholder="Your Password" class="form-control"  required="" id="pwd" name="pwd" autocomplete="off"/>

                        <br/><br/>
                        <button class="btn btn-lg btn-primary btn-block" type="submit"  name="add" id="add">Login</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>



    </body >
</html>
