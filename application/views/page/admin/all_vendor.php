<section class="content-header">
    <h1>
        All Vendor
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Vendor</li>
    </ol>
</section>




<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <!-- small box -->
            <table  class="table table-striped table-bordered" style="width:100%" id="vendor">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Username</th>
                        <th scope="col">Name</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">User Status</th>
                        <th scope="col">*</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($listingData as $serviceItem) { ?>
                        <tr class="table-secondary">
                            <th scope="row"><?php echo $serviceItem['userinfo_id']; ?></th>
                            <td><?php echo $serviceItem['userinfo_username']; ?></td>
                            <td><?php echo $serviceItem['name']; ?> <?php echo $serviceItem['lname']; ?></td>
                            <td><?php echo $serviceItem['phonecode']; ?> <?php echo $serviceItem['phonenumber']; ?></td>
                            <td>
                                <?php if ($serviceItem['userStatus'] == 1) { ?>
                                    Active
                                <?php } else if ($serviceItem['userStatus'] == 2) { ?>
                                    Waiting Approval
                                <?php } else if ($serviceItem['userStatus'] == 3) { ?>
                                    Banned
                                <?php } ?>

                            </td>

                            <td>
                                <?php if ($serviceItem['userStatus'] == 2) { ?>

                                <a  href="<?php echo base_url(); ?>admin/vendorApprove/<?php echo $serviceItem['userinfo_id']; ?>" class="btn btn-success btn-sm" style="color:#fff;"> Approve</a>
                                <?php } ?>
                                <?php if ($serviceItem['userStatus'] == 1) { ?>

                                    <a  href="<?php echo base_url(); ?>admin/vendorBanned/<?php echo $serviceItem['userinfo_id']; ?>" class="btn btn-danger btn-sm" style="color:#fff;"> Banned</a>
                                <?php } ?>
                                <?php if ($serviceItem['userStatus'] == 3) { ?>

                                    <a  href="<?php echo base_url(); ?>admin/vendorunBanned/<?php echo $serviceItem['userinfo_id']; ?>" class="btn btn-warning btn-sm" style="color:#fff;"> UnBanned</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.row -->
</section>


<script>

    $(document).ready(function () {
        $('#vendor').DataTable();
    });
</script>