<section class="content-header">
    <h1>
        Main Service Add Sub
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Main Service Add Sub</li>
    </ol>
</section>




<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <br/>    <br/>
            <?php foreach ($servicesInfo as $serviceItem) { ?>

                <form enctype="multipart/form-data" method="post" action="<?php echo base_url() ?>admin/update_main_sub_service">

                    <h4>Selected Main service is  - <?php echo $serviceItem['serviceName']; ?></h4>
                    <br>
                    <input type="hidden" class="form-control" id="serviceId" name="serviceId" value="<?php echo $serviceItem['serviceId']; ?>" required >

                    <div class="form-group">
                        <label >Select Sub Services,</label>
                        <br>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php foreach ($allSubService as $servicesubItem) { ?>
                                    <input type="checkbox" name="subServices[]" id="subServices" value="<?php echo $servicesubItem['serviceId']; ?>"  <?php if ($servicesubItem['selected'] == 1) echo "checked"; ?> > <?php echo $servicesubItem['serviceName']; ?> <br>
                                <?php } ?>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" name="edit" id="edit">Save</button>
                </form>
            <?php } ?>


        </div>
    </div>
    <!-- /.row -->
</section>
