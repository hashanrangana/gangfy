<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('service_model', 'service');
        $this->load->model('feature_model', 'feature');
        $this->load->model('listing_image_model', 'list_image');
        $this->load->model('listing_model', 'listing');
        $this->load->model('listing_slot_model', 'list_slot');
        $this->load->model('booking_model', 'booking');
        $this->load->model('user_model', 'user');
        $this->load->model('user_service_category_model', 'user_service');
    }

    public function vendor() {
        if ($this->checkLoginUser()) {
            $this->load->view('page/vendor/dashboard');
        } else {
            redirect('home/login');
        }
    }

    public function becomevendor() {
        if ($this->checkLoginUser()) {
            $this->load->view('page/client/become_vendor');
        } else {
            redirect('home/login');
        }
    }

    public function create_time_slots() {
        if ($this->checkLoginUser()) {
            $uid = $this->session->userdata('user_id');
            $slot = $this->listing->get_time_slot($uid);
            $data['list'] = $this->listing->get_listing($uid);
            $st = array();
            if (!empty($data['list'])) {

                for ($i = 0; $i < count($slot); $i++) {
                    $sid = $slot[$i]['slotId'];
                    $cs = $this->listing->check_slot($sid);

                    if (!empty($cs)) {
                        $st[$i]['edit'] = 2;
                    } else {
                        $st[$i]['edit'] = 1;
                    }
                    $st[$i]['slotId'] = $slot[$i]['slotId'];
                    $st[$i]['bookable'] = $slot[$i]['bookable'];
                    $st[$i]['listingName'] = $slot[$i]['listingName'];
                    $st[$i]['slot_date'] = $slot[$i]['slot_date'];
                    $st[$i]['slot_from'] = $slot[$i]['slot_from'];
                    $st[$i]['slot_to'] = $slot[$i]['slot_to'];
                }

                $data['slot'] = $st;
                $this->load->view('page/vendor/buildtimeslots', $data);
            } else {
                $this->session->set_flashdata('err_msg', 'You dont have created services Please create a Service');
                redirect('dashboard/create_services');
            }
        } else {
            redirect('home/login');
        }
    }

    public function create_services() {
        if ($this->checkLoginUser()) {
            $uid = $this->session->userdata('user_id');
            $cb = $this->listing->get_branch($uid);
            if (count($cb) > 0) {
                $data['bran'] = $cb;
                $data['allservices'] = $this->service->all_home_service();
                $data['allfeatures'] = $this->feature->get_feature_home();
                $this->load->view('page/vendor/add_listing', $data);
            } else {
                $this->session->set_flashdata('err_msg', 'You dont have created branch Please create a branch');
                redirect('dashboard/create_branch');
            }
        } else {
            redirect('home/login');
        }
    }

    public function create_branch() {
        if ($this->checkLoginUser()) {
            $this->load->view('page/vendor/add-branch');
        } else {
            redirect('home/login');
        }
    }

    public function edit_service() {
        if ($this->checkLoginUser()) {
            $this->session->unset_userdata('elist_id');
            $lid = $this->input->post('lid');
            $this->session->set_userdata(array('elist_id' => $lid));
            $id = $this->session->userdata('elist_id');
            $eid = $this->session->userdata('ulist_id');
            $uid = $this->session->userdata('user_id');
            $data['bran'] = $this->listing->get_branch($uid);
            $data['allservices'] = $this->service->all_home_service();
            if (!empty($id)) {
                $data['list'] = $this->listing->get_service($id);
                $this->load->view('page/vendor/edit_listing', $data);
            } elseif (!empty($eid)) {
                $data['list'] = $this->listing->get_service($eid);
                $this->load->view('page/vendor/edit_listing', $data);
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('listing/list_services');
            }
        } else {
            redirect('home/login');
        }
    }

    public function list_features() {
        if ($this->checkLoginUser()) {
            $id = $this->uri->segment(3);
            $this->session->unset_userdata('ulist_id');
            if (!empty($id)) {
                $this->session->set_userdata(array('ulist_id' => $id));
                $data['feature'] = $this->listing->list_features($id);
                $data['allfeatures'] = $this->feature->get_feature_home();
                $this->load->view('page/vendor/list-features', $data);
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/edit_service');
            }
        } else {
            redirect('home/login');
        }
    }

    public function list_branches() {
        if ($this->checkLoginUser()) {
            $uid = $this->session->userdata('user_id');
            $data['bran'] = $this->listing->list_branches($uid);
            $this->load->view('page/vendor/list-branches', $data);
        } else {
            redirect('home/login');
        }
    }

    public function edit_branch() {
        if ($this->checkLoginUser()) {
            $id = $this->uri->segment(3);
            $data['bran'] = $this->listing->branch_detail($id);
            $this->load->view('page/vendor/edit-branch', $data);
        } else {
            redirect('home/login');
        }
    }

    public function delete_slot() {
        $id = $this->uri->segment(3);
        $ext = $this->booking->slot_exist($id);
        $sd = $this->booking->slot_data($id);
        $day = $sd[0]['slot_day'];
        $this->session->unset_userdata('day');
        $this->session->set_userdata(array('day' => $day));

        if (count($ext) == 0) {
            $this->booking->delete_slot($id);
            $this->session->set_flashdata('succ_msg', 'You Have Successfully Delete the slot');
            redirect('dashboard/create_time_slots');
        } else {
            $this->session->set_flashdata('err_msg', 'You cannot delete the slot someone made the booking for this slot');
            redirect('dashboard/create_time_slots');
        }
    }

    public function open_slot() {
        if ($this->session->userdata('user_id')) {
            $id = $this->uri->segment(3);
            if (!empty($id)) {
                $sd = $this->booking->slot_day($id);
                $day = $sd[0]['slot_day'];
                $this->session->unset_userdata('day');
                $this->session->set_userdata(array('day' => $day));
                $this->booking->open_slot($id);
                redirect('dashboard/create_time_slots');
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/create_time_slots');
            }
        } else {
            redirect('home/login');
        }
    }

    public function close_slot() {
        if ($this->session->userdata('user_id')) {
            $id = $this->uri->segment(3);
            if (!empty($id)) {
                $sd = $this->booking->slot_day($id);
                $day = $sd[0]['slot_day'];
                $this->session->unset_userdata('day');
                $this->session->set_userdata(array('day' => $day));
                $this->booking->close_slot($id);
                redirect('dashboard/create_time_slots');
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/create_time_slots');
            }
        } else {
            redirect('home/login');
        }
    }

    public function get_service() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $data = $this->listing->get_listing($uid);
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($data));
        } else {
            redirect('home/login');
        }
    }

    public function get_services() {
        $bid = $this->input->post('bid');
        $data = $this->listing->get_services($bid);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    public function ban_customer() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('cid', 'Customer Id', 'trim|required');
            $this->form_validation->set_rules('reason', 'Reason', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('booking/list_customer');
            } else {
                $uid = $this->session->userdata('user_id');
                $cid = $this->input->post('cid');
                $reason = $this->input->post('reason');
                $desc = $this->input->post('description');

                $ext = $this->booking->ban_exist($uid, $cid);
                if (count($ext) == 0) {
                    $this->booking->ban_customer($uid, $cid, $reason, $desc);
                    $this->session->set_flashdata('succ_msg', 'This customer banned and he cant make your booking anymore');
                    redirect('booking/list_customer');
                } else {
                    $this->session->set_flashdata('err_msg', 'This customer Already Banned');
                    redirect('booking/list_customer');
                }
            }
        } else {
            redirect('home/login');
        }
    }

    public function customer_detail() {
        if ($this->session->userdata('user_id')) {
            $id = $this->uri->segment(3);
            $data['prof'] = $this->user->user_details($id);
            $this->load->view('page/vendor/customer-detail', $data);
        } else {
            redirect('home/login');
        }
    }

    public function my_bookings() {
        if ($this->checkLoginUser()) {

            $main['upcomingData'] = $this->booking->all_booking_by_user_upcoming($this->session->userdata('user_id'));
            $main['allData'] = $this->booking->all_booking_by_user_old_booking($this->session->userdata('user_id'));

            $main['mainview'] = 'page/site/my_bookings';
            $this->load->view('page/home', $main);
        } else {
            redirect('/');
        }
    }

    public function coupon_generater() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $data['list'] = $this->listing->get_listing($uid);
            $data['cup'] = $this->listing->get_coupon($uid);
            $data['cus'] = $this->booking->all_customer($uid);
            $this->load->view('page/vendor/coupon-generater', $data);
        } else {
            redirect('home/login');
        }
    }

    public function create_coupon() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('service', 'Service', 'trim|required');
            $this->form_validation->set_rules('cname', 'Coupon Name', 'trim|required');
            $this->form_validation->set_rules('ccode', 'Coupon Code', 'trim|required');
            $this->form_validation->set_rules('ctype', 'Coupon Type', 'trim|required');
            $this->form_validation->set_rules('cquota', 'Coupon Quota', 'trim|required');
            $this->form_validation->set_rules('cdesc', 'Coupon Desc', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/coupon_generater');
            } else {
                $uid = $this->session->userdata('user_id');
                $sid = $this->input->post('service');
                $cname = $this->input->post('cname');
                $ccode = $this->input->post('ccode');
                $ctype = $this->input->post('ctype');
                $cquota = $this->input->post('cquota');
                $cdesc = $this->input->post('cdesc');

                $ext = $this->listing->exist_coupon($sid, $cname);

                if (count($ext) == 0) {
                    $this->listing->save_coupon($sid, $cname, $ccode, $ctype, $cquota, $cdesc, $uid);
                    $this->session->set_flashdata('succ_msg', 'You have successfully create a coupon');
                    redirect('dashboard/coupon_generater');
                } else {
                    $this->session->set_flashdata('err_msg', 'This coupon already exist');
                    redirect('dashboard/coupon_generater');
                }
            }
        } else {
            redirect('home/login');
        }
    }

    public function gen_coupon() {
        $sid = $this->input->post('sname');
        $cname = $this->input->post('cname');
        $uid = $this->session->userdata('user_id');
        $rand = rand(1000, 10000);
        $cnum = $uid . $sid . substr($cname, 0, 1) . $rand;

        $data = array('cnum' => $cnum);

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    public function active_coupon() {
        if ($this->session->userdata('user_id')) {
            $id = $this->uri->segment(3);
            if (!empty($id)) {
                $this->listing->active_coupon($id);
                redirect('dashboard/coupon_generater');
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/coupon_generater');
            }
        } else {
            redirect('home/login');
        }
    }

    public function deactivate_coupon() {
        if ($this->session->userdata('user_id')) {
            $id = $this->uri->segment(3);
            if (!empty($id)) {
                $this->listing->deactivate_coupon($id);
                redirect('dashboard/coupon_generater');
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/coupon_generater');
            }
        } else {
            redirect('home/login');
        }
    }

    public function delete_coupon() {
        $id = $this->uri->segment(3);

        $coup = $this->booking->get_coupon($id);
        $ccode = $coup[0]['coupon_code'];
        $ext = $this->listing->check_coupon($ccode);

        if (count($ext) == 0) {
            $this->listing->delete_coupon($id);
            $this->session->set_flashdata('succ_msg', 'You Have Successfully Delete the coupon');
            redirect('dashboard/coupon_generater');
        } else {
            $this->session->set_flashdata('err_msg', 'You cannot delete the coupon someone made the booking for coupon');
            redirect('dashboard/coupon_generater');
        }
    }

    public function validate_coupon() {
        $sid = $this->input->post('sid');
        $ccode = $this->input->post('ccode');

        $ecode = explode('C', $ccode);
        $code = $ecode[0];
        $cd = $this->booking->coupon_code($sid, $code);

        if (count($cd) > 0) {
            $data = array('st' => 'valid', 'ct' => $cd[0]['coupon_type'], 'cv' => $cd[0]['quota']);
        } else {
            $data = array('st' => 'invalid');
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    public function share_coupon() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('cid', 'Coupon Id', 'trim|required');
            $this->form_validation->set_rules('cname', 'Custome Name', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/coupon_generater');
            } else {
                $uid = $this->session->userdata('user_id');
                $cid = $this->input->post('cid');
                $cname = $this->input->post('cname');

                $ud = $this->user->user_data($cname);
                $to = $ud[0]['userinfo_username'];
                $cd = $this->booking->get_coupon($cid);
                $cdes = $cd[0]['coupon_desc'];
                $cdtl = $cd[0]['coupon_code'] . 'C' . $cname . ' - (' . $cd[0]['quota'] . $cd[0]['coupon_type'] . ' Off)';

                $this->booking->share_coupon($cid, $cname, $uid);
                $this->coupon_share_email($to, $cdes, $cdtl);
                $this->session->set_flashdata('succ_msg', 'You have successfully Share the coupon');
                redirect('dashboard/coupon_generater');
            }
        } else {
            redirect('home/login');
        }
    }

    public function shared_coupon() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $cshr = $this->booking->shared_coupon($uid);
            $cdata = array();
            if (!empty($cshr)) {
                for ($i = 0; $i < count($cshr); $i++) {
                    $ccode = $cshr[$i]['coupon_code'];
                    $cid = $cshr[$i]['customer_id'];
                    $cbook = $this->booking->coupon_book($ccode . 'C' . $cid);
                    $tbook = $cbook[0]['tbook'];
                    $cdata[$i]['list_name'] = $cshr[$i]['listingName'];
                    $cdata[$i]['name'] = $cshr[$i]['name'];
                    $cdata[$i]['coupon_name'] = $cshr[$i]['coupon_name'];
                    $cdata[$i]['coupon_code'] = $cshr[$i]['coupon_code'];
                    $cdata[$i]['coupon_quota'] = $cshr[$i]['quota'] . $cshr[$i]['coupon_type'];
                    $cdata[$i]['coupon_share'] = $tbook;
                }
            }
            $data['csr'] = $cdata;
            $this->load->view('page/vendor/shared-coupon', $data);
        } else {
            redirect('home/login');
        }
    }

    public function list_coupon() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $data['cup'] = $this->booking->list_coupon($uid);
            $this->load->view('page/client/list-coupon', $data);
        } else {
            redirect('home/login');
        }
    }

    public function send_feedback() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $feed = $this->input->post('fdback');
            $this->booking->save_feedback($feed, $uid);
            echo 'true';
        } else {
            redirect('home/login');
        }
    }

    public function customize_message() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('cusid', 'Customer Id', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('booking/booking_request');
            } else {
                $uid = $this->session->userdata('user_id');
                $cid = $this->input->post('cusid');
                $desc = $this->input->post('message');

                $this->booking->save_message($desc, $cid, $uid);
                $this->session->set_flashdata('succ_msg', 'Message send to customer');
                redirect('booking/booking_request');
            }
        } else {
            redirect('home/login');
        }
    }

    public function send_message() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('cusid', 'Customer Id', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('booking/list_customer');
            } else {
                $uid = $this->session->userdata('user_id');
                $cid = $this->input->post('cusid');
                $desc = $this->input->post('message');

                $this->booking->save_message($desc, $cid, $uid);
                $this->session->set_flashdata('succ_msg', 'Message send to customer');
                redirect('booking/list_customer');
            }
        } else {
            redirect('home/login');
        }
    }

    public function booking_message() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('cusid', 'Customer Id', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('booking/list_booking');
            } else {
                $uid = $this->session->userdata('user_id');
                $cid = $this->input->post('cusid');
                $desc = $this->input->post('message');

                $this->booking->save_message($desc, $cid, $uid);
                $this->session->set_flashdata('succ_msg', 'Message send to customer');
                redirect('booking/list_booking');
            }
        } else {
            redirect('home/login');
        }
    }

    public function list_messages() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $data['msg'] = $this->booking->get_messages($uid);
            $this->load->view('page/client/list-message', $data);
        } else {
            redirect('home/login');
        }
    }

    public function get_message() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $mid = $this->input->post('mid');
            $msg = $this->booking->get_message($mid, $uid);
            $time = $msg[0]['created_at'];
            $this->booking->read_message($mid, $time);
            $data = array('name' => $msg[0]['name'], 'msg' => $msg[0]['message'], 'mtime' => date('d F Y', strtotime($msg[0]['created_at'])));
            $this->session->unset_userdata('msg');
            $urmsg = $this->booking->new_messages($uid);
            if (!empty($msg)) {
                $this->session->set_userdata(array('msg' => $urmsg));
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($data));
        } else {
            redirect('home/login');
        }
    }

    public function send_messages() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $data['msg'] = $this->booking->all_messages($uid);
            $this->load->view('page/vendor/all-message', $data);
        } else {
            redirect('home/login');
        }
    }

    public function booking_note() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $bid = $this->input->post('bid');
            $note = $this->input->post('bnote');

            $cnote = $this->booking->check_notes($bid, $uid);
            if (count($cnote) == 0) {
                $this->booking->save_note($bid, $note, $uid);
                echo 'Add';
            } else {
                $nid = $cnote[0]['bn_id'];
                $this->booking->update_note($nid, $note);
                echo 'Update';
            }
        } else {
            redirect('home/login');
        }
    }

    public function customer_note() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $cid = $this->input->post('cid');
            $note = $this->input->post('cnote');

            $cnote = $this->booking->customer_notes($cid, $uid);
            if (count($cnote) == 0) {
                $this->booking->save_customer_note($cid, $note, $uid);
                echo 'Add';
            } else {
                $nid = $cnote[0]['cn_id'];
                $this->booking->update_customer_note($nid, $note);
                echo 'Update';
            }
        } else {
            redirect('home/login');
        }
    }

    public function vendor_bookings() {
        if ($this->checkLoginUser()) {

            $main['upcomingData'] = $this->booking->all_booking_by_vendor_upcoming($this->session->userdata('user_id'));
            $main['allData'] = $this->booking->all_booking_by_vendor_old_booking($this->session->userdata('user_id'));
            $main['mainview'] = 'page/site/vendor_bookings';
            $this->load->view('page/home', $main);
        } else {
            redirect('/');
        }
    }

    public function host_settings() {
        if ($this->checkLoginUser()) {

            if (isset($_POST['update'])) {
                $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
                $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
                $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
                $this->form_validation->set_rules('profile-host-city', 'City', 'trim|required');
                $this->form_validation->set_rules('profileHostRegion', 'Region', 'trim|required');
                $this->form_validation->set_rules('profileHostCountry', 'Country', 'trim|required');
                if ($this->form_validation->run() && $this->session->userdata('user_id')) {
                    $data = array(
                        'name' => $this->input->post('firstname'),
                        'lname' => $this->input->post('lastname'),
                        'phonenumber' => $this->input->post('phone'),
                        'mobileNumber' => $this->input->post('mobile'),
                        'contactEmail' => $this->input->post('email'),
                        'address1' => $this->input->post('address1'),
                        'address2' => $this->input->post('address2'),
                        'city' => $this->input->post('profile-host-city'),
                        'region' => $this->input->post('profileHostRegion'),
                        'country' => $this->input->post('profileHostCountry'),
                    );

                    $this->user->update_user_host_profile($data, $this->session->userdata('user_id'));

                    $main['successprofile'] = 'success';
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/host_settings';
                    $this->load->view('page/home', $main);
                } else {
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/host_settings';
                    $this->load->view('page/home', $main);
                }
            } else if (isset($_POST['reset'])) {
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('repassword', 'Confirme Password', 'trim|required|matches[password]');

                if ($this->form_validation->run() && $this->session->userdata('user_id')) {
                    $data = array(
                        'pwd' => md5($this->input->post('password')),
                        'userId' => $this->session->userdata('user_id'),
                    );
                    $main['userDate'] = $this->user->updatePassword($data);

                    $main['successReset'] = 'success';
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/host_settings';
                    $this->load->view('page/home', $main);
                } else {
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/host_settings';
                    $this->load->view('page/home', $main);
                }
            } else {

                $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                $main['mainview'] = 'page/site/host_settings';
                $this->load->view('page/home', $main);
            }
        } else {
            redirect('/');
        }
    }

    public function vendor_settings() {
        if ($this->checkLoginUser()) {

            if (isset($_POST['update'])) {
                $this->form_validation->set_rules('catergory', 'Catergory', 'trim|required');
                $this->form_validation->set_rules('companyname', 'Company Name', 'trim|required');
                $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
                $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
                $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
                $this->form_validation->set_rules('profile-city', 'City', 'trim|required');
                $this->form_validation->set_rules('profileRegion', 'Region', 'trim|required');
                $this->form_validation->set_rules('profileCountry', 'Country', 'trim|required');

                if ($this->form_validation->run() && $this->session->userdata('user_id')) {
                    $data = array(
                        'userCatergory' => $this->input->post('catergory'),
                        'companyName' => $this->input->post('companyname'),
                        'name' => $this->input->post('firstname'),
                        'lname' => $this->input->post('lastname'),
                        'phonenumber' => $this->input->post('phone'),
                        'mobileNumber' => $this->input->post('mobile'),
                        'contactEmail' => $this->input->post('email'),
                        'address1' => $this->input->post('address1'),
                        'address2' => $this->input->post('address2'),
                        'city' => $this->input->post('profile-city'),
                        'region' => $this->input->post('profileRegion'),
                        'country' => $this->input->post('profileCountry'),
                    );

                    $this->user->update_user_vendor_profile($data, $this->session->userdata('user_id'));

                    $main['successprofile'] = 'success';
                    $main['services'] = $this->user_service->get_all_user_service();
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/vendor_settings';
                    $this->load->view('page/home', $main);
                } else {
                    $main['services'] = $this->user_service->get_all_user_service();
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/vendor_settings';
                    $this->load->view('page/home', $main);
                }
            } else if (isset($_POST['reset'])) {
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('repassword', 'Confirme Password', 'trim|required|matches[password]');

                if ($this->form_validation->run() && $this->session->userdata('user_id')) {
                    $data = array(
                        'pwd' => md5($this->input->post('password')),
                        'userId' => $this->session->userdata('user_id'),
                    );
                    $main['userDate'] = $this->user->updatePassword($data);

                    $main['successReset'] = 'success';
                    $main['services'] = $this->user_service->get_all_user_service();
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/vendor_settings';
                    $this->load->view('page/home', $main);
                } else {
                    $main['services'] = $this->user_service->get_all_user_service();
                    $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                    $main['mainview'] = 'page/site/vendor_settings';
                    $this->load->view('page/home', $main);
                }
            } else {

                $main['services'] = $this->user_service->get_all_user_service();
                $main['userDate'] = $this->user->user_id($this->session->userdata('user_id'));

                $main['mainview'] = 'page/site/vendor_settings';
                $this->load->view('page/home', $main);
            }
        } else {
            redirect('/');
        }
    }

    public function vendor_dashbaord() {
        if ($this->checkLoginUser()) {

            $main['totalOrders'] = count($this->booking->all_orders_by_user($this->session->userdata('user_id')));
            $main['totalPending'] = count($this->booking->all_pending_by_user($this->session->userdata('user_id')));
            $main['totalconfirm'] = count($this->booking->all_confirm_by_user($this->session->userdata('user_id')));
            $main['latestListing'] = $this->booking->latest_listing($this->session->userdata('user_id'));
            $main['latestOrders'] = $this->booking->latest_orders($this->session->userdata('user_id'));
            $main['mainview'] = 'page/site/dashboard';
            $this->load->view('page/home', $main);
        } else {
            redirect('/');
        }
    }

    public function host_dashboard() {
        if ($this->checkLoginUser()) {

            $main['listingData'] = $this->booking->all_booking_by_user($this->session->userdata('user_id'));

            $main['mainview'] = 'page/site/host_dashboard';

            $this->load->view('page/home', $main);
        } else {
            redirect('/');
        }
    }

    public function checkLoginUser() {
        $sess_id = $this->session->userdata('user_id');

        if (!empty($sess_id)) {
            return true;
        } else {
            return false;
        }
    }

    function coupon_share_email($to, $desc, $cdetail) {
        $this->load->library('email');

        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('GangFY Coupon Deal');

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>Here’s the Deal: Coupon is Ready.</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Hooray !... Your Coupon is Ready and Share with Your Gang</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>$desc</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='#' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				$cdetail
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
