<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('service_model', 'service');
        $this->load->model('feature_model', 'feature');
        $this->load->model('listing_image_model', 'list_image');
        $this->load->model('listing_model', 'listing');
        $this->load->model('listing_slot_model', 'list_slot');
        $this->load->model('booking_model', 'booking');
    }

    public function search() {
        $this->session->unset_userdata('slot_data');
        $this->session->unset_userdata('book_date');
        $this->session->unset_userdata('book_msg');
        $this->session->unset_userdata('price');
        $this->session->unset_userdata('sid');

        $sid = $this->input->post('sid');
        $sec = $this->input->post('sect');
        $list = $this->booking->select_listing($sid);
        $lid = $list[0]['listingId'];
        $bid = $list[0]['branch_id'];
        $price = $list[0]['price'];
        $lname = str_replace(" ", "-", $list[0]['listingName']);
        $bra = $this->listing->branch_detail($bid);
        $bname = str_replace(" ", "-", $bra[0]['branch_name']);

        $this->form_validation->set_rules('sid', 'Service Id', 'trim|required');
        $this->form_validation->set_rules('bookdate', 'Book Date', 'trim|required');
        $this->form_validation->set_rules('sect', 'Section', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $sview = "";
            $sview .= "<div class='alert alert-warning' role='alert>";
            $sview .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
            $sview .= "<span aria-hidden='true'></span>";
            $sview .= "</button>";
            $sview .= "Sorry! Please select a date";
            $sview .="</div>";
            $slotdata = array(
                'slot_data' => $sview
            );
            $this->session->set_userdata($slotdata);
            if ($sec == 'branch') {
                redirect('listing/branch/' . $bid . '/' . $bname);
            } elseif ($sec == 'service') {
                redirect('listing/all_services/' . $bid . '/' . $bname);
            } else {
                redirect('home/details/' . $lid . '/' . $lname);
            }
        } else {
            $date = $this->input->post('bookdate');
            $bdate = strtotime($date);
            $price = $list[0]['price'];
            $base_url = base_url() . 'booking/booking';
            $lsot = $this->booking->select_slot($date, $sid);
            $sval = '';
            if ($sec == 'branch') {
                $sval = 'bra';
            } elseif ($sec == 'service') {
                $sval = 'srv';
            } else {
                $sval = 'sng';
            }
            $sview = "";
            if (!empty($lsot)) {
                $sview.="<div>";
                $sview.= "<form action='$base_url' method='post' name='listForm'>";
                $sview.= "<div class='form-group'><input type='text' name='ccode' id='ccode' placeholder='Enter Coupon Code' class='form-control col-md-12'></div>";
                $sview.= "<div class='form-group' id='cval'></div>";
                $sview.= "<input type='hidden' name='sid' id='sid' value='$lid'>";
                $sview.= "<input type='hidden' name='ct' id='ct'>";
                $sview.= "<input type='hidden' name='cv' id='cv'>";
                $sview.= "<input type='hidden' name='sect' id='sect' value='$sval'>";
                $sview.= "<table class='table table-bordered table-striped'><thead><tr><th scope='col'>From</th><th scope='col'>To</th><th scope='col'></th></tr></thead><tbody>";
                foreach ($lsot as $sl) {
                    $slid = $sl['slotId'];
                    $from = $sl['slot_from'];
                    $to = $sl['slot_to'];
                    $hourdiff = $this->calculate_hours($to, $from);
                    $sview.= "<input type='hidden' name='slotid[]' id='slotid' value='$slid'>";
                    $sview.= "<tr><td>$from</td><td>$to</td><td align='center'><input name='tslot[]' id='tslot' type='checkbox' onchange='checkTotal()' value='$hourdiff'></td></tr>";
                }
                $sview.= "</tbody></table>";
                $sview.= " <div class='gbc-item'>";
                $sview.= "<label style='margin-bottom:10px;'> Hourly Rate $ <span id='idAmount'>$price + TAX</span></label>";
                $sview.= "<div class='gbc-rates'><h5>Amount Payable</h5>";
                $sview.= "<h2> $ <input type='text' id='total' size='2' name='total[]' value='0' style='font-weight: 900;border: 0px;background-color: #f1f1f1;'/>+ TAX</h2>";
                $sview.= "</div></div>";
                $sview.= "<div class='gbc-item'><input type='submit' class='btn gbc-btn' onclick='bookNow()' value='Book Slot' id='book-listing'></div>";
                $sview.= "</form>";
                $sview.="</div>";
                $slotdata = array(
                    'slot_data' => $sview,
                    'book_date' => $bdate,
                    'price' => $price,
                    'sid' => $sid
                );
                $this->session->set_userdata($slotdata);
                if ($sec == 'branch') {
                    redirect('listing/branch/' . $bid . '/' . $bname);
                } elseif ($sec == 'service') {
                    redirect('listing/all_services/' . $bid . '/' . $bname);
                } else {
                    redirect('home/details/' . $lid . '/' . $lname);
                }
            } else {
                $sview .= "<div class='alert alert-warning' role='alert>";
                $sview .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
                $sview .= "<span aria-hidden='true'></span>";
                $sview .= "</button>";
                $sview .= "Sorry! No Availability for This Date";
                $sview .="</div>";
                $slotdata = array(
                    'book_date' => $bdate,
                    'slot_data' => $sview,
                    'sid' => $sid
                );
                $this->session->set_userdata($slotdata);
                if ($sec == 'branch') {
                    redirect('listing/branch/' . $bid . '/' . $bname);
                } elseif ($sec == 'service') {
                    redirect('listing/all_services/' . $bid . '/' . $bname);
                } else {
                    redirect('home/details/' . $lid . '/' . $lname);
                }
            }
        }
    }

    public function booking() {

        $id = $this->input->post('sid');
        $this->session->unset_userdata('book_msg');
        $msg = "";
        if (!$this->session->userdata('user_id')) {

            $book_data = array(
                'slot_id' => $id
            );
            $this->session->set_userdata($book_data);
            redirect('home/login');
        } else {
            $this->form_validation->set_rules('sid', 'List Id', 'trim|required');
            $this->form_validation->set_rules('slotid[]', 'Slot Id', 'trim|required');
            $this->form_validation->set_rules('tslot[]', 'Slot Checked', 'trim|required');
            $this->form_validation->set_rules('sect', 'Section', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('/');
            } else {
                $sid = $this->input->post('sid');
                $slotid = $this->input->post('slotid');
                $tslot = $this->input->post('tslot');
                $ccode = $this->input->post('ccode');
                $sec = $this->input->post('sect');

                $ct = '';
                $cv = 0;
                $cup = '';
                if (!empty($ccode)) {
                    $ecode = explode('C', $ccode);
                    $code = $ecode[0];
                    $cd = $this->booking->coupon_code($sid, $code);
                    $ct = $cd[0]['coupon_type'];
                    $cv = $cd[0]['quota'];
                    $cup = $ccode;
                } else {
                    $cup = '';
                }

                $slot = $this->booking->list_data($sid);
                $lid = $slot[0]['listingId'];
                $bid = $slot[0]['branch_id'];
                $lname = str_replace(" ", "-", $slot[0]['listingName']);
                $bra = $this->listing->branch_detail($bid);
                $bname = str_replace(" ", "-", $bra[0]['branch_name']);
                $ldata = $this->booking->select_listing($lid);
                $vemail = $ldata[0]['userinfo_username'];

                if (!empty($sid)) {
                    $uid = $this->session->userdata('user_id');
                    $eto = $this->session->userdata('user_name');
                    $date = date('Y-m-d', $this->session->userdata('book_date'));

                    if (!empty($tslot) && !empty($slotid)) {
                        $price = $slot[0]['price'];
                        $cfl = count($tslot);
                        for ($i = 0; $i < $cfl; $i++) {
                            $lslot = $this->booking->get_slot($slotid[$i]);

                            $slid = $lslot[0]['slotId'];
                            $from = $lslot[0]['slot_from'];
                            $to = $lslot[0]['slot_to'];
                            $hour = $this->calculate_hours($to, $from);

                            if (!empty($ct) && !empty($cv)) {
                                $total = ($hour * $price);
                                if ($ct == '%') {
                                    $coup = ($total * ($cv / 100));
                                    $cda = ($total - $coup);
                                    $bkid = $this->booking->save_booking($slid, $date, $cup, $uid, $cda);
                                } else {
                                    $bkid = $this->booking->save_booking($slid, $date, $cup, $uid, ($total - $cv));
                                }
                            } else {
                                $bkid = $this->booking->save_booking($slid, $date, $cup, $uid, ($hour * $price));
                            }
                        }
                        $bt = 'booking';
                        $this->booking_email($vemail, $bkid, $bt);
                        $this->service_reservation($eto);
                        $msg .= "<div class='alert alert-success' role='alert>";
                        $msg .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
                        $msg .= "<span aria-hidden='true'></span>";
                        $msg .= "</button>";
                        $msg .= "Your booking request has been sent to the vendor. Please wait for their response.";
                        $msg .="</div>";
                        $bookdata = array(
                            'book_msg' => $msg
                        );
                        $this->session->set_userdata($bookdata);
                        $this->session->unset_userdata('slot_data');
                        if ($sec == 'bra') {
                            redirect('listing/branch/' . $bid . '/' . $bname);
                        } elseif ($sec == 'srv') {
                            redirect('listing/all_services/' . $bid . '/' . $bname);
                        } else {
                            redirect('home/details/' . $lid . '/' . $lname);
                        }
                    } else {
                        $msg .= "<div class='alert alert-warning' role='alert>";
                        $msg .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
                        $msg .= "<span aria-hidden='true'></span>";
                        $msg .= "</button>";
                        $msg .= "Sorry! Somthing Went Wrong Please Try Later";
                        $msg .="</div>";
                        $bookdata = array(
                            'book_msg' => $msg
                        );
                        $this->session->set_userdata($bookdata);
                        if ($sec == 'bra') {
                            redirect('listing/branch/' . $bid . '/' . $bname);
                        } elseif ($sec == 'srv') {
                            redirect('listing/all_services/' . $bid . '/' . $bname);
                        } else {
                            redirect('home/details/' . $lid . '/' . $lname);
                        }
                    }
                } else {
                    $msg .= "<div class='alert alert-warning' role='alert>";
                    $msg .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
                    $msg .= "<span aria-hidden='true'></span>";
                    $msg .= "</button>";
                    $msg .= "Sorry! Somthing Went Wrong Please Try Later";
                    $msg .="</div>";
                    $bookdata = array(
                        'book_msg' => $msg
                    );
                    $this->session->set_userdata($bookdata);
                    if ($sec == 'bra') {
                        redirect('listing/branch/' . $bid . '/' . $bname);
                    } elseif ($sec == 'srv') {
                        redirect('listing/all_services/' . $bid . '/' . $bname);
                    } else {
                        redirect('home/details/' . $lid . '/' . $lname);
                    }
                }
            }
        }
    }

    public function list_booking() {
        if ($this->session->userdata('user_id')) {
            $id = $this->session->userdata('user_id');
            $book = $this->booking->list_booking($id);

            $bd = array();
            for ($i = 0; $i < count($book); $i++) {
                $bid = $book[$i]['booking_Id'];
                $note = $this->booking->check_notes($bid, $id);
                if (!empty($note)) {
                    $bd[$i]['note'] = $note[0]['note'];
                } else {
                    $bd[$i]['note'] = '';
                }
                $bd[$i]['booking_Id'] = $bid;
                $bd[$i]['userinfo_id'] = $book[$i]['userinfo_id'];
                $bd[$i]['name'] = $book[$i]['name'];
                $bd[$i]['listingName'] = $book[$i]['listingName'];
                $bd[$i]['book_date'] = $book[$i]['book_date'];
                $bd[$i]['slot_from'] = $book[$i]['slot_from'];
                $bd[$i]['slot_to'] = $book[$i]['slot_to'];
                $bd[$i]['amount'] = $book[$i]['amount'];
                $bd[$i]['coupon_code'] = $book[$i]['coupon_code'];
                $bd[$i]['orderStatus'] = $book[$i]['orderStatus'];
            }

            $data['book'] = $bd;
            $this->load->view('page/vendor/list-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function approve_booking() {
        $bid = $this->uri->segment(3);
        if ($this->session->userdata('user_id')) {
            $ui = $this->session->userdata('user_id');
            $this->booking->approve_booking($bid);
            $bd = $this->booking->booking_details($bid);
            $to = $bd[0]['userinfo_username'];

            $book = $this->booking->new_booking($ui);
            $cbook = $this->session->userdata('nofbook');
            $cmb = $this->session->userdata('cbook');
            if (count($book) != 0 && empty($cmb)) {
                $this->session->set_userdata(array('nofbook' => count($book)));
            } elseif (count($book) != 0 && !empty($cmb)) {
                $this->session->set_userdata(array('nofbook' => (count($cbook) - 1)));
            } else {
                $this->session->unset_userdata('nofbook');
            }
            $this->booking_approve($to);
            $this->session->set_flashdata('succ_msg', 'You Have Successfully Approved Booking');
            redirect('booking/list_booking');
        } else {
            redirect('home/login');
        }
    }

    public function booking_reject() {
        $bid = $this->uri->segment(3);
        if ($this->session->userdata('user_id')) {
            $ui = $this->session->userdata('user_id');
            $this->booking->booking_reject($bid);
            $bd = $this->booking->booking_details($bid);
            $to = $bd[0]['userinfo_username'];

            $book = $this->booking->new_booking($ui);
            $cbook = $this->session->userdata('nofbook');
            $cmb = $this->session->userdata('cbook');
            if (count($book) != 0 && empty($cmb)) {
                $this->session->set_userdata(array('nofbook' => count($book)));
            } elseif (count($book) != 0 && !empty($cmb)) {
                $this->session->set_userdata(array('nofbook' => (count($cbook) - 1)));
            } else {
                $this->session->unset_userdata('nofbook');
            }

            $this->reject_email($to);
            $this->session->set_flashdata('war_msg', 'You Rejected The Booking');
            redirect('booking/list_booking');
        } else {
            redirect('home/login');
        }
    }

    public function my_booking() {
        if ($this->session->userdata('user_id')) {
            $id = $this->session->userdata('user_id');
            $book = $this->booking->customer_booking($id);
            $sb = array();

            for ($i = 0; $i < count($book); $i++) {
                $lid = $book[$i]['listingId'];
                $fv = $this->booking->get_favourites($lid, $id);
                if (!empty($fv)) {
                    $sb[$i]['fav'] = 1;
                    $sb[$i]['favid'] = $fv[0]['fav_id'];
                } else {
                    $sb[$i]['fav'] = 0;
                }
                $sb[$i]['booking_Id'] = $book[$i]['booking_Id'];
                $sb[$i]['listingName'] = $book[$i]['listingName'];
                $sb[$i]['serviceName'] = $book[$i]['serviceName'];
                $sb[$i]['subServiceName'] = $book[$i]['subServiceName'];
                $sb[$i]['book_date'] = $book[$i]['book_date'];
                $sb[$i]['slot_from'] = $book[$i]['slot_from'];
                $sb[$i]['slot_to'] = $book[$i]['slot_to'];
                $sb[$i]['amount'] = $book[$i]['amount'];
                $sb[$i]['orderStatus'] = $book[$i]['orderStatus'];
            }

            $data['book'] = $sb;
            $this->load->view('page/client/list-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function banned_customers() {
        if ($this->session->userdata('user_id')) {
            $id = $this->session->userdata('user_id');
            $data['cus'] = $this->booking->ban_list($id);
            $this->load->view('page/vendor/ban-list', $data);
        } else {
            redirect('home/login');
        }
    }

    function calculate_hours($to, $from) {
        $hourdiff = round((strtotime($to) - strtotime($from)) / 3600, 1);
        return $hourdiff;
    }

    public function customize_booking() {
        if ($this->session->userdata('user_id')) {
            $id = $this->input->post('bid');
            $lid = $this->input->post('lid');
            $sec = $this->input->post('sec');
            $this->session->unset_userdata('book_msg');
            $this->form_validation->set_rules('bid', 'Branch Id', 'trim|required');
            $this->form_validation->set_rules('sec', 'Section', 'trim|required');
            $this->form_validation->set_rules('cdate', 'Date', 'trim|required');
            $this->form_validation->set_rules('service[]', 'Service', 'trim|required');
            $this->form_validation->set_rules('sfrom[]', 'From', 'trim|required');
            $this->form_validation->set_rules('sto[]', 'To', 'trim|required');
            $this->form_validation->set_rules('duration[]', 'Duration', 'trim|required');
            $bra = $this->booking->select_branch($id);
            $bname = str_replace(" ", "-", $bra[0]['branch_name']);
            $vid = $bra[0]['user_id'];

            if (!empty($lid)) {
                $list = $this->booking->select_listing($lid);
                $lid = $list[0]['listingId'];
                $vid = $list[0]['vendorId'];
                $lname = str_replace(" ", "-", $list[0]['listingName']);
            }

            $msg = "";
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                $msg .= "<div class='alert alert-warning' role='alert>";
                $msg .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
                $msg .= "<span aria-hidden='true'></span>";
                $msg .= "</button>";
                $msg .= "Sorry! Please fill all required fields";
                $msg .="</div>";
                $bookdata = array(
                    'book_msg' => $msg
                );
                $this->session->set_userdata($bookdata);
                if ($sec == 'branch') {
                    redirect('listing/branch/' . $id . '/' . $bname);
                } elseif ($sec == 'service') {
                    redirect('listing/all_services/' . $id . '/' . $bname);
                } else {
                    redirect('home/details/' . $lid . '/' . $lname);
                }
            } else {
                $uid = $this->session->userdata('user_id');
                $cdate = $this->input->post('cdate');
                $service = $this->input->post('service');
                $sfrom = $this->input->post('sfrom');
                $sto = $this->input->post('sto');
                $dur = $this->input->post('duration');
                $desc = $this->input->post('desc');
                $bid = $this->booking->customize_booking($cdate, $desc, $vid, $uid);

                $ud = $this->booking->user_detail($vid);
                $to = $ud[0]['userinfo_username'];

                for ($i = 0; $i < count($service); $i++) {
                    $this->booking->customize_slot($bid, $service[$i], $sfrom[$i], $sto[$i], $dur[$i]);
                }
                $bt = 'custom';
                $this->booking_email($to, $bid, $bt);
                $msg .= "<div class='alert alert-success' role='alert>";
                $msg .= "<button type='button' class='close' data-dismiss='alert aria-label='Close'>";
                $msg .= "<span aria-hidden='true'></span>";
                $msg .= "</button>";
                $msg .= "Your booking request has been sent to the vendor. Please wait for their response.";
                $msg .="</div>";
                $bookdata = array(
                    'book_msg' => $msg
                );
                $this->session->set_userdata($bookdata);
                $this->session->unset_userdata('slot_data');
                if ($sec == 'branch') {
                    redirect('listing/branch/' . $id . '/' . $bname);
                } elseif ($sec == 'service') {
                    redirect('listing/all_services/' . $id . '/' . $bname);
                } else {
                    redirect('home/details/' . $lid . '/' . $lname);
                }
            }
        } else {
            redirect('home/login');
        }
    }

    function list_customer() {
        if ($this->session->userdata('user_id')) {
            $id = $this->session->userdata('user_id');
            $cust = $this->booking->all_customer($id);
            $ccus = array();

            for ($i = 0; $i < count($cust); $i++) {
                $cid = $cust[$i]['user_Id'];

                $cus = $this->booking->coupon_customers($cid);
                if (!empty($cus)) {
                    $ccus[$i]['coupon_code'] = 'Yes';
                } else {
                    $ccus[$i]['coupon_code'] = 'No';
                }

                $note = $this->booking->customer_notes($cid, $id);
                if (!empty($note)) {
                    $ccus[$i]['note'] = $note[0]['note'];
                } else {
                    $ccus[$i]['note'] = '';
                }

                $ccus[$i]['userinfo_id'] = $cust[$i]['userinfo_id'];
                $ccus[$i]['name'] = $cust[$i]['name'];
                $ccus[$i]['userinfo_username'] = $cust[$i]['userinfo_username'];
                $ccus[$i]['address1'] = $cust[$i]['address1'];
                $ccus[$i]['address2'] = $cust[$i]['address2'];
                $ccus[$i]['country'] = $cust[$i]['country'];
                $ccus[$i]['phonenumber_one'] = $cust[$i]['phonenumber_one'];
                $ccus[$i]['phonenumber_two'] = $cust[$i]['phonenumber_two'];
            }

            $data['cus'] = $ccus;
            $this->load->view('page/vendor/client-list', $data);
        } else {
            redirect('home/login');
        }
    }

    public function booking_request() {
        if ($this->session->userdata('user_id')) {
            $id = $this->session->userdata('user_id');
            $data['book'] = $this->booking->custom_booking($id);
            $this->load->view('page/vendor/customize-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function custom_booking() {
        if ($this->session->userdata('user_id')) {
            $id = $this->session->userdata('user_id');
            $data['list'] = $this->listing->get_listing($id);
            $data['book'] = $this->booking->get_customize_booking($id);
            $this->load->view('page/client/custom-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function view_booking() {
        if ($this->session->userdata('user_id')) {
            $bid = $this->uri->segment(3);
            $data['book'] = $this->booking->get_custom_booking($bid);
            $data['slot'] = $this->booking->get_custom_slot($bid);
            $this->load->view('page/vendor/view-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function booking_details() {
        if ($this->session->userdata('user_id')) {
            $bid = $this->uri->segment(3);
            $data['book'] = $this->booking->get_custom_booking($bid);
            $data['slot'] = $this->booking->get_custom_slot($bid);
            $this->load->view('page/client/view-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function modify_booking() {
        if ($this->session->userdata('user_id')) {
            $bid = $this->uri->segment(3);

            $book = $this->booking->get_custom_booking($bid);
            $id = $book[0]['vendor_id'];
            $data['list'] = $this->listing->get_listing($id);
            $data['slot'] = $this->booking->get_custom_slot($bid);
            $data['book'] = $book;
            $this->load->view('page/client/modify-booking', $data);
        } else {
            redirect('home/login');
        }
    }

    public function booking_modify() {
        if ($this->session->userdata('user_id')) {
            $bid = $this->input->post('bid');
            $this->form_validation->set_rules('bid', 'Booking Id', 'trim|required');
            $this->form_validation->set_rules('cdate', 'Date', 'trim|required');
            $this->form_validation->set_rules('service[]', 'Service', 'trim|required');
            $this->form_validation->set_rules('sfrom[]', 'From', 'trim|required');
            $this->form_validation->set_rules('sto[]', 'To', 'trim|required');
            $this->form_validation->set_rules('duration[]', 'Duration', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('booking/modify_booking/' . $bid);
            } else {
                $cdate = $this->input->post('cdate');
                $service = $this->input->post('service');
                $sfrom = $this->input->post('sfrom');
                $sto = $this->input->post('sto');
                $dur = $this->input->post('duration');
                $desc = $this->input->post('desc');
                $this->booking->modify_customize_booking($bid, $cdate, $desc);

                $slot = $this->booking->get_custom_slot($bid);

                for ($i = 0; $i < count($slot); $i++) {
                    $rid = $slot[$i]['rs_id'];
                    $this->booking->modify_customize_slot($rid, $service[$i], $sfrom[$i], $sto[$i], $dur[$i]);
                }
                $this->session->set_flashdata('succ_msg', 'You Have Successfully Modify the Booking');
                redirect('booking/modify_booking/' . $bid);
            }
        } else {
            redirect('home/login');
        }
    }

    public function approve_customize_booking() {
        $bid = $this->uri->segment(3);
        if ($this->session->userdata('user_id')) {
            $this->booking->approve_customize_booking($bid);
            $bd = $this->booking->get_custom_booking($bid);
            $to = $bd[0]['userinfo_username'];
            $ui = $this->session->userdata('user_id');

            $book = $this->booking->new_customize_booking($ui);
            $cbook = $this->session->userdata('nofbook');
            if (count($book) != 0) {
                $this->session->set_userdata(array('nofbook' => ($cbook - 1), 'cbook' => 'true'));
            } else {
                $this->session->set_userdata(array('nofbook' => ($cbook - 1)));
                $this->session->unset_userdata('cbook');
            }

            $this->booking_approve($to);
            $this->session->set_flashdata('succ_msg', 'You Have Successfully Approved Booking');
            redirect('booking/booking_request');
        } else {
            redirect('home/login');
        }
    }

    public function customize_booking_reject() {
        $bid = $this->uri->segment(3);
        if ($this->session->userdata('user_id')) {
            $this->booking->reject_customize_booking($bid);
            $bd = $this->booking->get_custom_booking($bid);
            $to = $bd[0]['userinfo_username'];

            $ui = $this->session->userdata('user_id');

            $book = $this->booking->new_customize_booking($ui);
            $cbook = $this->session->userdata('nofbook');
            if (count($book) != 0) {
                $this->session->set_userdata(array('nofbook' => ($cbook - 1), 'cbook' => 'true'));
            } else {
                $this->session->set_userdata(array('nofbook' => ($cbook - 1)));
                $this->session->unset_userdata('cbook');
            }

            $this->reject_email($to);
            $this->session->set_flashdata('war_msg', 'You Rejected The Booking');
            redirect('booking/booking_request');
        } else {
            redirect('home/login');
        }
    }

    public function add_favorites() {
        if ($this->session->userdata('user_id')) {
            $bid = $this->uri->segment(3);
            $uid = $this->session->userdata('user_id');
            $book = $this->booking->get_service($bid);
            $lid = $book[0]['listingId'];
            if (!empty($lid) && !empty($uid)) {
                $ext = $this->booking->ext_favourites($lid, $uid);

                if (count($ext) == 0) {
                    $this->booking->add_favourites($lid, $uid);
                    $this->session->unset_userdata('fav');
                    $fav = $this->booking->all_favourites($uid);
                    $this->session->set_userdata(array('fav' => $fav));
                    $this->session->set_flashdata('succ_msg', 'Listing Added to The Favorites');
                    redirect('booking/my_booking');
                } else {
                    $this->session->set_flashdata('err_msg', 'This Listing Already Exist in Favorites');
                    redirect('booking/my_booking');
                }
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('booking/my_booking');
            }
        } else {
            redirect('home/login');
        }
    }

    function remove_favorites() {
        if ($this->session->userdata('user_id')) {
            $bid = $this->uri->segment(3);
            $uid = $this->session->userdata('user_id');

            if (!empty($bid)) {
                $this->booking->remove_favourites($bid);
                $this->session->unset_userdata('fav');
                $fav = $this->booking->all_favourites($uid);
                $this->session->set_userdata(array('fav' => $fav));
                $this->session->set_flashdata('succ_msg', 'Listing Removed from The Favorites');
                redirect('booking/my_booking');
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('booking/my_booking');
            }
        } else {
            redirect('home/login');
        }
    }

    public function booking_email($to, $bid, $bt) {
        $this->load->library('email');
        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('You Have Received New Booking');

        $service = '';
        $name = '';
        $phone = '';
        $date = '';
        $slotd = '';
        $msg = '';
        if ($bt == 'booking') {
            $book = $this->booking->get_booking($bid);
            $service = $book[0]['listingName'];
            $name = $book[0]['name'] . ' ' . $book[0]['lname'];
            $phone = $book[0]['phonenumber_one'];
            $date = $book[0]['book_date'];
        } else {
            $cbook = $this->booking->custom_booking_detail($bid);
            $service = 'Custom Booking';
            $name = $cbook[0]['name'] . ' ' . $cbook[0]['lname'];
            $phone = $cbook[0]['phonenumber_one'];
            $date = $cbook[0]['request_date'];
            $msg = "<p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'><strong>Notes: </strong>" . $cbook[0]['request_desc'] . "</p>";
            $sld = $this->booking->custom_booking_slot($bid);
            $slotd .= "<table class='table'><thead><tr><th style='margin:0px,20px,20px,0px;'>Service Name</th><th style='margin:0px,20px,20px,0px;'>From Time</th><th style='margin:0px,20px,20px,0px;'>To Time</th><th>Duration</th></tr></thead><tbody>";
            foreach ($sld as $sd) {
                $slotd .= "<tr><td>" . $sd['listingName'] . "</td><td>" . $sd['req_from'] . "</td><td>" . $sd['req_to'] . "</td><td>" . $sd['duration'] . "</td></tr>";
            }
            $slotd .= "</tbody></table><br>";
        }

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>You Have Received New Reservation</title></head>
            <link rel='stylesheet' href=" . base_url() . "assets/css/email.css'>
            <link href=" . base_url() . "/assets/vendor/bootstrap/css/bootstrap.css' rel='stylesheet'> 
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for joining GangFY Reservation !</h1>
            <br>
	    <h3 style='box-sizing: border-box; font-size: 1.00rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Booking Details</h3>
            <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'><strong>Date: </strong>$date</p>
            <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'><strong>Service: </strong>$service</p>
            <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'><strong>Name: </strong>$name</p>
            <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'><strong>Phone: </strong>$phone</p>
            $msg
            <br>
            $slotd
            <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>For More Details Please Loging in to Your Account</p>
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . base_url() . "home/login' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Login Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
            return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function service_reservation($to) {
        $this->load->library('email');
        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('You Have successfully Made Reservation');

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>You Have successfully Made Reservation</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for Reserve with GangFY Reservation !</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>You Have successfully Made Reservation and Wait Until Vendor Confirm Your Booking, For More Details Please Loging in to Your Account</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . base_url() . "home/login' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Login Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
            return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function booking_approve($to) {
        $this->load->library('email');
        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('Your Booking Has Been Approved');

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>Your Booking Has Been Approved by Vendor</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for Reserve with GangFY Reservation !</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>Your Booking Has Been Confirmed , For More Details Please Loging in to Your Account</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . base_url() . "home/login' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Login Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
            return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function reject_email($to) {
        $this->load->library('email');
        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('Your Booking Has Been Rejected');

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>Your Booking Has Been Rejected by Vendor</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for Reserve with GangFY Reservation !</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>Sorry!... Your Booking Has Been Rejected by Vendor, For More Details Please Loging in to Your Account</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . base_url() . "home/login' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Login Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
            return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
