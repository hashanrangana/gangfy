<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('service_model', 'service');
        $this->load->model('feature_model', 'feature');
        $this->load->model('listing_image_model', 'list_image');
        $this->load->model('listing_model', 'listing');
        $this->load->model('listing_slot_model', 'list_slot');
        $this->load->model('main_service_sub_service', 'sub_service');
        $this->load->model('booking_model', 'booking');
        $this->load->model('user_model', 'users');
    }

    public function add_branch() {
        if ($this->checkLoginUser()) {
            $this->form_validation->set_rules('branch', 'Branch', 'trim|required');
            $this->form_validation->set_rules('address1', 'Address Line 1', 'trim|required');
            $this->form_validation->set_rules('pac', 'City / Town', 'trim|required');
            $this->form_validation->set_rules('addRegion', 'Region', 'trim|required');
            $this->form_validation->set_rules('latbox', 'latbox', 'trim|required');
            $this->form_validation->set_rules('lngbox', 'lngbox', 'trim|required');
            $this->form_validation->set_rules('postal', 'Postal Code', 'trim|required');
            $this->form_validation->set_rules('addCountry', 'Country', 'trim|required');
            $this->form_validation->set_rules('editor', 'Description', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/create_branch');
            } else {
                $uid = $this->session->userdata('user_id');
                $branch = $this->input->post('branch');
                $add1 = $this->input->post('address1');
                $add2 = $this->input->post('address2');
                $loct = $this->input->post('pac');
                $region = $this->input->post('addRegion');
                $postal = $this->input->post('postal');
                $country = $this->input->post('addCountry');
                $lat = $this->input->post('latbox');
                $lng = $this->input->post('lngbox');
                $desc = $this->input->post('editor');

                $ext = $this->listing->exist_branch($branch);

                if (count($ext) == 0) {

                    $img_path = wordwrap(strtolower($branch), 1, '-', 0);
                    $path_one = './global/uploads/branch/' . $uid . '/' . $img_path . '/';
                    $path_rpl = str_replace(".", "", $path_one);
                    $path_final = substr($path_rpl, 1);

                    if (!file_exists($path_one)) {
                        mkdir($path_one, 0777, TRUE);
                    }
                    $config = array(
                        'upload_path' => $path_one,
                        'allowed_types' => "gif|jpg|png|jpeg",
                        'overwrite' => TRUE,
                        'max_size' => "2048",
                        'max_height' => "768",
                        'max_width' => "1024",
                        'overwrite' => TRUE
                    );
                    $path_name = '';
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload()) {
                        $data = $this->upload->data();
                        $path_name = $path_final . $data['file_name'];
                    }

                    $this->listing->save_branch($branch, $loct, $region, $postal, $country, $lat, $lng, $add1, $add2, $desc, $path_name, $uid);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully Create Branch');
                    redirect('dashboard/create_branch');
                } else {
                    $this->session->set_flashdata('err_msg', 'This Branch alredy exist');
                    redirect('dashboard/create_branch');
                }
            }
        } else {
            redirect('home/login');
        }
    }

    public function update_branch() {
        if ($this->checkLoginUser()) {
            $brid = $this->input->post('branid');
            $this->form_validation->set_rules('branid', 'Branch Id', 'trim|required');
            $this->form_validation->set_rules('branch', 'Branch', 'trim|required');
            $this->form_validation->set_rules('address1', 'Address Line 1', 'trim|required');
            $this->form_validation->set_rules('pac', 'City / Town', 'trim|required');
            $this->form_validation->set_rules('addRegion', 'Region', 'trim|required');
            $this->form_validation->set_rules('latbox', 'latbox', 'trim|required');
            $this->form_validation->set_rules('lngbox', 'lngbox', 'trim|required');
            $this->form_validation->set_rules('postal', 'Postal Code', 'trim|required');
            $this->form_validation->set_rules('addCountry', 'Country', 'trim|required');
            $this->form_validation->set_rules('editor', 'Description', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/edit_branch/' . $brid);
            } else {
                $uid = $this->session->userdata('user_id');
                $branch = $this->input->post('branch');
                $add1 = $this->input->post('address1');
                $add2 = $this->input->post('address2');
                $loct = $this->input->post('pac');
                $region = $this->input->post('addRegion');
                $postal = $this->input->post('postal');
                $country = $this->input->post('addCountry');
                $lat = $this->input->post('latbox');
                $lng = $this->input->post('lngbox');
                $desc = $this->input->post('editor');
                $bd = $this->listing->branch_detail($brid);
                $image = $bd[0]['branch_logo'];

                $img_path = wordwrap(strtolower($branch), 1, '-', 0);
                $path_one = './global/uploads/branch/' . $uid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );
                $path_name = '';
                $this->upload->initialize($config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                }

                if (!empty($path_name)) {
                    $this->listing->update_branch($brid, $branch, $loct, $region, $postal, $country, $lat, $lng, $add1, $add2, $desc, $path_name);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully Updated Branch Details');
                    redirect('dashboard/edit_branch/' . $brid);
                } else {
                    $this->listing->update_branch($brid, $branch, $loct, $region, $postal, $country, $lat, $lng, $add1, $add2, $desc, $image);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully Updated Branch Details');
                    redirect('dashboard/edit_branch/' . $brid);
                }
            }
        } else {
            redirect('home/login');
        }
    }

    public function delete_branch() {
        if ($this->checkLoginUser()) {
            $id = $this->uri->segment(3);
            $cb = $this->listing->check_branch($id);
            if (count($cb) == 0) {
                $this->listing->delete_branch($id);
                $this->session->set_flashdata('succ_msg', 'You Have Successfully Deleted Branch');
                redirect('dashboard/list_branches');
            } else {
                $this->session->set_flashdata('err_msg', 'This Branch Details Already Using');
                redirect('dashboard/list_branches');
            }
        } else {
            redirect('home/login');
        }
    }

    public function add_listing() {

        if ($this->checkLoginUser()) {
            $this->form_validation->set_rules('branch', 'Branch Name', 'trim|required');
            $this->form_validation->set_rules('listingName', 'Listing Name', 'trim|required');
            $this->form_validation->set_rules('serviceName', 'Services', 'trim|required');
            $this->form_validation->set_rules('viewSubAdd', 'Sub Services', 'trim|required');
            $this->form_validation->set_rules('price', 'price', 'trim|required');

            if ($this->form_validation->run()) {

                $uid = $this->session->userdata('user_id');
                $branch = $this->input->post('branch');
                $lName = $this->input->post('listingName');
                $srv_Id = $this->input->post('serviceName');
                $sb_Id = $this->input->post('viewSubAdd');
                $price = $this->input->post('price');
                $desc = $this->input->post('editor');

                $ext = $this->listing->exist_listing($lName, $srv_Id, $sb_Id);

                if (count($ext) == 0) {
                    $lid = $this->listing->save($branch, $lName, $srv_Id, $sb_Id, $price, $desc, $uid);

                    $listFeatures = $this->input->post('features');

                    $img_path = wordwrap(strtolower($lName), 1, '-', 0);
                    $path_one = './global/uploads/listing/' . $uid . '/' . $img_path . '/';
                    $path_rpl = str_replace(".", "", $path_one);
                    $path_final = substr($path_rpl, 1);

                    if (!file_exists($path_one)) {
                        mkdir($path_one, 0777, TRUE);
                    }

                    $count = count($_FILES['userfile']['size']);
                    foreach ($_FILES as $value)
                        for ($s = 0; $s <= $count - 1; $s++) {

                            $_FILES['userfile']['name'] = $value['name'][$s];
                            $_FILES['userfile']['type'] = $value['type'][$s];
                            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                            $_FILES['userfile']['error'] = $value['error'][$s];
                            $_FILES['userfile']['size'] = $value['size'][$s];

                            $config['upload_path'] = '' . $path_one . '';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '0';
                            $config['overwrite'] = TRUE;

                            $this->upload->initialize($config);
                            $this->upload->do_upload();
                            $data = $this->upload->data();
                            $path_name = $path_final . $data['file_name'];
                            $this->list_image->save_listing_image($lid, $path_name);
                        }

                    if ($listFeatures) {
                        $cfl = count($listFeatures);
                        for ($i = 0; $i < $cfl; $i++) {
                            $this->feature->save_features_listingId($lid, $listFeatures[$i]);
                        }
                    }
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully Create Service');
                    redirect('dashboard/create_services');
                } else {
                    $this->session->set_flashdata('err_msg', 'This services alredy exist');
                    redirect('dashboard/create_services');
                }
            } else {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/create_services');
            }
        } else {
            redirect('/home/login');
        }
    }

    public function create_slot() {
        if ($this->checkLoginUser()) {
            $this->form_validation->set_rules('sdate', 'Service Name', 'trim|required');
            $this->form_validation->set_rules('service[]', 'Service Name', 'trim|required');
            $this->form_validation->set_rules('sfrom[]', 'Slot From', 'trim|required');
            $this->form_validation->set_rules('sto[]', 'Slot To', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/create_time_slots');
            } else {

                $sdate = $this->input->post('sdate');
                $serv = $this->input->post('service');
                $sfrom = $this->input->post('sfrom');
                $sto = $this->input->post('sto');
                $uid = $this->session->userdata('user_id');

                $csv = count($serv);
                for ($i = 0; $i < $csv; $i++) {

                    $exs = $this->listing->exist_slot($sdate, $sfrom[$i], $sto[$i], $serv[$i], $uid);

                    if (count($exs) == 0) {
                        $this->listing->save_slot($sdate, $sfrom[$i], $sto[$i], $serv[$i], $uid);
                    } else {
                        $this->session->set_flashdata('err_msg', 'Some Slot already exist that you added');
                        redirect('dashboard/create_time_slots');
                    }
                }

                $this->session->set_flashdata('succ_msg', 'You Have Successfully Save Slot');
                redirect('dashboard/create_time_slots');
            }
        } else {
            redirect('home/login');
        }
    }

    public function multiple_slot() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('drange', 'List Id', 'trim|required');
            $this->form_validation->set_rules('sfrom', 'Slot From', 'trim|required');
            $this->form_validation->set_rules('sto', 'Slot To', 'trim|required');
            $this->form_validation->set_rules('service[]', 'Service', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/create_time_slots');
            } else {
                $date = $this->input->post('drange');
                $from = $this->input->post('sfrom');
                $to = $this->input->post('sto');
                $serv = $this->input->post('service');
                $uid = $this->session->userdata('user_id');

                $sep_date = explode("-", $date);
                $first_date = $sep_date[0];
                $second_date = $sep_date[1];

                $fdate = date('Y-m-d', strtotime($first_date));
                $sdate = date('Y-m-d', strtotime($second_date));
                $day = $this->day_calculate($fdate, $sdate);

                for ($i = 0; $i < count($serv); $i++) {

                    for ($a = 0; $a < $day; $a++) {
                        $ndate = date('Y-m-d', strtotime($a . " day", strtotime($fdate)));
                        $exs = $this->listing->exist_slot($ndate, $from, $to, $serv[$i], $uid);

                        if (count($exs) == 0) {
                            $this->listing->save_slot($ndate, $from, $to, $serv[$i], $uid);
                        } else {
                            $this->session->set_flashdata('err_msg', 'Some Slot already exist that you added');
                            redirect('dashboard/create_time_slots');
                        }
                    }
                }
                $this->session->set_flashdata('succ_msg', 'You Have Successfully Save Slot');
                redirect('dashboard/create_time_slots');
            }
        } else {
            redirect('home/login');
        }
    }

    function day_calculate($fday, $sday) {
        $date_one = new DateTime($fday);
        $date_two = new DateTime($sday);
        $interval = $date_one->diff($date_two);
        $day = $interval->format('%a');
        return $day + 1;
    }

    public function slot_data() {
        $sid = $this->input->post('sid');

        $data = $this->listing->slot_data($sid);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    public function update_slot() {
        if ($this->session->userdata('user_id')) {
            $this->form_validation->set_rules('slid', 'Slot Id', 'trim|required');
            $this->form_validation->set_rules('sldate', 'Slot Date', 'trim|required');
            $this->form_validation->set_rules('slfrom', 'Slot From', 'trim|required');
            $this->form_validation->set_rules('slto', 'Slot To', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/create_time_slots');
            } else {
                $sid = $this->input->post('slid');
                $date = $this->input->post('sldate');
                $from = $this->input->post('slfrom');
                $to = $this->input->post('slto');
                $uid = $this->session->userdata('user_id');

                $ext = $this->listing->exist_time_slot($date, $from, $to, $uid);

                if (count($ext) == 0) {
                    $this->listing->update_slot($sid, $date, $from, $to);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully Update Slot');
                    redirect('dashboard/create_time_slots');
                } else {
                    $this->session->set_flashdata('err_msg', 'Some Slot already exist that you added');
                    redirect('dashboard/create_time_slots');
                }
            }
        } else {
            redirect('home/login');
        }
    }

    public function search_slot() {
        if ($this->checkLoginUser()) {
            $date = $this->input->post('date');
            $serv = $this->input->post('serv');
            $uid = $this->session->userdata('user_id');
            $data['list'] = $this->listing->get_listing($uid);
            $st = array();

            if (!empty($date) && empty($serv)) {
                $slot = $this->listing->search_slot($date, 'null', $uid);
                for ($i = 0; $i < count($slot); $i++) {
                    $sid = $slot[$i]['slotId'];
                    $cs = $this->listing->check_slot($sid);

                    if (!empty($cs)) {
                        $st[$i]['edit'] = 2;
                    } else {
                        $st[$i]['edit'] = 1;
                    }
                    $st[$i]['slotId'] = $slot[$i]['slotId'];
                    $st[$i]['bookable'] = $slot[$i]['bookable'];
                    $st[$i]['listingName'] = $slot[$i]['listingName'];
                    $st[$i]['slot_date'] = $slot[$i]['slot_date'];
                    $st[$i]['slot_from'] = $slot[$i]['slot_from'];
                    $st[$i]['slot_to'] = $slot[$i]['slot_to'];
                }

                $data['slot'] = $st;
                $this->load->view('page/vendor/buildtimeslots', $data);
            } elseif (empty($date) && !empty($serv)) {
                $slot = $this->listing->search_slot('null', $serv, $uid);
                for ($i = 0; $i < count($slot); $i++) {
                    $sid = $slot[$i]['slotId'];
                    $cs = $this->listing->check_slot($sid);

                    if (!empty($cs)) {
                        $st[$i]['edit'] = 2;
                    } else {
                        $st[$i]['edit'] = 1;
                    }
                    $st[$i]['slotId'] = $slot[$i]['slotId'];
                    $st[$i]['bookable'] = $slot[$i]['bookable'];
                    $st[$i]['listingName'] = $slot[$i]['listingName'];
                    $st[$i]['slot_date'] = $slot[$i]['slot_date'];
                    $st[$i]['slot_from'] = $slot[$i]['slot_from'];
                    $st[$i]['slot_to'] = $slot[$i]['slot_to'];
                }

                $data['slot'] = $st;
                $this->load->view('page/vendor/buildtimeslots', $data);
            } elseif (!empty($date) && !empty($serv)) {
                $slot = $this->listing->search_slot($date, $serv, $uid);
                for ($i = 0; $i < count($slot); $i++) {
                    $sid = $slot[$i]['slotId'];
                    $cs = $this->listing->check_slot($sid);

                    if (!empty($cs)) {
                        $st[$i]['edit'] = 2;
                    } else {
                        $st[$i]['edit'] = 1;
                    }
                    $st[$i]['slotId'] = $slot[$i]['slotId'];
                    $st[$i]['bookable'] = $slot[$i]['bookable'];
                    $st[$i]['listingName'] = $slot[$i]['listingName'];
                    $st[$i]['slot_date'] = $slot[$i]['slot_date'];
                    $st[$i]['slot_from'] = $slot[$i]['slot_from'];
                    $st[$i]['slot_to'] = $slot[$i]['slot_to'];
                }
                $data['slot'] = $st;
                $this->load->view('page/vendor/buildtimeslots', $data);
            } else {
                $this->session->set_flashdata('err_msg', 'Please select date or service to search');
                redirect('dashboard/create_time_slots');
            }
        } else {
            redirect('login');
        }
    }

    public function list_services() {
        if ($this->checkLoginUser()) {
            $this->session->unset_userdata('day');
            $this->session->unset_userdata('elist_id');
            $this->session->unset_userdata('ulist_id');
            $uid = $this->session->userdata('user_id');
            $main['alllisting'] = $this->listing->all_vendor_listing($uid);
            $this->load->view('page/vendor/all_listing', $main);
        } else {
            redirect('/');
        }
    }

    public function list_images() {
        if ($this->checkLoginUser()) {
            $this->session->unset_userdata('list_id');
            $this->session->unset_userdata('day');
            $sid = $this->uri->segment(3);
            $this->session->set_userdata(array('list_id' => $sid));
            $data['img'] = $this->list_image->get_images($sid);
            $this->load->view('page/vendor/list-images', $data);
        } else {
            redirect('home/login');
        }
    }

    public function delete_image() {
        if ($this->checkLoginUser()) {
            $lid = $this->session->userdata('list_id');
            $id = $this->uri->segment(3);

            $image = $this->list_image->get_image($id);
            $img = $image[0]['image'];
            if (!empty($img) && !empty($lid)) {
                $this->list_image->delete_image($id);
                $remove_path = './' . $img;
                unlink($remove_path);
                $this->session->set_flashdata('succ_msg', 'You Have Successfully Deleted Image');
                redirect('listing/list_images/' . $lid);
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('listing/list_images/' . $lid);
            }
        } else {
            redirect('home/login');
        }
    }

    public function update_images() {
        if ($this->checkLoginUser()) {
            $lid = $this->session->userdata('list_id');

            if (!empty($lid)) {
                $image = $this->list_image->get_listing($lid);
                $fname = $image[0]['listingName'];
                $uid = $image[0]['vendorId'];
                $img_path = wordwrap(strtolower($fname), 1, '-', 0);
                $path_one = './global/uploads/listing/' . $uid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );
                $this->upload->initialize($config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                    $this->list_image->update_image($lid, $path_name);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully Updated Images');
                    redirect('listing/list_images/' . $lid);
                } else {
                    $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                    redirect('listing/list_images/' . $lid);
                }
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('listing/list_images/' . $lid);
            }
        } else {
            redirect('home/login');
        }
    }

    public function update_service() {
        if ($this->checkLoginUser()) {
            $this->session->unset_userdata('ulist_id');
            $lid = $this->input->post('lid');
            $this->session->set_userdata(array('ulist_id' => $lid));
            $this->form_validation->set_rules('lid', 'List Id', 'trim|required');
            $this->form_validation->set_rules('branch', 'Branch Name', 'trim|required');
            $this->form_validation->set_rules('listingName', 'Title', 'trim|required');
            $this->form_validation->set_rules('serviceName', 'Services', 'trim|required');
            $this->form_validation->set_rules('viewSubAdd', 'Sub Services', 'trim|required');
            $this->form_validation->set_rules('price', 'price', 'trim|required');
            if ($this->form_validation->run() == FALSE && !empty($lid)) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('dashboard/edit_service');
            } else {
                $branch = $this->input->post('branch');
                $lName = $this->input->post('listingName');
                $srv_Id = $this->input->post('serviceName');
                $sb_Id = $this->input->post('viewSubAdd');
                $price = $this->input->post('price');
                $desc = $this->input->post('editor');

                $this->listing->update_listing($lid, $branch, $lName, $srv_Id, $sb_Id, $price, $desc);
                $this->session->set_flashdata('succ_msg', 'You Have Successfully Updated Sevice');
                redirect('dashboard/edit_service');
            }
        } else {
            redirect('home/login');
        }
    }

    public function delete_feature() {
        if ($this->checkLoginUser()) {
            $lid = $this->uri->segment(3);
            $id = $this->session->userdata('ulist_id');
            if (!empty($lid)) {
                $this->listing->delete_features($lid);
                $this->session->set_flashdata('succ_msg', 'You Have Successfully Deleted Amenities');
                redirect('dashboard/list_features/' . $id);
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/list_features/' . $id);
            }
        } else {
            redirect('home/login');
        }
    }

    public function add_features() {
        if ($this->checkLoginUser()) {
            $id = $this->session->userdata('ulist_id');
            $features = $this->input->post('features');
            if ($features && $id) {
                $cfl = count($features);
                for ($i = 0; $i < $cfl; $i++) {
                    $ext = $this->listing->exist_features($id, $features[$i]);
                    if (count($ext) == 0) {
                        $this->feature->save_features_listingId($id, $features[$i]);
                        if (($cfl - 1) == $i) {
                            $this->session->set_flashdata('succ_msg', 'You Have Successfully Update Amenities');
                            redirect('dashboard/list_features/' . $id);
                        }
                    } else {
                        $this->session->set_flashdata('err_msg', 'Please Dont Add Exist Amenities');
                        redirect('dashboard/list_features/' . $id);
                    }
                }
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
                redirect('dashboard/list_features/' . $id);
            }
        } else {
            redirect('home/login');
        }
    }

    public function search() {

        $service = $_GET['service'];
        $subService = $_GET['subService'];
        $lcity = $_GET['lcity'];

        $listingData = $this->listing->service_by_id($service, $subService, $lcity);
        echo json_encode($listingData);
    }

    public function branch() {
        $bid = $this->uri->segment(3);
        $bname = str_replace("-", " ", $this->uri->segment(4));

        if (!empty($bid) && !empty($bname)) {
            if (!$this->session->userdata('user_id')) {
                $book_data = array(
                    'branch_id' => $bid
                );
                $this->session->set_userdata($book_data);
                redirect('home/login');
            } else {
                $data['srv'] = $this->listing->all_service($bid);
                $data['list'] = $this->listing->branch_details($bid, $bname);
                $data['amt'] = $this->listing->get_amenities($bid);
                $this->load->view('page/site/branch', $data);
            }
        }
    }

    public function all_services() {
        $lid = $this->uri->segment(3);
        $bname = str_replace("-", " ", $this->uri->segment(4));

        if (!empty($lid) && !empty($bname)) {
            $ls = array();
            $list = $this->listing->all_services($lid, $bname);

            for ($i = 0; $i < count($list); $i++) {
                $id = $list[$i]['listingId'];
                $image = $this->listing->get_list_image($id);
                $ls[$i]['image'] = $image[0]['image'];
                $ls[$i]['listingId'] = $list[$i]['listingId'];
                $ls[$i]['branch_id'] = $list[$i]['branch_id'];
                $ls[$i]['listingName'] = $list[$i]['listingName'];
                $ls[$i]['description'] = $list[$i]['description'];
                $ls[$i]['location'] = $list[$i]['location'];
            }

            $data['list'] = $ls;
            $data['srv'] = $this->listing->all_service($lid);
            $this->load->view('page/site/list-services', $data);
        }
    }

    public function listing_image() {
        if ($this->checkLoginUser()) {
            $listingId = $this->uri->segment(3);
            $vendorId = $this->session->userdata('user_id');
            if ($listingId && $vendorId) {
                $main['allListings'] = $this->listing->edit_listing_id($listingId, $vendorId);
                $main['allImages'] = $this->list_image->listing_id_image($listingId);
                if (isset($_POST['add'])) {
                    if (!empty($_FILES)) {
                        $files_url = $this->upload_file_listing('files');
                    } else {
                        $files_url = '';
                    }

                    $data = [];
                    if ($this->is_multi($files_url[0])) {
                        foreach ($files_url[0] as $FileName) {

                            $data = [];
                            $data = array([
                                    'listing_id	' => $this->input->post('listingId'),
                                    'image' => $FileName['file_name'],
                            ]);

                            $this->list_image->save_listing_image($data);
                        }
                        redirect('listing/listing_image/' . $listingId);
                    } else {
                        if ($files_url[0] != '') {

                            $data = array([
                                    'listing_id	' => $this->input->post('listingId'),
                                    'image' => $files_url[0]['file_name'],
                            ]);

                            $this->list_image->save_listing_image($data);
                            redirect('listing/listing_image/' . $listingId);
                        } else {
                            redirect('listing/listing_image/' . $listingId);
                        }
                    }
                } else {
                    $main['mainview'] = 'page/site/listing_image';
                    $this->load->view('page/home', $main);
                }
            } else {
                redirect('/');
            }
        } else {
            redirect('/');
        }
    }

    function is_multi($array) {
        return (count($array) != count($array, 1));
    }

    function upload_file_listing($file) {
        $this->load->library('upload');

        $config['upload_path'] = './global/uploads/listing/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);
        if ($this->upload->do_upload($file)) {
            $image[] = $this->upload->data();

            if (sizeof($image[0]) == 14) {
                $thumb = array();
                $thumb = array($image[0]);

                $this->resizeImage($thumb);
            } else {
                $index = 0;
                $values = array();

                $values = array($image[0]);
                $this->resizeMultipleImage($values);
            }
            return $image;
        } else {
            
        }
    }

    public function resizeImage($filename) {
        $this->load->library('image_lib');

        $source_path = './global/uploads/listing/' . $filename[0]['file_name'];
        $target_path = './global/uploads/listing/thumbnail';
        $target_path = './global/uploads/listing/thumbnail';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => FALSE,
            'width' => 300,
            'height' => 300
        );
        $this->image_lib->clear();

        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
    }

    public function resizeMultipleImage($filename) {
        $this->load->library('image_lib');
        foreach ($filename[0] as $value) {

            $source_path = './global/uploads/listing/' . $value['file_name'];
            $target_path = './global/uploads/listing/thumbnail';
            $target_path = './global/uploads/listing/thumbnail';
            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'maintain_ratio' => TRUE,
                'create_thumb' => FALSE,
                'width' => 300,
                'height' => 300
            );

            $this->image_lib->clear();

            $this->image_lib->initialize($config_manip);
            $this->image_lib->resize();
        }
    }

    public function listing_features() {

        if ($this->checkLoginUser()) {
            $listingId = $this->uri->segment(3);
            $vendorId = $this->session->userdata('user_id');

            if ($listingId && $vendorId) {
                $main['allListings'] = $this->listing->edit_listing_id($listingId, $vendorId);
                $main['allFeatures'] = $this->feature->all_listing_feature($listingId);


                $main['mainview'] = 'page/site/listing_features';
                $this->load->view('page/home', $main);
            } else {
                redirect('/');
            }
        } else {
            redirect('/');
        }
    }

    public function listing_slot() {
        if ($this->checkLoginUser()) {
            $listingId = $this->uri->segment(3);
            $vendorId = $this->session->userdata('user_id');
            if ($listingId && $vendorId) {

                $main['mainview'] = 'page/site/listing_slot';
                $this->load->view('page/home', $main);
            } else {
                redirect('/');
            }
        } else {
            redirect('/');
        }
    }

    public function checkLoginUser() {
        $sess_id = $this->session->userdata('user_id');

        if (!empty($sess_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function save_listing_feature() {
        $this->load->helper('url');

        if (isset($_POST['add'])) {

            if ($this->session->userdata('user_id') && $this->input->post('listingId')) {
                $cat = $this->input->post('subServices');
                if ($cat) {
                    $saveCat = $this->feature->save_features_listingId($cat, $this->input->post('listingId'));
                    if ($saveCat) {
                        redirect('listing/all_listing');
                    }
                } else {
                    redirect('listing/all_listing');
                }
            }
        }
    }

    public function get_by_service() {
        $serviceId = $this->uri->segment(3);

        if ($serviceId) {
            $listingData = $this->sub_service->all_sub_service_get($serviceId);

            echo json_encode($listingData);
        } else {
            echo json_encode(0);
        }
    }

    public function get_by_edit_service() {
        $serviceId = $this->uri->segment(3);
        $listingId = $this->uri->segment(4);

        if ($serviceId && $listingId) {
            $listingData = $this->listing->service_listing_id($listingId, $serviceId);

            echo json_encode($listingData);
        } else {
            echo json_encode(0);
        }
    }

    public function delete_listing() {

        if ($this->checkLoginUser()) {
            $listingId = $this->uri->segment(3);
            $this->listing->achieve_listing_id($listingId);
            redirect('listing/all_listing');
        } else {
            redirect('/');
        }
    }

    public function slot() {

        if ($this->checkLoginUser()) {
            $slotId = $this->uri->segment(3);
            $listingId = $this->uri->segment(4);
            $this->list_slot->delete($slotId);
            redirect('listing/edit_listing/' . $listingId);
        } else {
            redirect('/');
        }
    }

    public function deleteImage() {

        if ($this->checkLoginUser()) {
            $listingImageId = $this->uri->segment(3);
            $listingId = $this->uri->segment(4);
            $name = $this->list_image->get_image_id($listingImageId);

            if ($this->image->delete($listingImageId, $listingId)) {
                if ($name[0]->image != null && file_exists(PUBPATH . "global/uploads/listing/" . $name[0]->image)) {
                    unlink(PUBPATH . "global/uploads/listing/" . $name[0]->image);
                }
                if ($name[0]->image != null && file_exists(PUBPATH . "global/uploads/listing/thumbnail" . $name[0]->image)) {
                    unlink(PUBPATH . "global/uploads/listing/thumbnail" . $name[0]->image);
                }
                redirect('listing/edit_listing/' . $listingId);
            }
        } else {
            redirect('/');
        }
    }

}
