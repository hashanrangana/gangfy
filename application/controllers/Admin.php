<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('service_model', 'service');
        $this->load->model('feature_model', 'feature');
        $this->load->model('sub_services_model', 'sub_services');
        $this->load->model('user_model', 'users');
        $this->load->model('booking_model', 'booking');
    }

    public function checkLoginUser() {
        $sess_id = $this->session->userdata('admin_id');

        if (!empty($sess_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function index() {
        $this->load->helper('url');
        $sess_id = $this->session->userdata('admin_id');

        if (!empty($sess_id)) {
            $result['listingData'] = $this->booking->all_booking_by_admin();

            $result['adminmain'] = 'page/admin/all_order';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function checkLogin() {

        if (isset($_POST['add'])) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('pwd', 'Password', 'required');

            if ($this->form_validation->run()) {
                $email = $this->input->post('email');
                $pwd = $this->input->post('pwd');
                $type = 'admin';

                $result = $this->users->admin_login($email, $pwd, $type);
                if (count($result) == 1) {
                    $login_data = array(
                        'admin_name' => $result[0]['name'],
                        'admin_id' => $result[0]['userinfo_id'],
                        'admin_logged_in' => TRUE
                    );
                    $this->session->set_userdata($login_data);
                    $this->main_view();
                } else {

                    $this->load->view('page/admin/login');
                }
            }
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function main_view() {
        if ($this->checkLoginUser()) {
            $result['listingData'] = $this->booking->all_booking();
            $result['adminmain'] = 'page/admin/all_order';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function all_host() {
        if ($this->checkLoginUser()) {
            $result['listingData'] = $this->users->all_host_by_admin();
            $result['adminmain'] = 'page/admin/all_host';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function all_vendor() {
        if ($this->checkLoginUser()) {
            $result['listingData'] = $this->users->all_vendor_by_admin();
            $result['adminmain'] = 'page/admin/all_vendor';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function logout() {
        $this->session->unset_userdata('admin_logged_in');
        session_destroy();
        $this->load->view('page/admin/login');
    }

    public function all_service() {
        $this->load->helper('url');
        $sess_id = $this->session->userdata('admin_id');

        if (!empty($sess_id)) {
            $result['allservices'] = $this->service->all_service();

            $result['adminmain'] = 'page/admin/all_service';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function create_service() {
        $this->load->helper('url');
        $sess_id = $this->session->userdata('admin_id');

        if (!empty($sess_id)) {
            $result['adminmain'] = 'page/admin/create_service';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function mainSerivceInactive() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->service->in_active_service($Id);
            $result['allservices'] = $this->service->all_service();

            $result['adminmain'] = 'page/admin/all_service';

            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function mainSerivceActive() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->service->active_service($Id);
            $result['allservices'] = $this->service->all_service();

            $result['adminmain'] = 'page/admin/all_service';

            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function createMianService() {
        if (isset($_POST['add'])) {
            $this->form_validation->set_rules('name', 'name', 'trim|required');
            $this->form_validation->set_rules('status', 'status', 'required');

            if ($this->form_validation->run()) {
                $data = array(
                    'serviceName' => $this->input->post('name'),
                    'serviceStatus' => (int) $this->input->post('status'),
                );
                $result['val'] = $this->service->save_service($data);

                redirect('/admin/all_service');
            } else {

                $this->create_service();
            }
        }
    }

    public function all_sub_service() {
        $this->load->helper('url');

        if ($this->checkLoginUser()) {
            $result['allsubservices'] = $this->sub_services->all_sub_service();

            $result['adminmain'] = 'page/admin/all_sub_service';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function subSerivceInactive() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->sub_services->in_active_service($Id);
            $result['allsubservices'] = $this->sub_services->all_sub_service();

            $result['adminmain'] = 'page/admin/all_sub_service';

            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function subSerivceActive() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->sub_services->active_service($Id);
            $result['allsubservices'] = $this->sub_services->all_sub_service();

            $result['adminmain'] = 'page/admin/all_sub_service';

            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function create_sub_service() {
        $this->load->helper('url');
        $sess_id = $this->session->userdata('admin_id');

        if (!empty($sess_id)) {
            $result['adminmain'] = 'page/admin/create_sub_service';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function createSubService() {
        if (isset($_POST['add'])) {
            $this->form_validation->set_rules('name', 'name', 'trim|required');
            $this->form_validation->set_rules('status', 'status', 'required');

            if ($this->form_validation->run()) {
                $data = array(
                    'subServiceName' => $this->input->post('name'),
                    'subServiceStatus' => (int) $this->input->post('status'),
                );

                $result['val'] = $this->sub_services->save_service($data);

                redirect('/admin/all_sub_service');
            } else {

                $this->create_sub_service();
            }
        }
    }

    public function all_features() {
        $this->load->helper('url');

        if ($this->checkLoginUser()) {
            $result['allfeature'] = $this->feature->all_feature();

            $result['adminmain'] = 'page/admin/all_features';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function create_features() {
        $this->load->helper('url');

        if ($this->checkLoginUser()) {
            $result['adminmain'] = 'page/admin/create_features';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function createFeatures() {
        if (isset($_POST['add'])) {
            $this->form_validation->set_rules('name', 'name', 'trim|required');
            $this->form_validation->set_rules('status', 'status', 'required');

            if ($this->form_validation->run()) {
                $data = array(
                    'featuresName' => $this->input->post('name'),
                    'fStatus' => (int) $this->input->post('status'),
                );
                $result['val'] = $this->feature->save_feature($data);

                redirect('/admin/all_features');
            } else {

                $this->create_features();
            }
        }
    }

    public function featuresInactive() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->feature->in_active_feature($Id);
            $result['allfeature'] = $this->feature->all_feature();

            $result['adminmain'] = 'page/admin/all_features';

            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function featuresActive() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->feature->active_feature($Id);
            $result['allfeature'] = $this->feature->all_feature();

            $result['adminmain'] = 'page/admin/all_features';

            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function add_main_sub_service() {
        $this->load->helper('url');
        $sess_id = $this->session->userdata('admin_id');

        if (!empty($sess_id)) {
            $result['allservices'] = $this->service->all_service();
            $result['adminmain'] = 'page/admin/all_main_sub_service';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function main_view_sub() {
        $this->load->helper('url');
        $sess_id = $this->session->userdata('admin_id');
        $Id = $this->uri->segment(3);

        if (!empty($sess_id) && $Id) {

            $this->load->model('main_service_sub_service');

            $result['servicesInfo'] = $this->service->get_service_by_id($Id);
            $result['allSubService'] = $this->main_service_sub_service->all_sub_service_by_service_id($Id);

            $result['adminmain'] = 'page/admin/create_main_sub_service';
            $this->load->view('page/admin_home', $result);
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function update_main_sub_service() {
        $this->load->helper('url');
        $this->load->library('form_validation');
        if (isset($_POST['edit'])) {

            $this->load->model('main_service_sub_service');

            $cat = $this->input->post('subServices');
            $serviceId = $this->input->post('serviceId');

            if ($cat && $serviceId) {
                $saveCat = $this->main_service_sub_service->save($cat, $serviceId);

                if ($saveCat) {

                    redirect('admin/add_main_sub_service');
                }
            } else {
                redirect('admin/add_main_sub_service');
            }
        }
    }

    public function hostBanned() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->users->user_banned($Id);
            redirect('admin/all_host');
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function hostunBanned() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->users->user_unbanned($Id);
            redirect('admin/all_host');
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function vendorApprove() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $this->users->vendor_approve($Id);
            $ud = $this->users->user_details($Id);
            $to = $ud[0]['userinfo_username'];
            $this->vendor_approve($to);
            redirect('admin/all_vendor');
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function vendorBanned() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->users->user_banned($Id);
            redirect('admin/all_vendor');
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function vendorunBanned() {
        $this->load->helper('url');
        $Id = $this->uri->segment(3);

        if ($this->checkLoginUser()) {
            $result['val'] = $this->users->user_unbanned($Id);
            redirect('admin/all_vendor');
        } else {
            $this->load->view('page/admin/login');
        }
    }

    public function vendor_approve($to) {
        $this->load->library('email');
        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('Your Account Has Been Approved');

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>Your Account Has Been Activated</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for joining GangFY Reservation !</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>Your Account Has Been Activated, Please Loging in to Your Account</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . base_url() . "home/login' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Login Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
            return;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
