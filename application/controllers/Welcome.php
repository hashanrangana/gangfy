<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function _construct() {
        parent::_construct();
        if (!$this->session->userdata('user_id')) {
            redirect('home/login');
        }
    }

    public function my_qr(){
        $this->load->view('page/client/my_qr');
    }

    public function client() {
        //if ($this->checkLoginUser()) {
        $this->load->view('page/client/dashboard');
        //} else {
        //redirect('home/login');
        //}
    }

    public function index() {
        if ($this->session->userdata('user_id')) {
            $this->load->model('booking_model', 'booking');
            $this->load->model('listing_model', 'listing');

            $ui = $this->session->userdata('user_id');
            $ut = $this->session->userdata('user_type');
            $sid = $this->session->userdata('slot_id');
            $bid = $this->session->userdata('branch_id');

            if ($ut == 'vendor' && empty($sid) && empty($bid)) {
                $book = $this->booking->new_booking($ui);
                $cbk = $this->booking->new_customize_booking($ui);
                if (count($book) != 0 || count($cbk) != 0) {
                    $this->session->set_userdata(array('nofbook' => count($book) + count($cbk), 'cbook' => 'true'));
                } else {
                    $this->session->unset_userdata('nofbook');
                    $this->session->unset_userdata('cbook');
                }
                $data['cup'] = $this->booking->coupon_booking($ui);
                $cus = $this->booking->all_customer($ui);
                if (!empty($cus)) {
                    $data['cus'] = count($cus);
                }
                $bd = $this->booking->total_booking($ui);
                if (!empty($bd)) {
                    $data['book'] = count($bd);
                }
                $serv = $this->booking->get_services($ui);
                if (!empty($serv)) {
                    $data['srv'] = count($serv);
                }
                $cou = $this->booking->total_coupon($ui);
                if (!empty($cou)) {
                    $data['ct'] = $cou[0]['ctotal'];
                }
                $this->load->view('page/vendor/dashboard', $data);
            } elseif (!empty($bid)) {
                $bra = $this->listing->branch_detail($bid);
                $bname = str_replace(" ", "-", $bra[0]['branch_name']);
                $this->session->unset_userdata('branch_id');
                redirect('listing/branch/' . $bid . '/' . $bname);
            } elseif (!empty($sid)) {
                $slot = $this->booking->listing_data($sid);
                $lid = $slot[0]['listingId'];
                $lname = str_replace(" ", "-", $slot[0]['listingName']);
                $this->session->unset_userdata('slot_id');
                redirect('home/details/' . $lid . '/' . $lname);
            } else {
                $ban = $this->booking->check_ban($ui);
                $fav = $this->booking->all_favourites($ui);
                if (count($ban) > 0) {
                    $this->session->set_userdata(array('banned' => count($ban)));
                } else {
                    $this->session->unset_userdata('banned');
                }
                if (!empty($fav)) {
                    $this->session->set_userdata(array('fav' => $fav));
                }
                $msg = $this->booking->new_messages($ui);
                if (!empty($msg)) {
                    $this->session->set_userdata(array('msg' => $msg));
                }
                $this->load->view('page/client/dashboard');
            }
        } else {
            redirect('home/login');
        }
    }

}
