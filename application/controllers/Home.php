<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('user_model', 'users');
        $this->load->model('service_model', 'service');
        $this->load->model('feature_model', 'feature');
        $this->load->model('listing_image_model', 'list_image');
        $this->load->model('listing_model', 'listing');
        $this->load->library('encrypt');
    }

    public function index() {
        $main['title'] = 'Gangfy Reservation – Best Value. Best chioce.';
        $main['menuStyle'] = '1';

        $main['allservices'] = $this->service->all_home_service();
        $main['mainview'] = 'page/site/main';
        $this->remove_session();
        $this->load->view('page/home', $main);
    }

    public function register() {
        $this->load->library('session');

        if (isset($_POST['add'])) {
            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
//            $this->form_validation->set_rules('usertype', 'Type', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('repassword', 'Repeat password', 'trim|required|matches[password]');

            if ($this->form_validation->run()) {
                $resultNew = $this->users->check_email($this->input->post('email'));

                if (count($resultNew) >= 1) {
                    $main['regCheckStatus'] = 'error';
                    $this->load->view('page/site/register', $main);
                } else {

                    $data = array(
                        'userinfo_type' => 'customer',
                        'name' => $this->input->post('fname'),
                        'lname' => $this->input->post('lname'),
                        'userinfo_username' => $this->input->post('email'),
                        'userinfo_pwd' => md5($this->input->post('password')),
                        'userStatus' => ($this->input->post('usertype') == 'vendor') ? 2 : 1,
                    );

                    $result = $this->users->sign_up($data);

                    $resultUser = $this->users->user_id($result);

                    $to = $this->input->post('email');
                    if (count($resultUser) == 1) {
                        $login_data = array(
                            'user_name' => $resultUser[0]['name'],
                            'user_id' => $resultUser[0]['userinfo_id'],
                            'user_type' => $resultUser[0]['userinfo_type'],
                            'user_image' => $resultUser[0]['image'],
                            'logged_in' => TRUE
                        );

                        $this->session->set_userdata($login_data);

                        if ($this->input->post('usertype') == 'vendor') {
                            $this->session->unset_userdata('logged_in');
                            $this->session->unset_userdata('user_name', 'user_id', 'user_type', 'user_image');
                            session_destroy();
                            $main['vendorCheckStatus'] = 'vendor_success';
                            $sub = 'You Have Successfully Register as Vendor';
                            $title = 'You Have Successfully Register as Vendor Please Waiting For The Approval';
                            $reason = 'Please waiting until we complete the approval process';
                            $this->registration_email($to, $sub, $title, $reason);
                            $this->load->view('page/site/register', $main);
                        } else {
                            $this->session->set_flashdata('succ_msg', 'You Have Successfully Create an Accout and Please Login');
                            $sub = 'You Have Successfully Register as Customer';
                            $title = 'You Have Successfully Register as Customer';
                            $reason = 'Thank you for joining GangFY Reservation Please Use Below Link to Access Your Account';
                            $this->registration_email($to, $sub, $title, $reason);
                            $main['login_data'] = $this->session->userdata();
                            $main['allservices'] = $this->service->all_home_service();
                            redirect('/');
                        }
                    } else {
                        $this->load->view('page/site/register');
                    }
                }
            } else {
                $this->load->view('page/site/register');
            }
        } else {
            $this->load->view('page/site/register');
        }
    }

    public function login() {
        if ($this->session->userdata('user_id')) {
            redirect(base_url());
        } else {
            $this->load->library('session');
            if (isset($_POST['add'])) {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('pwd', 'Password', 'required');

                if ($this->form_validation->run()) {

                    $email = $this->input->post('email');
                    $pwd = $this->input->post('pwd');

                    $result = $this->users->login_reg($email, $pwd);
                    if (!empty($result)) {
                        $statusid = $result[0]['userStatus'];
                        $uid = $result[0]['userinfo_id'];
                        $ban = $this->users->check_banned($uid);
                        if (count($ban) == 0) {
                            if ($statusid == 1) {
                                $login_data = array(
                                    'user_name' => $result[0]['name'],
                                    'user_id' => $uid,
                                    'user_type' => $result[0]['userinfo_type'],
                                    'user_image' => $result[0]['image'],
                                    'logged_in' => TRUE
                                );
                                $this->session->set_userdata($login_data);

                                $main['login_data'] = $this->session->userdata();
                                $main['allservices'] = $this->service->all_home_service();
                                if ($this->session->userdata('slot_id') || $this->session->userdata('branch_id')) {
                                    redirect('welcome');
                                } else {
                                    redirect(base_url());
                                }
                            } else if ($statusid == 2) {
                                $main['CheckVendor'] = true;
                                $this->load->view('page/site/login', $main);
                            } else if ($statusid == 3) {
                                $main['Checkbanner'] = true;
                                $this->load->view('page/site/login', $main);
                            } else {
                                $main['checkStatus'] = true;
                                $this->load->view('page/site/login', $main);
                            }
                        } else {
                            $main['ban'] = 'Your account is temporarily banned, Wait until we get back to you';
                            $this->load->view('page/site/login', $main);
                        }
                    } else {
                        $main['checkStatus'] = true;
                        $this->load->view('page/site/login', $main);
                    }
                } else {
                    $this->load->view('page/site/login', $main);
                }
            } else {
                $this->load->view('page/site/login');
            }
        }
    }

    public function recover() {
        $this->load->view('page/site/recover_password');
    }

    public function gangfy_register() {

        $uname = $this->input->post('user_name');
        $pass = $this->input->post('password');
        $fname = $this->input->post('first_name');
        $lname = $this->input->post('last_name');
        $country = $this->input->post('country');

        $chkemail = $this->users->check_email($uname);

        if (count($chkemail) == 0) {
            $this->users->gangfy_user($uname, $pass, $fname, $lname, $country);
            $sub = 'You Have Successfully Register as Customer';
            $reason = 'Thank you for joining GangFY Reservation Please Use Below Link to Access Your Account';
            $this->registration_email($uname, $sub, $sub, $reason);
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function password_reset() {
        $this->form_validation->set_rules('pass_reset', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('err_msg', 'Please Enter Valid Email Address');
            redirect('home/recover');
        } else {
            $email = $this->input->post('pass_reset');
            $ud = $this->users->get_user_id($email);
            if (!empty($ud)) {
                $euid = $this->encrypt->encode($ud[0]['userinfo_id']);
                $elink = $elink = base_url() . 'home/reset_password/' . str_replace(array('+', '/', '='), array('-', '_', '~'), $euid);
                $this->password_reset_email($email, $elink);
                $this->session->set_flashdata('succ_msg', 'Password Reset Email Has Been Sent! Please Check Your Email');
                redirect('home/recover');
            } else {
                $this->session->set_flashdata('err_msg', 'There is no Account From this Email Address');
                redirect('home/recover');
            }
        }
    }

    public function reset_password() {
        $uid = $this->uri->segment(3);
        if (!empty($uid)) {
            $data['uid'] = $uid;
            $this->load->view('page/site/reset-password', $data);
        } else {
            $this->session->set_flashdata('err_msg', 'Somthing went wrong please try later');
            $this->load->view('page/site/reset-password');
        }
    }

    public function change_password() {
        $uid = $this->input->post('uid');
        $this->form_validation->set_rules('uid', 'User Id', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('repassword', 'Repeat password', 'trim|required|matches[password]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('err_msg', 'Please Enter Valid Password');
            redirect('home/reset_password/' . $uid);
        } else {
            $did = str_replace(array('-', '_', '~'), array('+', '/', '='), $uid);
            $id = $this->encrypt->decode($did);

            if (filter_var($id, FILTER_VALIDATE_INT)) {
                $pass = $this->input->post('repassword');
                $this->users->change_password($id, $pass);
                $this->session->set_flashdata('rps_msg', 'You have successfully changed your password and Log in to your account');
                redirect('home/login');
            } else {
                $this->session->set_flashdata('err_msg', 'Somthing went wrong please contact GangFy Team');
                redirect('home/reset_password/' . $uid);
            }
        }
    }

    public function password_change() {
        $email = $this->input->post('user_name');
        $pass = $this->input->post('password');

        if (!empty($email) && !empty($pass)) {
            $this->users->password_change($email, $pass);
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function update_account() {
        $uname = $this->input->post('user_name');
        $fname = $this->input->post('first_name');
        $lname = $this->input->post('last_name');
        $country = $this->input->post('country');

        $user = $this->users->get_user_id($uname);
        $uid = $user[0]['userinfo_id'];
        if (!empty($uid)) {
            $this->users->update_account($uid, $fname, $lname, $country);
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function details() {
        $listingId = $this->uri->segment(3);
        $listingName = str_replace("-", " ", $this->uri->segment(4));

        if ($listingId && $listingName) {

            $list = $this->listing->listing_id($listingId, $listingName);
            $bid = $list[0]['branch_id'];
            $main['allListings'] = $list;
            $main['listingFeatures'] = $this->feature->get_by_id($listingId);
            $main['listingImages'] = $this->list_image->get_by_id($listingId);
            $main['lsot'] = $this->listing->listing_slot($listingId);
            $main['srv'] = $this->listing->all_service($bid);

            $main['mainview'] = 'page/site/details';
            $this->load->view('page/home', $main);
        } else {
            redirect('/');
        }
    }

    public function logout() {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_name', 'user_id', 'user_type', 'user_image');
        $this->session->sess_destroy();
        redirect('/');
    }

    public function about_us() {
        $main['mainview'] = 'page/site/about_us';
        $this->load->view('page/home', $main);
    }

    public function privacy_policy() {
        $main['mainview'] = 'page/site/privacy_policy';
        $this->load->view('page/home', $main);
    }

    public function contact_us() {
        if (isset($_POST['Send'])) {
            $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('comment', 'Comment', 'trim|required');
            if ($this->form_validation->run()) {
                $ci = get_instance();
                $ci->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "sdineshwaran16@gmail.com";
                $config['smtp_pass'] = "booklu@0047";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $ci->email->initialize($config);
                $ci->email->set_newline("\r\n");

                $ci->email->from('sdineshwaran16@gmail.com', 'Gangfy Reservation');
                $ci->email->to('rsbandulahewa@gmail.com');

                $ci->email->subject('Contacted from Gangfy Reservation - ' . $this->input->post('subject'));
                $mailContent = '
		<table width="100%" border="0" cellspacing="0" cellpadding="10">
		<tr>
		   <td colspan="2" align="center" bgcolor="#343a40"><img src="http://ec2-35-165-49-128.us-west-2.compute.amazonaws.com/gangfy-booking/assets/images/logo.png" width="235" height="36"></td>
		   </tr>
		   <tr>
		   <td colspan="2"><div align="left" style="font-family:font-family: Arial, Helvetica, sans-serif;">Someone as contacted your for,</div></td>
		   </tr>
		   <tr >
		   <td  style="font-family:font-family: Arial, Helvetica, sans-serif;"><strong>Customer Name</strong>:</td>
		   <td  style="font-family:font-family: Arial, Helvetica, sans-serif;">' . $this->input->post('firstname') . ' ' . $this->input->post('lastname') . '</td>
		 </tr>
		 <tr>
		 <td  style="font-family:font-family: Arial, Helvetica, sans-serif;"><strong>Email</strong>:</td>
		 <td  style="font-family:font-family: Arial, Helvetica, sans-serif;">' . $this->input->post('email') . '</td>
	   </tr>
	   <tr>
	   <td  style="font-family:font-family: Arial, Helvetica, sans-serif;"><strong>Comment</strong>:</td>
	   <td  style="font-family:font-family: Arial, Helvetica, sans-serif;">' . $this->input->post('comment') . '</td>
	 </tr>
	 <tr>
	 <td  style="font-family:font-family: Arial, Helvetica, sans-serif;">Regards,<br><br> Gangfy Reservation Team</td>
   </tr>
		 <tr>
		   <td colspan="2"><div align="center" style="font-family:font-family: Arial, Helvetica, sans-serif;">© Gangfy Reservation 2019 | Privacy policy & Terms and Conditions</div></td>
		   </tr>
		</table>';

                $ci->email->message($mailContent);

                if (!$ci->email->send()) {
                    $main['emailSuccess'] = 'success';
                    $main['mainview'] = 'page/site/contact_us';
                    $this->load->view('page/home', $main);
                } else {
                    $main['emailSuccess'] = 'error';
                    $main['mainview'] = 'page/site/contact_us';
                    $this->load->view('page/home', $main);
                }
            } else {

                $main['mainview'] = 'page/site/contact_us';
                $this->load->view('page/home', $main);
            }
        } else {

            $main['mainview'] = 'page/site/contact_us';
            $this->load->view('page/home', $main);
        }
    }

    public function terms_and_conditions() {
        $main['mainview'] = 'page/site/terms_and_conditions';
        $this->load->view('page/home', $main);
    }

    public function profile() {
        if ($this->session->userdata('user_id')) {
            $ut = $this->session->userdata('user_type');
            $uid = $this->session->userdata('user_id');
            $data['prof'] = $this->users->user_data($uid);
            if ($ut == 'vendor') {
                $this->load->view('page/vendor/profile', $data);
            } else {
                $this->load->view('page/client/profile', $data);
            }
        } else {
            redirect('home/login');
        }
    }

    public function update_profile() {
        if ($this->session->userdata('user_id')) {
            $uid = $this->session->userdata('user_id');
            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('pone', 'Phone Code', 'trim|required');
            $this->form_validation->set_rules('adone', 'Add One', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('region', 'Region', 'trim|required');
            $this->form_validation->set_rules('country', 'Country', 'trim|required');
            $this->form_validation->set_rules('postal', 'Postal Code', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('err_msg', 'Please Fill All Required Fields');
                redirect('home/profile');
            } else {
                $prof = $this->users->user_data($uid);
                $image = $prof[0]['image'];
                $fname = $this->input->post('fname');
                $lname = $this->input->post('lname');
                $pone = $this->input->post('pone');
                $ptwo = $this->input->post('ptwo');
                $cname = $this->input->post('cname');
                $adone = $this->input->post('adone');
                $adtwo = $this->input->post('adtwo');
                $email = $this->input->post('email');
                $city = $this->input->post('city');
                $region = $this->input->post('region');
                $country = $this->input->post('country');
                $postal = $this->input->post('postal');

                $img_path = wordwrap(strtolower($fname), 1, '-', 0);
                $path_one = './global/uploads/profile/' . $uid . '/' . $img_path . '/';
                $path_rpl = str_replace(".", "", $path_one);
                $path_final = substr($path_rpl, 1);

                if (!file_exists($path_one)) {
                    mkdir($path_one, 0777, TRUE);
                }
                $config = array(
                    'upload_path' => $path_one,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'overwrite' => TRUE,
                    'max_size' => "2048",
                    'max_height' => "768",
                    'max_width' => "1024",
                    'overwrite' => TRUE
                );
                $path_name = '';
                $this->upload->initialize($config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $path_name = $path_final . $data['file_name'];
                }

                if (!empty($path_name)) {
                    $this->users->update_profile($uid, $fname, $pone, $ptwo, $lname, $path_name, $cname, $email, $adone, $adtwo, $city, $region, $country, $postal);
                    unlink($this->session->userdata('user_image'));
                    $this->session->unset_userdata('user_image');
                    $user_img = array('user_image' => $path_name);
                    $this->session->set_userdata($user_img);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully update your profile details');
                    redirect('home/profile');
                } else {
                    $this->users->update_profile($uid, $fname, $pone, $ptwo, $lname, $image, $cname, $email, $adone, $adtwo, $city, $region, $country, $postal);
                    $this->session->set_flashdata('succ_msg', 'You Have Successfully update your profile details');
                    redirect('home/profile');
                }
            }
        } else {
            redirect('home/login');
        }
    }

    public function registration_email($to, $sub, $title, $reason) {
        $this->load->library('email');

        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject($sub);

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>$title</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for joining GangFY Reservation !</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>$reason</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . base_url() . "home/login' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Login Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function password_reset_email($to, $rlink) {
        $this->load->library('email');

        $this->email->from('noreplymadeapps@gmail.com', 'GangFY Reservation');
        $this->email->to($to, 'GangFY Reservation');
        $this->email->subject('GangFY Account Recovery');

        $message = "<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    <meta name='viewport' content='width=device-width'>
	    <title>Forgotten Password Reset</title></head>
            <link rel='stylesheet' href='" . base_url() . "'assets/css/email.css'>
            <body class='body' style='font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;'>
	    <header class='mt2 mb2' style='margin-bottom: 20px; margin-top: 20px;'>
		<img src='" . base_url() . "assets/img/logo.png'>
	    </header>
        
	    <h1 style='box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;'>Thanks for joining GangFY Reservation !</h1>
	    <p style='box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;'>Please Click on the Following Link to Reset Your Password</p>
	
	    <p class='mt2 mb2 mt3--lg mb3--lg' style='box-sizing: border-box; margin: 0; margin-bottom: 20px; margin-top: 20px; padding: 0;'>
		<span class='button__shadow' style='border-bottom: 2px solid rgba(0,0,0,0.1); border-radius: 4px; box-sizing: border-box; display: block; width: 100%;'>
			<a class='button' href='" . $rlink . "' style='background: #00B9FF; border-radius: 3px; box-sizing: border-box; color: white; display: block; font-size: 1rem; font-weight: 600; padding: 12px 20px; text-align: center; text-decoration: none; width: 100%;' target='_blank'>
				Click Here
			</a>
		</span></p>
		<p class='db mb1 gray' style='box-sizing: border-box; color: #999; display: block; margin: 0; margin-bottom: 10px; padding: 0;'>If you don’t know why you got this email, please tell us straight away so we can fix this for you.</p>                                                    
		<footer class='mt2 mt4--lg' style='border-top: 1px solid #D9D9D9; margin-top: 20px; padding: 20px 0;'>
		<ul style='box-sizing: border-box; list-style: none; margin: 0; margin-bottom: 0; padding: 0;'>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>GangFY Reservation</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/privacy_policy' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;1 target='_blank'>Privacy</a></small>
			</li>
			<li style='box-sizing: border-box; margin: 0; margin-bottom: 10px; padding: 0;'>
				<small style='box-sizing: border-box; color: #999;'><a href='" . base_url() . "home/terms_and_conditions' style='border-bottom: 1px solid #E6E6E6; box-sizing: border-box; color: inherit; text-decoration: none;' target='_blank'>Terms</a></small>
			</li>
		</ul>
	    </footer>
            </body>";

        $this->email->message($message);
        try {
            $this->email->send();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function remove_session() {
        $this->session->unset_userdata('slot_data');
        $this->session->unset_userdata('book_date');
        $this->session->unset_userdata('book_msg');
        $this->session->unset_userdata('price');
        $this->session->unset_userdata('sid');
    }

}
