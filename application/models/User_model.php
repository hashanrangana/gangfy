<?php

class User_model extends CI_Model {

    function login_reg($uname, $pwd) {
        $this->db->where('userinfo_username', $uname);
        $this->db->where('userinfo_pwd', md5($pwd));
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function sign_up($data) {
        $this->db->insert('userinfo', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function admin_login($uname, $pwd, $type) {
        $this->db->where('userinfo_username', $uname);
        $this->db->where('userinfo_pwd', md5($pwd));
        $this->db->where('userinfo_type', $type);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function user_id($Id) {
        $this->db->where('userinfo_id', $Id);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function check_email($email) {
        $this->db->where('userinfo_username', $email);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function all_host_by_admin() {
        $this->db->where('userinfo_type =', 'host');
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function all_vendor_by_admin() {
        $this->db->where('userinfo_type =', 'vendor');
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function user_banned($Id) {
        $this->db->where('userinfo_id', $Id);
        $data['userStatus'] = 3;
        $this->db->update('userinfo', $data);
    }

    function user_unbanned($Id) {
        $this->db->where('userinfo_id', $Id);
        $data['userStatus'] = 1;
        $this->db->update('userinfo', $data);
    }

    function vendor_approve($Id) {
        $this->db->where('userinfo_id', $Id);
        $data['userStatus'] = 1;
        $this->db->update('userinfo', $data);
    }

    function user_data($id) {
        $this->db->where('userinfo_id', $id);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function update_profile($id, $name, $pone, $ptwo, $lname, $image, $cname, $email, $adone, $adtw, $city, $reg, $cuntry, $postal) {
        $this->db->where('userinfo_id', $id);
        $data['name'] = $name;
        $data['phonenumber_one'] = $pone;
        $data['phonenumber_two'] = $ptwo;
        $data['lname'] = $lname;
        $data['image'] = $image;
        $data['companyName'] = $cname;
        $data['contactEmail'] = $email;
        $data['address1'] = $adone;
        $data['address2'] = $adtw;
        $data['city'] = $city;
        $data['region'] = $reg;
        $data['country'] = $cuntry;
        $data['user_postal'] = $postal;
        $this->db->update('userinfo', $data);
    }

    function user_details($id) {
        $this->db->where('userinfo_id', $id);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function get_user_id($email) {
        $this->db->where('userinfo_username', $email);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function change_password($id, $pwd) {
        $this->db->where('userinfo_id', $id);
        $data['userinfo_pwd'] = md5($pwd);
        $this->db->update('userinfo', $data);
    }

    function password_change($email, $pwd) {
        $this->db->where('userinfo_username', $email);
        $data['userinfo_pwd'] = md5($pwd);
        $this->db->update('userinfo', $data);
    }

    function check_banned($id) {
        $this->db->where('customer_id', $id);
        $query = $this->db->get('ban_list');
        $data = $query->result_array();
        return $data;
    }

    function gangfy_user($email, $pwd, $fname, $lname, $country) {
        $this->db->set('userinfo_username', $email);
        $this->db->set('userinfo_pwd', md5($pwd));
        $this->db->set('userinfo_type', 'customer');
        $this->db->set('name', $fname);
        $this->db->set('lname', $lname);
        $this->db->set('country', $country);
        $this->db->insert('userinfo');
    }

    function update_usertype($id, $utype) {
        $this->db->where('userinfo_id', $id);
        $data['userinfo_type'] = $utype;
        $this->db->update('userinfo', $data);
    }

    function update_account($id, $name, $lname, $cuntry) {
        $this->db->where('userinfo_id', $id);
        $data['name'] = $name;
        $data['lname'] = $lname;
        $data['country'] = $cuntry;
        $this->db->update('userinfo', $data);
    }

    function update_user_vendor_profile($data, $userId) {
        return $this->db->query("UPDATE userinfo SET userCatergory = '" . $data['userCatergory'] . "',companyName = '" . $data['companyName'] . "',name = '" . $data['name'] . "',lname = '" . $data['lname'] . "',phonenumber = '" . $data['phonenumber'] . "',mobileNumber = '" . $data['mobileNumber'] . "',contactEmail = '" . $data['contactEmail'] . "',address1 = '" . $data['address1'] . "',address2 = '" . $data['address2'] . "',city = '" . $data['city'] . "',region = '" . $data['region'] . "',country = '" . $data['country'] . "' WHERE userinfo_id = '" . $userId . "' ");
    }

    function update_user_host_profile($data, $userId) {
        return $this->db->query("UPDATE userinfo SET name = '" . $data['name'] . "',lname = '" . $data['lname'] . "',phonenumber = '" . $data['phonenumber'] . "',mobileNumber = '" . $data['mobileNumber'] . "',contactEmail = '" . $data['contactEmail'] . "',address1 = '" . $data['address1'] . "',address2 = '" . $data['address2'] . "',city = '" . $data['city'] . "',region = '" . $data['region'] . "',country = '" . $data['country'] . "' WHERE userinfo_id = '" . $userId . "' ");
    }

    function updatePassword($value) {
//        return $this->db->query("UPDATE userinfo SET userinfo_pwd = '" . $data['pwd'] . "' WHERE userinfo_id = '" . $data['userId'] . "' ");
        $this->db->where('userinfo_id', $value['userId']);
        $data['userinfo_pwd'] = $value['pwd'];
        $this->db->update('userinfo', $data);
    }

}
