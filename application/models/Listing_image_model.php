<?php

class Listing_image_model extends CI_Model {

    public function listing_id_image($listingId) {
        $result = $this->db->query("SELECT  * FROM listing_images WHERE listing_i = '" . $listingId . "'");
        $countValue = count($result->result());
        if ($countValue > 0) {
            return $result->result_array();
        } else {
            return [];
        }
    }

    public function save_listing_image($lid, $image) {
        $this->db->set('listing_id', $lid);
        $this->db->set('image', $image);
        $this->db->insert('listing_images');
    }

    public function get_by_id($listingId) {
        $this->db->where('listing_id', $listingId);
        $query = $this->db->get('listing_images');
        $data = $query->result_array();
        return $data;
    }

    public function get_image_id($listingImageId) {
        $this->db->where('listingImageId', $listingImageId);
        $query = $this->db->get('listing_images');
        $data = $query->result_array();
        return $data;
    }

    public function delete($listingImageId, $listingId) {
        $this->db->query("DELETE FROM listing_images WHERE listingImageId = '" . $listingImageId . "' AND listing_id = '" . $listingId . "'");
        return 1;
    }

    function get_images($lid) {
        $this->db->where('listing_images.listing_id', $lid);
        $this->db->join('listing', 'listing.listingId = listing_images.listing_id');
        $query = $this->db->get('listing_images');
        $data = $query->result_array();
        return $data;
    }

    function delete_image($id) {
        $this->db->where('listingImageId', $id);
        $this->db->delete('listing_images');
    }

    function get_image($id) {
        $this->db->where('listingImageId', $id);
        $query = $this->db->get('listing_images');
        $data = $query->result_array();
        return $data;
    }

    function get_listing($id) {
        $this->db->where('listingId', $id);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function update_image($lid, $image) {
        $this->db->set('listing_id', $lid);
        $this->db->set('image', $image);
        $this->db->insert('listing_images');
    }

}
