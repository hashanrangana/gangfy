<?php

class User_service_category_model extends CI_Model {

    function get_all_user_service() {
        $this->db->where('uServiceStatus', 1);
        $query = $this->db->get('user_service_category');
        $data = $query->result_array();
        return $data;
    }

}
