<?php

class Booking_model extends CI_Model {

    function save_booking($slotid, $date, $coupon, $uid, $amount) {
        $this->db->set('slotId', $slotid);
        $this->db->set('book_date', $date);
        $this->db->set('coupon_code', $coupon);
        $this->db->set('user_Id', $uid);
        $this->db->set('amount', $amount);
        $this->db->insert('booking');
        return $this->db->insert_id();
    }

    function slot_data($sid) {
        $this->db->where('listing_slot.slotId', $sid);
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function list_data($sid) {
        $this->db->where('listingId', $sid);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function slot_day($sid) {
        $this->db->where('slotId', $sid);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function listing_data($lid) {
        $this->db->where('listingId', $lid);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function booking_exist($sid, $uid) {
        $this->db->where('slotId', $sid);
        $this->db->where('orderStatus', 2);
        $this->db->where('user_Id', $uid);
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function list_booking($sid) {
        $this->db->where('listing_slot.user_id', $sid);
        $this->db->order_by('booking.booking_Id', 'DESC');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function customer_booking($sid) {
        $this->db->where('booking.user_id', $sid);
        $this->db->where('userinfo.userinfo_type =', 'customer');
        $this->db->order_by('booking.booking_Id', 'DESC');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function new_booking($id) {
        $this->db->where('listing_slot.user_id', $id);
        $this->db->where('booking.orderStatus', 2);
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function new_customize_booking($id) {
        $this->db->where('booking_request.vendor_id', $id);
        $this->db->where('booking_request.status', 2);
        $this->db->order_by('booking_request.br_id', 'DESC');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking_request.user_id');
        $query = $this->db->get('booking_request');
        $data = $query->result_array();
        return $data;
    }

    function select_slot($date, $lid) {
        $this->db->where('slot_date', $date);
        $this->db->where('listingId', $lid);
        $this->db->where('bookable =', 1);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function select_listing($lid) {
        $this->db->where('listing.listingId', $lid);
        $this->db->join('userinfo', 'userinfo.userinfo_id = listing.vendorId');
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function select_branch($id) {
        $this->db->where('branch.branch_id', $id);
        $this->db->join('userinfo', 'userinfo.userinfo_id = branch.user_id');
        $query = $this->db->get('branch');
        $data = $query->result_array();
        return $data;
    }

    function get_slot($sid) {
        $this->db->where('slotId', $sid);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function get_listing($lid) {
        $this->db->where('listingId', $lid);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function approve_booking($Id) {
        $this->db->where('booking_Id', $Id);
        $data['orderStatus'] = 1;
        $this->db->update('booking', $data);
    }

    function booking_details($bid) {
        $this->db->where('booking.booking_Id', $bid);
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_Id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function get_booking($bid) {
        $this->db->where('booking.booking_Id', $bid);
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_Id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function booking_reject($Id) {
        $this->db->where('booking_Id', $Id);
        $data['orderStatus'] = 3;
        $this->db->update('booking', $data);
    }

    function delete_slot($id) {
        $this->db->where('slotId', $id);
        $this->db->delete('listing_slot');
    }

    function slot_exist($id) {
        $this->db->where('slotId', $id);
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function all_booking() {
        $this->db->order_by('booking.booking_Id', 'DESC');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function all_customer($sid) {
        $this->db->where('listing.vendorId', $sid);
        $this->db->group_by('userinfo.userinfo_id');
        $this->db->where('userinfo.userinfo_type =', 'customer');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function ban_list($sid) {
        $this->db->where('listing.vendorId', $sid);
        $this->db->group_by('userinfo.userinfo_id');
        $this->db->where('userinfo.userinfo_type =', 'customer');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('ban_list', 'ban_list.customer_id = userinfo.userinfo_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function open_slot($id) {
        $this->db->where('slotId', $id);
        $data['bookable'] = 1;
        $this->db->update('listing_slot', $data);
    }

    function close_slot($id) {
        $this->db->where('slotId', $id);
        $data['bookable'] = 2;
        $this->db->update('listing_slot', $data);
    }

    function ban_exist($vid, $cid) {
        $this->db->where('vendor_id', $vid);
        $this->db->where('customer_id', $cid);
        $query = $this->db->get('ban_list');
        $data = $query->result_array();
        return $data;
    }

    function ban_customer($vid, $cid, $reason, $desc) {
        $this->db->set('vendor_id', $vid);
        $this->db->set('customer_id', $cid);
        $this->db->set('reason', $reason);
        $this->db->set('description', $desc);
        $this->db->insert('ban_list');
    }

    function check_ban($cid) {
        $this->db->where('customer_id', $cid);
        $query = $this->db->get('ban_list');
        $data = $query->result_array();
        return $data;
    }

    function coupon_code($lid, $code) {
        $this->db->where('listingId', $lid);
        $this->db->where('coupon_code', $code);
        $this->db->where('status', 1);
        $query = $this->db->get('coupon_code');
        $data = $query->result_array();
        return $data;
    }

    function coupon_customers($uid) {
        $this->db->where('booking.user_Id', $uid);
        $this->db->where('booking.coupon_code !=', "");
        $this->db->group_by('booking.user_Id');
        $this->db->where('userinfo.userinfo_type =', 'customer');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function get_coupon($cid) {
        $this->db->where('cd_id', $cid);
        $query = $this->db->get('coupon_code');
        $data = $query->result_array();
        return $data;
    }

    function coupon_booking($uid) {
        $this->db->select('COUNT(booking.coupon_code) AS cbook, SUM(booking.amount) AS camt');
        $this->db->where('listing.vendorId', $uid);
        $this->db->where('booking.coupon_code !=', "");
        $this->db->where('userinfo.userinfo_type =', 'customer');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('coupon_code', 'coupon_code.listingId = listing.listingId');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function share_coupon($cid, $csid, $uid) {
        $this->db->set('cd_id', $cid);
        $this->db->set('customer_id', $csid);
        $this->db->set('user_id', $uid);
        $this->db->insert('coupon_share');
    }

    function shared_coupon($uid) {
        $this->db->group_by('coupon_share.customer_id');
        $this->db->where('coupon_code.user_id', $uid);
        $this->db->join('listing', 'listing.listingId = coupon_code.listingId');
        $this->db->join('coupon_share', 'coupon_share.cd_id = coupon_code.cd_id');
        $this->db->join('userinfo', 'userinfo.userinfo_id = coupon_share.customer_id');
        $query = $this->db->get('coupon_code');
        $data = $query->result_array();
        return $data;
    }

    function coupon_book($ccode) {
        $this->db->select('COUNT(booking_Id) AS tbook');
        $this->db->where('coupon_code', $ccode);
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function total_booking($uid) {
        $this->db->where('listing_slot.user_id', $uid);
        $this->db->order_by('booking.booking_Id', 'DESC');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking.user_id');
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function get_services($id) {
        $this->db->where('vendorId', $id);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function total_coupon($id) {
        $this->db->select('COUNT(cd_id) AS ctotal');
        $this->db->where('user_id', $id);
        $query = $this->db->get('coupon_code');
        $data = $query->result_array();
        return $data;
    }

    function list_coupon($id) {
        $this->db->group_by('coupon_share.cd_id');
        $this->db->where('coupon_share.customer_id', $id);
        $this->db->where('coupon_code.status', 1);
        $this->db->join('userinfo', 'userinfo.userinfo_id = coupon_share.user_id');
        $this->db->join('coupon_code', 'coupon_code.cd_id = coupon_share.cd_id');
        $this->db->join('listing', 'listing.listingId = coupon_code.listingId');
        $query = $this->db->get('coupon_share');
        $data = $query->result_array();
        return $data;
    }

    function get_service($id) {
        $this->db->where('booking.booking_Id', $id);
        $this->db->join('listing_slot', 'listing_slot.slotId = booking.slotId');
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function ext_favourites($id, $uid) {
        $this->db->where('listingId', $id);
        $this->db->where('user_id', $uid);
        $this->db->where('status', 1);
        $query = $this->db->get('favourites');
        $data = $query->result_array();
        return $data;
    }

    function add_favourites($id, $uid) {
        $this->db->set('listingId', $id);
        $this->db->set('user_id', $uid);
        $this->db->insert('favourites');
    }

    function get_favourites($lid, $uid) {
        $this->db->where('listingId', $lid);
        $this->db->where('user_id', $uid);
        $this->db->where('status', 1);
        $query = $this->db->get('favourites');
        $data = $query->result_array();
        return $data;
    }

    function remove_favourites($id) {
        $this->db->where('fav_id', $id);
        $data['status'] = 3;
        $this->db->update('favourites', $data);
    }

    function all_favourites($uid) {
        $this->db->select('*,favourites.created_at AS fdate');
        $this->db->where('user_id', $uid);
        $this->db->where('status', 1);
        $this->db->join('listing', 'listing.listingId = favourites.listingId');
        $query = $this->db->get('favourites');
        $data = $query->result_array();
        return $data;
    }

    function save_feedback($fb, $uid) {
        $this->db->set('feedback', $fb);
        $this->db->set('user_id', $uid);
        $this->db->insert('feedback');
    }

    function save_message($msg, $cid, $vid) {
        $this->db->set('message', $msg);
        $this->db->set('customer_id', $cid);
        $this->db->set('vendor_id', $vid);
        $this->db->insert('messages');
    }

    function get_messages($id) {
        $this->db->order_by('messages.msg_id', 'DESC');
        $this->db->where('messages.customer_id', $id);
        $this->db->join('userinfo', 'userinfo.userinfo_id = messages.vendor_id');
        $query = $this->db->get('messages');
        $data = $query->result_array();
        return $data;
    }

    function new_messages($id) {
        $this->db->order_by('messages.msg_id', 'DESC');
        $this->db->where('messages.customer_id', $id);
        $this->db->where('messages.status', 1);
        $this->db->join('userinfo', 'userinfo.userinfo_id = messages.vendor_id');
        $query = $this->db->get('messages');
        $data = $query->result_array();
        return $data;
    }

    function get_message($id, $cid) {
        $this->db->where('messages.msg_id', $id);
        $this->db->where('messages.customer_id', $cid);
        $this->db->join('userinfo', 'userinfo.userinfo_id = messages.vendor_id');
        $query = $this->db->get('messages');
        $data = $query->result_array();
        return $data;
    }

    function read_message($id, $time) {
        $this->db->where('msg_id', $id);
        $data['status'] = 2;
        $data['created_at'] = $time;
        $this->db->update('messages', $data);
    }

    function all_messages($id) {
        $this->db->order_by('messages.msg_id', 'DESC');
        $this->db->where('messages.vendor_id', $id);
        $this->db->join('userinfo', 'userinfo.userinfo_id = messages.customer_id');
        $query = $this->db->get('messages');
        $data = $query->result_array();
        return $data;
    }

    function check_notes($bid, $uid) {
        $this->db->where('booking_Id', $bid);
        $this->db->where('user_id', $uid);
        $query = $this->db->get('booking_notes');
        $data = $query->result_array();
        return $data;
    }

    function save_note($bid, $note, $uid) {
        $this->db->set('booking_Id', $bid);
        $this->db->set('note', $note);
        $this->db->set('user_id', $uid);
        $this->db->insert('booking_notes');
    }

    function update_note($id, $note) {
        $this->db->where('bn_id', $id);
        $data['note'] = $note;
        $this->db->update('booking_notes', $data);
    }

    function customer_notes($cid, $uid) {
        $this->db->where('customer_id', $cid);
        $this->db->where('user_id', $uid);
        $query = $this->db->get('customer_notes');
        $data = $query->result_array();
        return $data;
    }

    function save_customer_note($cid, $note, $uid) {
        $this->db->set('customer_id', $cid);
        $this->db->set('note', $note);
        $this->db->set('user_id', $uid);
        $this->db->insert('customer_notes');
    }

    function update_customer_note($id, $note) {
        $this->db->where('cn_id', $id);
        $data['note'] = $note;
        $this->db->update('customer_notes', $data);
    }

    function customize_booking($date, $desc, $vid, $uid) {
        $this->db->set('request_date', $date);
        $this->db->set('request_desc', $desc);
        $this->db->set('vendor_id', $vid);
        $this->db->set('user_id', $uid);
        $this->db->insert('booking_request');
        return $this->db->insert_id();
    }

    function modify_customize_booking($id, $date, $desc) {
        $this->db->where('br_id', $id);
        $data['request_date'] = $date;
        $data['request_desc'] = $desc;
        $this->db->update('booking_request', $data);
    }

    function customize_slot($bid, $lid, $from, $to, $dur) {
        $this->db->set('br_id', $bid);
        $this->db->set('listingId', $lid);
        $this->db->set('req_from', $from);
        $this->db->set('req_to', $to);
        $this->db->set('duration', $dur);
        $this->db->insert('request_slot');
    }

    function modify_customize_slot($id, $lid, $from, $to, $dur) {
        $this->db->where('rs_id', $id);
        $data['listingId'] = $lid;
        $data['req_from'] = $from;
        $data['req_to'] = $to;
        $data['duration'] = $dur;
        $this->db->update('request_slot', $data);
    }

    function custom_booking($sid) {
        $this->db->where('booking_request.vendor_id', $sid);
        $this->db->order_by('booking_request.br_id', 'DESC');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking_request.user_id');
        $query = $this->db->get('booking_request');
        $data = $query->result_array();
        return $data;
    }

    function get_custom_booking($bid) {
        $this->db->where('booking_request.br_id', $bid);
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking_request.user_id');
        $query = $this->db->get('booking_request');
        $data = $query->result_array();
        return $data;
    }

    function get_custom_slot($bid) {
        $this->db->where('br_id', $bid);
        $this->db->join('listing', 'listing.listingId = request_slot.listingId');
        $query = $this->db->get('request_slot');
        $data = $query->result_array();
        return $data;
    }

    function approve_customize_booking($id) {
        $this->db->where('br_id', $id);
        $data['status'] = 1;
        $this->db->update('booking_request', $data);
    }

    function reject_customize_booking($id) {
        $this->db->where('br_id', $id);
        $data['status'] = 3;
        $this->db->update('booking_request', $data);
    }

    function user_detail($id) {
        $this->db->where('userinfo_id', $id);
        $query = $this->db->get('userinfo');
        $data = $query->result_array();
        return $data;
    }

    function get_customize_booking($sid) {
        $this->db->where('booking_request.user_id', $sid);
        $this->db->order_by('booking_request.br_id', 'DESC');
        $this->db->where('userinfo.userinfo_id NOT IN (SELECT ban_list.customer_id FROM ban_list)');
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking_request.user_id');
        $query = $this->db->get('booking_request');
        $data = $query->result_array();
        return $data;
    }

    function custom_booking_detail($bid) {
        $this->db->where('booking_request.br_id', $bid);
        $this->db->join('userinfo', 'userinfo.userinfo_id = booking_request.user_id');
        $query = $this->db->get('booking_request');
        $data = $query->result_array();
        return $data;
    }

    function custom_booking_slot($bid) {
        $this->db->where('request_slot.br_id', $bid);
        $this->db->join('listing', 'listing.listingId = request_slot.listingId');
        $query = $this->db->get('request_slot');
        $data = $query->result_array();
        return $data;
    }

    public function all_booking_by_user_update($id) {
        $query = $this->db->query("SELECT * FROM booking  RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.vendor_Id = userinfo.userinfo_id   WHERE host_Id='" . $id . "'");
        return $query->result();
    }

    public function all_booking_by_user($id) {
        $query = $this->db->query("SELECT * FROM booking  RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.vendor_Id = userinfo.userinfo_id   WHERE host_Id='" . $id . "'");
        return $query->result();
    }

    public function all_booking_by_vendor($id) {

        $query = $this->db->query("SELECT * FROM booking RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.host_Id = userinfo.userinfo_id  WHERE vendor_Id='" . $id . "'");

        return $query->result();
    }

    public function all_booking_by_admin() {
        $query = $this->db->query("SELECT * FROM booking RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId  RIGHT JOIN userinfo ON booking.host_Id = userinfo.userinfo_id WHERE booking.host_Id!='null'");

        return $query->result();
    }

    public function save_approve_booking($Id) {
        $this->db->query("UPDATE booking SET orderStatus = 'Confirmed' WHERE booking_Id = '" . $Id . "' ");
        $query = $this->db->query("SELECT * FROM booking  WHERE booking_Id ='" . $Id . "'");

        return $query->result();
    }

    public function all_booking_by_user_upcoming($id) {
        $datVal = date('Y-m-d H:i:s');
        $query = $this->db->query("SELECT * FROM booking  RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.vendor_Id = userinfo.userinfo_id   WHERE host_Id='" . $id . "' AND booking_from >= '" . $datVal . "'");
        return $query->result();
    }

    public function all_booking_by_user_old_booking($id) {
        $datVal = date('Y-m-d H:i:s');
        $query = $this->db->query("SELECT * FROM booking  RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.vendor_Id = userinfo.userinfo_id   WHERE host_Id='" . $id . "' AND booking_from <= '" . $datVal . "'");
        return $query->result();
    }

    public function all_booking_by_vendor_upcoming($id) {
        $datVal = date('Y-m-d H:i:s');
        $query = $this->db->query("SELECT * FROM booking RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.host_Id = userinfo.userinfo_id  WHERE vendor_Id='" . $id . "' AND booking_from >= '" . $datVal . "'");
        return $query->result();
    }

    public function all_booking_by_vendor_old_booking($id) {
        $datVal = date('Y-m-d H:i:s');
        $query = $this->db->query("SELECT * FROM booking RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.host_Id = userinfo.userinfo_id  WHERE vendor_Id='" . $id . "' AND booking_from <= '" . $datVal . "'");
        return $query->result();
    }

    public function all_orders_by_user($id) {
        $query = $this->db->query("SELECT * FROM booking WHERE vendor_Id = '" . $id . "'");
        return $query->result();
    }

    public function all_pending_by_user($id) {
        $query = $this->db->query("SELECT * FROM booking WHERE vendor_Id = '" . $id . "' AND orderStatus = 'Pending'");
        return $query->result();
    }

    public function all_confirm_by_user($id) {
        $query = $this->db->query("SELECT * FROM booking WHERE vendor_Id = '" . $id . "' AND orderStatus = 'Confirmed'");
        return $query->result();
    }

    public function latest_listing($id) {
        $query = $this->db->query("SELECT * FROM listing LEFT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN sub_services ON listing.sub_service_Id = sub_services.subServicesId WHERE vendorId='" . $id . "' AND activeStatus != '2' ORDER BY `created_at` DESC LIMIT 5");
        return $query->result();
    }

    public function latest_orders($id) {
        $query = $this->db->query("SELECT * FROM booking RIGHT JOIN listing ON booking.listing_Id = listing.listingId  RIGHT JOIN services ON listing.service_Id = services.serviceId RIGHT JOIN userinfo ON booking.host_Id = userinfo.userinfo_id  WHERE vendor_Id='" . $id . "' ORDER BY `create_At` DESC LIMIT 5");
        return $query->result();
    }

}
