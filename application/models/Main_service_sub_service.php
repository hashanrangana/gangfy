<?php

class Main_service_sub_service extends CI_Model {

    public function save($data, $serviceId) {
        $this->db->where('services_Id', $serviceId); // I change id with book_id
        $this->db->delete('main_service_sub_service');
        $countAdd = 0;
        foreach ($data as $value) {
            $this->db->query("INSERT INTO main_service_sub_service (services_Id,sub_services_id,activeStatus) VALUES('" . $serviceId . "','" . $value . "','" . 1 . "')");
            $countAdd ++;
        }
        if (count($data) == $countAdd) {
            return 1;
        }
    }

    public function all_sub_service_by_service_id($serviceId) {
        $result = $this->db->query("SELECT  * FROM sub_services WHERE subServiceStatus = 1");
        $index = 0;
        $countValue = count($result->result());

        if ($countValue > 0) {
            foreach ($result->result() as $value) {

                $imgQuery = $this->db->query("SELECT * FROM main_service_sub_service  WHERE services_Id= '" . $serviceId . "' AND sub_services_id='" . $value->subServicesId . "'");

                if (count($imgQuery->result())) {
                    $data[] = array(
                        'serviceId' => $value->subServicesId,
                        'serviceName' => $value->subServiceName,
                        'selected' => 1
                    );
                } else {
                    $data[] = array(
                        'serviceId' => $value->subServicesId,
                        'serviceName' => $value->subServiceName,
                        'selected' => 0
                    );
                }
                $index++;
            }
        } else {
            $data[] = null;
        }
        return $data;
    }

    public function all_sub_service_get($serviceId) {
        $this->db->where('services_Id', $serviceId);
        $this->db->join('sub_services', 'sub_services.subServicesId = main_service_sub_service.sub_services_id');
        $query = $this->db->get('main_service_sub_service');
        $data = $query->result_array();
        return $data;
    }

    public function all_sub_service_edit($serviceId, $listingId) {
        $query = $this->db->query("SELECT * FROM main_service_sub_service   LEFT JOIN sub_services ON main_service_sub_service.sub_services_id = sub_services.subServicesId  WHERE services_Id='" . $serviceId . "'");
        return $query->result();
    }

}
