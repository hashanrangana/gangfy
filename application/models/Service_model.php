<?php

class Service_model extends CI_Model {

    public function all_service() {
        $query = $this->db->get('services');
        $data = $query->result_array();
        return $data;
    }

    public function all_home_service() {
        $this->db->where('serviceStatus', 1);
        $query = $this->db->get('services');
        $data = $query->result_array();
        return $data;
    }

    public function in_active_service($Id) {
        $this->db->where('serviceId', $Id);
        $data['serviceStatus'] = 0;
        $this->db->update('services', $data);
    }

    public function active_service($Id) {
        $this->db->where('serviceId', $Id);
        $data['serviceStatus'] = 1;
        $this->db->update('services', $data);
    }

    public function save_service($data) {
        return $this->db->query("INSERT INTO services (	serviceName,serviceStatus) VALUES('" . $data['serviceName'] . "','" . $data['serviceStatus'] . "')");
    }

    public function get_service_by_id($Id) {
        $this->db->where('serviceId', $Id);
        $query = $this->db->get('services');
        $data = $query->result_array();
        return $data;
    }

}
