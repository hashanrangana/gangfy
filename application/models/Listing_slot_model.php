<?php

class Listing_slot_model extends CI_Model {

    public function save($sname, $sday, $sfrom, $sto, $lid) {
        $this->db->set('slot_name', $sname);
        $this->db->set('slot_day', $sday);
        $this->db->set('slot_from', $sfrom);
        $this->db->set('slot_to', $sto);
        $this->db->set('listing_id', $lid);
        $this->db->insert('listing_slot');
    }

    public function listing_id_edit($listingId) {
        $this->db->where('listingId', $listingId);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    public function delete($slotId) {
        $this->db->where('slotId', $slotId);
        $this->db->delete('listing_slot');
    }

}
