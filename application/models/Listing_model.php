<?php

class Listing_model extends CI_Model {

    public function save($branch, $lName, $srv_Id, $sb_Id, $price, $desc, $vndId) {
        $this->db->set('branch_id', $branch);
        $this->db->set('listingName', $lName);
        $this->db->set('service_Id', $srv_Id);
        $this->db->set('sub_service_Id', $sb_Id);
        $this->db->set('price', $price);
        $this->db->set('description', $desc);
        $this->db->set('vendorId', $vndId);
        $this->db->insert('listing');
        return $this->db->insert_id();
    }

    public function all() {
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    public function all_vendor_listing($id) {
        $this->db->where('listing.vendorId', $id);
        $this->db->where('listing.activeStatus !=', 2);
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    public function edit_listing_id($listingId, $vendorId) {
        $this->db->where('listingId', $listingId);
        $this->db->where('vendorId', $vendorId);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    public function update_listing($lid, $branch, $lName, $srv_Id, $sb_Id, $price, $desc) {
        $this->db->where('listingId', $lid);
        $data['branch_id'] = $branch;
        $data['listingName'] = $lName;
        $data['service_Id'] = $srv_Id;
        $data['sub_service_Id'] = $sb_Id;
        $data['price'] = $price;
        $data['description'] = $desc;
        $this->db->update('listing', $data);
    }

    function save_slot($date, $sfrom, $sto, $lid, $uid) {
        $this->db->set('slot_date', $date);
        $this->db->set('slot_from', $sfrom);
        $this->db->set('slot_to', $sto);
        $this->db->set('listingId', $lid);
        $this->db->set('user_id', $uid);
        $this->db->insert('listing_slot');
        return $this->db->insert_id();
    }

    function exist_slot($sdate, $from, $to, $lid, $uid) {
        $this->db->where('slot_date', $sdate);
        $this->db->where('slot_from', $from);
        $this->db->where('slot_to', $to);
        $this->db->where('listingId', $lid);
        $this->db->where('user_id', $uid);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function slot_listing($lid, $sid) {
        $this->db->set('listingId', $lid);
        $this->db->set('slotId', $sid);
        $this->db->insert('slot_listing');
    }

    function check_slot($sid) {
        $this->db->where('slotId', $sid);
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function slot_data($sid) {
        $this->db->where('slotId', $sid);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function exist_time_slot($date, $from, $to, $uid) {
        $this->db->where('slot_date', $date);
        $this->db->where('slot_from', $from);
        $this->db->where('slot_to', $to);
        $this->db->where('user_id', $uid);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function update_slot($sid, $date, $from, $to) {
        $this->db->where('slotId', $sid);
        $data['slot_date'] = $date;
        $data['slot_from'] = $from;
        $data['slot_to'] = $to;
        $this->db->update('listing_slot', $data);
    }

    function exist_listing($lname, $sid, $subid) {
        $this->db->where('listingName', $lname);
        $this->db->where('service_Id', $sid);
        $this->db->where('sub_service_Id', $subid);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function get_listing($id) {
        $this->db->where('vendorId', $id);
        $this->db->where('activeStatus', 1);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function get_services($id) {
        $this->db->where('branch_id', $id);
        $this->db->where('activeStatus', 1);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function get_time_slot($id) {
        $this->db->where('user_id', $id);
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function get_service($id) {
        $this->db->where('listingId', $id);
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function search_slot($date, $lid, $id) {
        if ($date != 'null') {
            $this->db->where('listing_slot.slot_date', $date);
        }
        if ($lid != 'null') {
            $this->db->where('listing_slot.listingId', $lid);
        }
        $this->db->where('user_id', $id);
        $this->db->join('listing', 'listing.listingId = listing_slot.listingId');
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    function list_features($id) {
        $this->db->where('listing_features.listing_id', $id);
        $this->db->join('features', 'features.featuresId = listing_features.features_id');
        $this->db->join('listing', 'listing.listingId = listing_features.listing_id');
        $query = $this->db->get('listing_features');
        $data = $query->result_array();
        return $data;
    }

    function delete_features($id) {
        $this->db->where('listingfeaturesId', $id);
        $this->db->delete('listing_features');
    }

    function exist_features($lid, $fid) {
        $this->db->where('listing_id', $lid);
        $this->db->where('features_id', $fid);
        $query = $this->db->get('listing_features');
        $data = $query->result_array();
        return $data;
    }

    function exist_branch($branch) {
        $this->db->where('branch_name', $branch);
        $query = $this->db->get('branch');
        $data = $query->result_array();
        return $data;
    }

    function save_branch($branch, $loc, $region, $postal, $country, $lat, $lng, $adone, $adtwo, $desc, $logo, $uid) {
        $this->db->set('branch_name', $branch);
        $this->db->set('location', $loc);
        $this->db->set('region', $region);
        $this->db->set('postal_code', $postal);
        $this->db->set('country', $country);
        $this->db->set('lat', $lat);
        $this->db->set('lng', $lng);
        $this->db->set('address_one', $adone);
        $this->db->set('address_two', $adtwo);
        $this->db->set('branch_desc', $desc);
        $this->db->set('branch_logo', $logo);
        $this->db->set('user_id', $uid);
        $this->db->insert('branch');
    }

    function get_branch($uid) {
        $this->db->where('user_id', $uid);
        $query = $this->db->get('branch');
        $data = $query->result_array();
        return $data;
    }

    function list_branches($uid) {
        $this->db->where('user_id', $uid);
        $this->db->where('status', 1);
        $query = $this->db->get('branch');
        $data = $query->result_array();
        return $data;
    }

    function branch_detail($id) {
        $this->db->where('branch_id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get('branch');
        $data = $query->result_array();
        return $data;
    }

    function update_branch($bid, $branch, $loct, $region, $postal, $country, $lat, $lng, $add1, $add2, $desc, $logo) {
        $this->db->where('branch_id', $bid);
        $data['branch_name'] = $branch;
        $data['location'] = $loct;
        $data['region'] = $region;
        $data['postal_code'] = $postal;
        $data['country'] = $country;
        $data['lat'] = $lat;
        $data['lng'] = $lng;
        $data['address_one'] = $add1;
        $data['address_two'] = $add2;
        $data['branch_desc'] = $desc;
        $data['branch_logo'] = $logo;
        $this->db->update('branch', $data);
    }

    function check_branch($id) {
        $this->db->where('branch_id', $id);
        $this->db->where('activeStatus !=', 2);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function delete_branch($id) {
        $this->db->where('branch_id', $id);
        $this->db->delete('branch');
    }

    function get_coupon($id) {
        $this->db->where('user_id', $id);
        $this->db->join('listing', 'listing.listingId = coupon_code.listingId');
        $query = $this->db->get('coupon_code');
        $data = $query->result_array();
        return $data;
    }

    function exist_coupon($lid, $cname) {
        $this->db->where('listingId', $lid);
        $this->db->where('coupon_name', $cname);
        $query = $this->db->get('coupon_code');
        $data = $query->result_array();
        return $data;
    }

    function save_coupon($lid, $cname, $ccode, $ctype, $quota, $desc, $uid) {
        $this->db->set('listingId', $lid);
        $this->db->set('coupon_name', $cname);
        $this->db->set('coupon_code', $ccode);
        $this->db->set('coupon_type', $ctype);
        $this->db->set('quota', $quota);
        $this->db->set('coupon_desc', $desc);
        $this->db->set('user_id', $uid);
        $this->db->insert('coupon_code');
    }

    function active_coupon($Id) {
        $this->db->where('cd_id', $Id);
        $data['status'] = 1;
        $this->db->update('coupon_code', $data);
    }

    function deactivate_coupon($Id) {
        $this->db->where('cd_id', $Id);
        $data['status'] = 2;
        $this->db->update('coupon_code', $data);
    }

    function delete_coupon($id) {
        $this->db->where('cd_id', $id);
        $this->db->delete('coupon_code');
    }

    function check_coupon($code) {
        $this->db->where('coupon_code', $code);
        $query = $this->db->get('booking');
        $data = $query->result_array();
        return $data;
    }

    function all_service($bid) {
        $this->db->where('branch_id', $bid);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function branch_details($bid, $bname) {
        $this->db->where('branch_id', $bid);
        $this->db->where('branch_name', $bname);
        $query = $this->db->get('branch');
        $data = $query->result_array();
        return $data;
    }

    function get_amenities($id) {
        $this->db->group_by('listing_features.features_id');
        $this->db->where('branch.branch_id', $id);
        $this->db->join('listing', 'listing.listingId = listing_features.listing_id');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('listing_features');
        $data = $query->result_array();
        return $data;
    }

    public function service_by_service_id($id) {
        $query = $this->db->query("SELECT * FROM listing LEFT JOIN services ON listing.service_Id = services.serviceId  WHERE service_Id='" . $id . "'");
        return $query->result();
    }

    public function service_by_city_id($latGoogle, $lngGoogle) {
        $query = $this->db->query("SELECT * FROM listing LEFT JOIN services ON listing.service_Id = services.serviceId  WHERE lat='" . $latGoogle . "'AND lng='" . $lngGoogle . "'");
        return $query->result();
    }

    public function listing_id($listingId, $listingName) {
        $this->db->where('listingId', $listingId);
        $this->db->where('listingName', $listingName);
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    public function listing_slot($listingId) {
        $this->db->where('listingId', $listingId);
        $query = $this->db->get('listing_slot');
        $data = $query->result_array();
        return $data;
    }

    public function all_services($bid, $bname) {
        $this->db->order_by('branch.branch_id', 'ASC');
        $this->db->where('branch.branch_id', $bid);
        $this->db->where('branch.branch_name', $bname);
        $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
        $this->db->join('services', 'services.serviceId = listing.service_Id');
        $this->db->join('branch', 'branch.branch_id = listing.branch_id');
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

    function get_list_image($lid) {
        $this->db->limit(1);
        $this->db->where('listing_id', $lid);
        $query = $this->db->get('listing_images');
        $data = $query->result_array();
        return $data;
    }

    public function save_cat_by_listingId($serviceId, $data, $listingId) {

        $this->db->where('listings_id', $listingId); // I change id with book_id
        $this->db->delete('services_vs_sub_services');
        $countAdd = 0;
        foreach ($data as $value) {
            $this->db->query("INSERT INTO services_vs_sub_services (services_Id,sub_services_id,listings_id) VALUES('" . $serviceId . "','" . $value . "','" . $listingId . "')");
            $countAdd ++;
        }

        if (count($data) == $countAdd) {
            return 1;
        }
    }

    public function service_listing_id($listingId, $listingInfo) {

        $serviceId = $listingInfo;
        $result = $this->db->query("SELECT  * FROM main_service_sub_service    LEFT JOIN sub_services ON main_service_sub_service.sub_services_id = sub_services.subServicesId WHERE services_Id = $serviceId ");
        $index = 0;
        $countValue = count($result->result());

        if ($countValue > 0) {
            foreach ($result->result() as $value) {

                $imgQuery = $this->db->query("SELECT * FROM services_vs_sub_services  WHERE listings_id= '" . $listingId . "' AND sub_services_id='" . $value->sub_services_id . "'");

                if (count($imgQuery->result())) {
                    $data[] = array(
                        'serviceId' => $value->subServicesId,
                        'serviceName' => $value->subServiceName,
                        'selected' => true
                    );
                } else {
                    $data[] = array(
                        'serviceId' => $value->subServicesId,
                        'serviceName' => $value->subServiceName,
                        'selected' => false
                    );
                }


                $index++;
            }
        } else {
            $data[] = null;
        }



        return $data;
    }

    public function service_listing_detail_by_id($listingId, $listingInfo) {
        $serviceId = $listingInfo;
        $result = $this->db->query("SELECT  * FROM main_service_sub_service    LEFT JOIN sub_services ON main_service_sub_service.sub_services_id = sub_services.subServicesId WHERE services_Id = $serviceId ");
        $index = 0;
        $countValue = count($result->result());
        return $result->result();
    }

    public function get_vendor_info_listing_id($listingId) {
        $query = $this->db->query("SELECT * FROM listing  WHERE listingId='" . $listingId . "' ");
        return $query->result();
    }

    public function service_by_id($service, $subService, $lcity) {
        $data = array();

        if ($service && $subService && $lcity) {
//            foreach ($subService as $subserviceVal) {
//                $this->db->group_by('branch.branch_id');
//                $this->db->where('listing.service_Id', $service);
//                $this->db->where('listing.sub_service_Id', $subserviceVal);
//                $this->db->where('branch.location', $lcity);
//                $this->db->where('listing.activeStatus', 1);
//                $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
//                $this->db->join('services', 'services.serviceId = listing.service_Id');
//                $this->db->join('branch', 'branch.branch_id = listing.branch_id');
//                $query = $this->db->get('listing');
//
//                $countValue = count($query->result());
//                if ($countValue > 0) {
//                    foreach ($query->result() as $value) {
//                        $data[] = $value;
//                    }
//                }
//            }
            $this->db->group_by('listing.branch_id');
            $this->db->where('listing.service_Id', $service);
//                $this->db->where('listing.sub_service_Id', $subserviceVal);
            $this->db->where('branch.location', $lcity);
            $this->db->where('branch.status', 1);
            $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
            $this->db->join('services', 'services.serviceId = listing.service_Id');
            $this->db->join('branch', 'branch.branch_id = listing.branch_id');
            $query = $this->db->get('listing');

            $countValue = count($query->result());
            if ($countValue > 0) {
                foreach ($query->result() as $value) {
                    $data[] = $value;
                }
            }

            return $data;
        } else {
            $this->db->where('listing.activeStatus', 1);
            $this->db->join('sub_services', 'sub_services.subServicesId = listing.sub_service_Id');
            $this->db->join('services', 'services.serviceId = listing.service_Id');
            $this->db->join('branch', 'branch.branch_id = listing.branch_id');
            $query = $this->db->get('listing');
            return $query->result();
        }
    }

    public function achieve_listing_id($id) {
        $this->db->where('listingId', $id);
        $data['activeStatus'] = 2;
        $this->db->update('listing', $data);
    }

    public function edit_listing_id_subservice($listingId, $listingInfo) {
        $serviceId = $listingInfo;

        $result = $this->db->query("SELECT  * FROM sub_services  LEFT JOIN main_service_sub_service ON main_service_sub_service.sub_services_id = sub_services.subServicesId WHERE services_Id = $serviceId ");
        $index = 0;
        $countValue = count($result->result());

        if ($countValue > 0) {
            foreach ($result->result() as $value) {

                $imgQuery = $this->db->query("SELECT * FROM listing  WHERE  sub_service_Id='" . $value->services_Id . "'");

                if (count($imgQuery->result())) {
                    $data[] = array(
                        'serviceId' => $value->subServicesId,
                        'serviceName' => $value->subServiceName,
                        'selected' => true
                    );
                } else {
                    $data[] = array(
                        'serviceId' => $value->subServicesId,
                        'serviceName' => $value->subServiceName,
                        'selected' => false
                    );
                }


                $index++;
            }
        } else {
            $data[] = null;
        }
        return $data;
    }

    public function listing_email_id($listingId) {
        $this->db->where('listingId', $listingId);
        $query = $this->db->get('listing');
        $data = $query->result_array();
        return $data;
    }

}
