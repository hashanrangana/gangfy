<?php

class Feature_model extends CI_Model {

    public function all_feature() {
        $query = $this->db->get('features');
        $data = $query->result_array();
        return $data;
    }

    public function all_listing_feature($listingId) {
        $result = $this->db->query("SELECT  * FROM features WHERE fStatus = 1");
        $index = 0;
        $countValue = count($result->result());

        if ($countValue > 0) {
            foreach ($result->result() as $value) {

                $imgQuery = $this->db->query("SELECT * FROM listing_features  WHERE listing_id= '" . $listingId . "' AND features_id='" . $value->featuresId . "'");

                if (count($imgQuery->result())) {
                    $data[] = array(
                        'featuresId' => $value->featuresId,
                        'featuresName' => $value->featuresName,
                        'selected' => 1
                    );
                } else {
                    $data[] = array(
                        'featuresId' => $value->featuresId,
                        'featuresName' => $value->featuresName,
                        'selected' => 0
                    );
                }

                $index++;
            }
        } else {
            $data[] = null;
        }
        return $data;
    }

    public function in_active_feature($Id) {
        $this->db->where('featuresId', $Id);
        $data['fStatus'] = 0;
        $this->db->update('features', $data);
    }

    public function active_feature($Id) {
        $this->db->where('featuresId', $Id);
        $data['fStatus'] = 1;
        $this->db->update('features', $data);
    }

    public function save_feature($data) {
        return $this->db->query("INSERT INTO features (	featuresName,fStatus) VALUES('" . $data['featuresName'] . "','" . $data['fStatus'] . "')");
    }

    public function save_features_listingId($lid, $fid) {
        $this->db->set('listing_id', $lid);
        $this->db->set('features_id', $fid);
        $this->db->insert('listing_features');
    }

    public function get_by_id($listingId) {
        $this->db->where('listing_id', $listingId);
        $this->db->join('features', 'features.featuresId = listing_features.features_id');
        $query = $this->db->get('listing_features');
        $data = $query->result_array();
        return $data;
    }

    public function get_feature_home() {
        $this->db->where('fStatus', 1);
        $query = $this->db->get('features');
        $data = $query->result_array();
        return $data;
    }

}
