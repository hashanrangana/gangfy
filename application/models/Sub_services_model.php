<?php

class Sub_services_model extends CI_Model {

    public function all_sub_service() {
        $query = $this->db->get('sub_services');
        $data = $query->result_array();
        return $data;
    }

    public function all_home_sub_service() {
        $this->db->where('subServiceStatus', 1);
        $query = $this->db->get('sub_services');
        $data = $query->result_array();
        return $data;
    }

    public function in_active_service($Id) {
        $this->db->where('subServicesId', $Id);
        $data['subServiceStatus'] = 0;
        $this->db->update('sub_services', $data);
    }

    public function active_service($Id) {
        $this->db->where('subServicesId', $Id);
        $data['subServiceStatus'] = 1;
        $this->db->update('sub_services', $data);
    }

    public function save_service($data) {
        return $this->db->query("INSERT INTO sub_services (	subServiceName,subServiceStatus) VALUES('" . $data['subServiceName'] . "','" . $data['subServiceStatus'] . "')");
    }

    public function all_sub_service_by_service_id($Id) {
        $this->db->where('subServicesId', $Id);
        $query = $this->db->get('sub_services');
        $data = $query->result_array();
        return $data;
    }

}
